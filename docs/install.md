### Windows (the easy way)

** The easy way for many Windows users is to install the *packaged* version, which
includes all the dependencies (Python and all libraries).  The latest version is
available [here](https://bitbucket.org/angusyeh/tim/downloads). **

** To install, simply unzip to a directory, then run the included file `tim.exe`.
Administrator privilege is not needed. **

### Install from Source

The other way is to install it like many other Python packages (TIM is written
in the Python language).  This method is better suited for users who is
interested in using PyTOUGH on other scripting in general.  Please download
[TIM's source code](https://bitbucket.org/angusyeh/tim) and execute the command:

```python setup.py install```

Launch TIM by running the script `tim.py`,  created during install.  Windows users might find it under a directory like this `C:\Python27\Scripts\`.

### Required Packages

* Python 2.7
* PyQt 4.8+ (if you have the latest PyTOUGH working, this is the only additional library required)
* NumPy
* Pint
* Matplotlib 1.0+ (and its dependencies)
* Scipy
* VTK (optional)

### Install Python and Python packages on Windows

For Windows users that wants to use the whole Python environment, I recommend Python installer from [official website](https://www.python.org/downloads/) and packages provided by [Christoph Gohlke](http://www.lfd.uci.edu/~gohlke/pythonlibs/).

Here is an example step by step guide for a clean system.  If you already have Python and Python packages installed on your machine, you should be very careful.

Note any versions later than these are likely to be fine.  You will only see more up-to-date version than this list. It is crucial to choose the ones with keyword ```cp27```.  The ```win_amd64``` keyword is to match Python installer's ```X86-64```.

> *(For 32-bit Windows you need to install 32-bit Python installer from official Python, and choose all files labelled with ```win32``` from the package site.)*

> *Another good instruction on installing Python on Windows can be found here: <http://python-guide.readthedocs.org/en/latest/starting/install/win/#installing-python-on-windows>*

1. Download and install Python:

    * Official Python [Windows X86-64 MSI Installer (2.7.10 or later)](https://www.python.org/ftp/python/2.7.10/python-2.7.10.amd64.msi)

    * Install the MSI (administrator privilege is required), you should 
      select the option of "Adding python.exe to your Path".

    * Check if the system has path points to the python folder and the python script folder.  On Win 7 or later, hit Win key, then type ```system environment```, you will find the option ```Edit the system environment variables```, then click ```Environment Variables```.  For example, the ```Path``` variable on my machine contains these two directories ```C:\Python27\;C:\Python27\Scripts\```.

2. Download packages from [Christoph Gohlke's website](http://www.lfd.uci.edu/~gohlke/pythonlibs/), for example:
    
    - numpy-1.9.2+mkl-cp27-none-win_amd64.whl
    - PyQt4-4.11.4-cp27-none-win_amd64.whl
    - matplotlib-1.4.3-cp27-none-win_amd64.whl
    - scipy-0.16.0-cp27-none-win_amd64.whl
    - VTK-6.2.0-cp27-none-win_amd64.whl (optional, only for exporting to VTK related files using PyTOUGH)

3. Use pip to install packages:

    * Launch command line window, browse to the directory where you downloaded the package .whl files.

    * type ```pip install SomePackages‑cp27‑none‑win_amd64.whl```, name of the .whl files here should matches exactly with the ones you have downloaded.  Do this to install all packages.  Check the screen to see if al packages are successfully installed.  Read more about wheels (those .whl files) and pip [here](https://pip.pypa.io/en/latest/user_guide.html#installing-from-wheels).

    * Install other packages (pure Python) directly using pip, Pint is required, but PyQtGraph and MkDocs is optional.

    ```
    pip install pint
    pip install pyqtgraph
    pip install mkdocs
    ```

4. Download and install PyTOUGH:

    * Download the source ZIP file from [PyTOUGH](https://github.com/acroucher/PyTOUGH) (look for the 'Download ZIP' button).

    * If you want the cutting edge 'testing' branch, click 'branch: master' and then select 'testing'.  This will allow you to download the latest testing version of PyTOUGH.

    * Install by executing ```python setup.py install``` from unzipped source directory.

5. Download and install TIM:
    
    * Download the source ZIP from TIM's [download page](https://bitbucket.org/angusyeh/tim/downloads), click 'Download repository'.

    * Install by executing ```python setup.py install``` from unzipped source directory.

7. Run:

    * Under the command line, type ```tim.py``` should get it started.  Please make sure the Python script directory (where tim.py is located, eg. C:\Python27\Scripts\) is in your path.

### If all goes to custard

Hopefully you will not need to do this.  But if you have lots of trouble using various Python packages on Windows, you may need to clean up your Python installation (and packages).

I will write up step by step guide if there is a demand, but here I will just point out some checking/cleaning tips.  During the process, you might find a geeky friend very handy.

If you feel the following tips looks dangerous or confusing, maybe it's time to just download and use the [packaged version of TIM](#windows-the-easy-way).  It really doesn't affect (or affected by) other parts of your system.

* Check if you have multiple (and potentially conflicting) Python installed

    - If so, which Python are you actually running (in commandline > `where python`)

    - Some applications ship with their own Python.  It might be okay, as long as you have your `PATH` setup properly.  (in Start > type ```Edit environment vairables```).  Usually you want your Python installation listed **before** other applications.  These applications tend to have no trouble finding their own Python.

* Check if your Python version matches the packages

    - Type `python` in commandline, and observe the screen.  You want to be sure that your packages' version matches Python's: 32-bit to win32, 64-bit to amd64/x86_64.

    - Any version dependency not satisfied?

* Check your packages' path, do they match what you expects?

    - import a module, and then check where is that module located in your disk

    - check the python's module searching path (`sys.path`)

    - check the system environment `PYTHONPATH` as well

```
>>> import mulgrids
>>> print mulgrids.__file__
```

```
>>> import sys
>>> print '\n'.join(sys.path)
```

* To uninstall Python (after unisntall all packages), try using the normal method of uninstall: Start > Program and Features > etc.  After all these, check the following:

    - Inspect the Python directory.  You may need to manually remove some left-over Python if you previously installed Python twice on top of each other etc.

    - Inspect Python directory to remove all left-over pacakges, in something like `C:\Python27\Lib\site-packages\`.  Note these might not get uninstalled even if you uninstall 'Python'.  Just delete the whole Python directory if necessary.

