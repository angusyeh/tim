"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

# This spec file is used by PyInstaller for generating a portable folder of TIM.
#
# Usage:
# pyinstaller tim.spec
#
# This will create two directories 'build' and 'dist'.  'dist' directory will
# contain a 'tim' folder that able to to distributed without any of the
# dependency like Python, PyTOUGH and any other required modules.
#
# In order to keep OyTOUGH and TIM as source, instead of bundled into the
# executable, the entries for PyTOUGH and TIM files are separately processed,
# and directly kept in the distribution as source.

# 17/07/2018 4:22:08 p.m.
# NOTE: on windows with shapely there is an error at the moment, needs to modify
# C:\Python27\Lib\site-packages\PyInstaller\hooks\hook-shapely.py
# Line 28: dll_files = ['geos_c.dll'] # , 'geos.dll' is missing on windows

# -*- mode: python -*-
a = Analysis(['tim.py'],
             pathex=['.\\source'],
             hiddenimports=["Tkinter", "FileDialog"],
             hookspath=None,
             runtime_hooks=None)

# files of PyTOUGH source
pytough_files = [
  'fixed_format_file',
  'geometry',
  'IAPWS97',
  'mulgrids',
  'setup',
  't2data',
  't2grids',
  't2incons',
  't2listing',
  't2thermo',]

# A TOC appears to be a list of tuples of the form (name, path, typecode). In
# fact, it's an ordered set, not a list. A TOC contains no duplicates, where
# uniqueness is based on name only. Furthermore, within this constraint, a TOC
# preserves order.

pytough_orig = TOC()
pytough_to_add = TOC()
for x in a.pure:
  if x[0] in pytough_files:
    pytough_orig.append(x)
    x = (x[0]+'.py', x[1].replace('.pyc','.py'), 'PYSOURCE')
    pytough_to_add.append(x)

# files of TIM's source
tim_orig = TOC()
tim_to_add = TOC()
for x in a.pure:
  if x[0].startswith('timgui'):
    tim_orig.append(x)
    if '__init__' in x[1]:
      x = ('timgui\\__init__.py', x[1].replace('.pyc','.py'), 'PYSOURCE')
    else:
      x = (x[0].replace('timgui.','timgui\\')+'.py', x[1].replace('.pyc','.py'), 'PYSOURCE')
    tim_to_add.append(x)

# additional files
tim_data = TOC()
tim_data.append(('COPYING', '.\\COPYING', 'DATA'))
tim_data.append(('TIM_README.md', '.\\README.md', 'DATA'))
tim_data.append(('timgui\\ui_resources.qrc', 'timgui\\ui_resources.qrc', 'DATA'))
tim_data.append(('timgui\\arrow_scales.json', 'timgui\\arrow_scales.json', 'DATA'))
tim_data.append(('timgui\\colorbars.json', 'timgui\\colorbars.json', 'DATA'))
tim_data.append(('timgui\\keymap.json', 'timgui\\keymap.json', 'DATA'))

# take PyTOUGH and TIM out from pyz
# NOTE a.pure - pytough_orig - tim_orig does not work anymore
to_remove = pytough_orig + tim_orig
in_exe = a.pure - to_remove
pyz = PYZ(in_exe)

exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='tim.exe',
          debug=False,
          strip=None,
          upx=True,
          console=True )

# include PyTOUGH and TIM separately
coll = COLLECT(pytough_to_add, tim_to_add, tim_data,
               exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=None,
               upx=True,
               name='tim')
