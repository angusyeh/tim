#! python distutils installer will modify this line (for *unix)

"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""
import sys
import os

if __name__ == '__main__':
    # overwrite version info with detailed git desc if git/repo available
    import timgui.info
    from timgui.get_version_info import getGitDesc
    v = getGitDesc(os.path.dirname(os.path.realpath(__file__)))
    if v:
        timgui.info.long_version = v

    # this will trigger initialisation of logging module
    import timgui.log

    from PyQt4.QtGui import QApplication,QIcon
    app = QApplication(sys.argv)
    app.setOrganizationName("University of Auckland")
    app.setOrganizationDomain("auckland.ac.nz")
    app.setApplicationName("Tim isn't Mulgraph")
    app.setWindowIcon(QIcon(":/rabbit_large.png"))

    from timgui.ui_main_window import MainWindow
    tim = MainWindow()
    tim.show()

    app.exec_()
