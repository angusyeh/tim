"""
Copyright 2013, 2023 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from colorbar_and_scale import FloatFormatter

class Ui_FormatterDialog(QDialog):
    """
    A modal dialog box that let user choose an desired float formatting. Choices
    are:
    - automatic
    - fixed-point, user config decimal places
    - scientific, user config decimal places
    """
    def __init__(self, parent=None):
        super(Ui_FormatterDialog, self).__init__(parent,
            Qt.WindowTitleHint | Qt.WindowSystemMenuHint) # hide What's This
        self.setWindowTitle('Number format')
        self.group_box = QGroupBox()
        self.group_box.setLayout(QGridLayout())
        self.buttons = QButtonGroup()
        self.button_box = QDialogButtonBox(
            QDialogButtonBox.Ok | QDialogButtonBox.Cancel)

        self.setLayout(QVBoxLayout())
        self.layout().addWidget(self.group_box)
        self.layout().addWidget(self.button_box)

        # choices
        self.rb_auto = QRadioButton('Automatic')
        self.rb_fixedpoint = QRadioButton('Fixed-point')
        self.rb_scientific = QRadioButton('Scientific')
        self.spin_fixedpoint = QSpinBox()
        self.spin_scientific = QSpinBox()

        self.buttons.addButton(self.rb_auto, 0)
        self.buttons.addButton(self.rb_fixedpoint, 1)
        self.buttons.addButton(self.rb_scientific, 2)
        self.group_box.layout().addWidget(self.rb_auto,0,0)
        self.group_box.layout().addWidget(self.rb_fixedpoint,1,0)
        self.group_box.layout().addWidget(self.rb_scientific,2,0)
        self.group_box.layout().addWidget(self.spin_fixedpoint,1,1)
        self.group_box.layout().addWidget(self.spin_scientific,2,1)

        # activate/deactivate spinboxes following radio buttons
        QObject.connect(self.rb_fixedpoint, SIGNAL('toggled (bool)'),
                        self.spin_fixedpoint.setEnabled)
        QObject.connect(self.rb_scientific, SIGNAL('toggled (bool)'),
                        self.spin_scientific.setEnabled)

        # initialise consistent state
        self.rb_fixedpoint.setChecked(False)
        self.spin_fixedpoint.setEnabled(False)
        self.rb_scientific.setChecked(False)
        self.spin_scientific.setEnabled(False)
        self.spin_fixedpoint.setRange(0, 10)
        self.spin_scientific.setRange(0, 10)
        self.spin_fixedpoint.setValue(2)
        self.spin_scientific.setValue(2)
        self.rb_auto.setChecked(True)

        QObject.connect(self.button_box, SIGNAL('accepted()'), self.accept)
        QObject.connect(self.button_box, SIGNAL('rejected()'), self.reject)

    def setUp(self, vname, formatter=None):
        """
        set up the choices of the dialog box, if a formatter is provided, the
        value of it will be used to initialise the dialog.
        """
        def decode_fmt(fmt):
            if fmt == 'auto':
                return 'auto', 0
            else:
                try:
                    if 'f' in fmt:
                        pt = fmt.index('f')
                        pp = fmt.index('.')
                        return 'fixedpoint', int(fmt[pp+1:pt])
                    if 'e' in fmt:
                        pt = fmt.index('e')
                        pp = fmt.index('.')
                        return 'scientific', int(fmt[pp+1:pt])
                except Exception as e:
                    msg = 'Unknown string format: %s' % fmt
                    logger.error(msg)
                    raise Exception(e)
        # set up the dialog box
        self.group_box.setTitle(vname)
        if formatter is None:
            formatter = FloatFormatter('auto')
        typ, decimals = decode_fmt(formatter.getFormat())
        if typ == 'auto':
            self.rb_auto.setChecked(True)
        else:
            if typ == 'fixedpoint':
                self.rb_fixedpoint.setChecked(True)
                self.spin_fixedpoint.setValue(decimals)
            elif typ == 'scientific':
                self.rb_scientific.setChecked(True)
                self.spin_scientific.setValue(decimals)
            else:
                raise Exception('Unknown string format: %s' % fmt)

    def getSelected(self):
        """ return a new formatter with configuration """
        if self.rb_auto.isChecked():
            fmt = 'auto'
        elif self.rb_fixedpoint.isChecked():
            fmt = '%%.%df' % self.spin_fixedpoint.value()
        elif self.rb_scientific.isChecked():
            fmt = '%%.%de' % self.spin_scientific.value()
        return FloatFormatter(fmt=fmt)


def getFormatFromDialog(vname, formatter):
    """
    Raise a modal UI to ask user to choose a number format. Return the updated
    formatter object if accepted (user clicked OK), otherwise return None.
    """
    dialog = Ui_FormatterDialog()
    dialog.setUp(vname, formatter)
    if dialog.exec_():
        return dialog.getSelected()
    else:
        return None


if __name__ == '__main__':
    import sys
    from colorbar_and_scale import FloatFormatter
    app = QApplication(sys.argv)

    print getFormatFromDialog('test', FloatFormatter('%.5f'))


