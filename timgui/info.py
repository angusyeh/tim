"""
Information about the TIM application.
"""

"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

# these variables are also used by the distutils setup
name = "TIM"
long_name = "Tim Isn't Mulgraph"
version = "0.7.2"
long_version = ""
# NOTE long_version will be overwritten if git desc can be obtained alive
#      by running "python timgui/get_version_info.py"

description = "Tim Isn't Mulgraph - A Graphical Tool for TOUGH2 Simulators"
long_description = ''.join([
    "Tim Isn't Mulgraph (TIM) is a graphical tool for TOUGH2 simulators.  TIM ",
    "aims to ease model calibration with a set of 2-D based visualisation tools ",
    "combined with ability to modify key parameters interactively. ",
])

author='Angus Yeh, University of Auckland'
author_email='a.yeh@auckland.ac.nz'
url='https://bitbucket.org/angusyeh/tim'
license='GNU GPL'
