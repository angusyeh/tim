"""
This module provides base classes of commands.
"""

"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

class WindowCommand(object):
    """ Base class for commands to be executed """
    def __init__(self, scope):
        super(WindowCommand, self).__init__()
        self.__dict__ = scope.copy()

    @classmethod
    def name(cls):
        clsname = cls.__name__
        name = clsname[0:-8]
        return name

    def run(self):
        """ concrete command class should implement this """
        pass

    def undo(self):
        """ concrete command class should implement this"""
        pass

    @property
    def caption(self):
        """ implement this to return a text used by action or as caption """
        return self.__class__.__name__

    tip = ""

