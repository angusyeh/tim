"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *

# constants, will be moved
mv_ITEM_COORD_CACHE = False
mv_ITEM_DEVICE_CACHE = True


class FixedSizePoint(QGraphicsItem):
    """ A point with a fixed size as seen on the screen, regardless of zoome
    level (to the first QGraphicsView object it is displayed in).
    """
    def __init__(self, x, y, size=10.0, color='red', parent=None):
        super(QGraphicsItem, self).__init__(parent)
        self.x = x
        self.y = y
        self.size = size
        self.scaled_size = size
        self.brush = QBrush(QColor('red'))
        self.setPos(x, y)
    
    def boundingRect(self):
        size = self.scaled_size
        return QRectF(- size / 2.0, - size / 2.0, size, size)
    
    def calc_scaled_size(self):
        try:
            view = self.scene().views()[0]
        except IndexError:
            # cannot calc scaled_size if no view attached
            return self.size
        view_rect = view.viewport().rect()
        view_top_left = view.mapToScene(view_rect.topLeft())
        view_bottom_right = view.mapToScene(view_rect.bottomRight())
        # skip Y calculation because width and height are the same
        scene_width = view_bottom_right.x() - view_top_left.x()
        # scene_height = view_bottom_right.y() - view_top_left.y()
        scaled_width = self.size / view.viewport().width() * scene_width
        # scale_height = self.size / view.viewport().height() * scene_height
        return scaled_width

    def paint(self, painter, option, widget):
        size = self.calc_scaled_size()
        if self.scaled_size != size:
            self.scaled_size = size
            self.prepareGeometryChange() # trigger update of .boundingRect()
        painter.setBrush(self.brush)
        painter.setPen(Qt.NoPen)
        painter.drawEllipse(self.boundingRect())


class TimPolygonItem(QGraphicsPolygonItem):
    def __init__(self, polygon, parent=None):
        super(TimPolygonItem, self).__init__(polygon,parent)
        """
        if mv_ITEM_COORD_CACHE:
            self.setCacheMode(QGraphicsItem.ItemCoordinateCache)
        elif mv_ITEM_DEVICE_CACHE:
            self.setCacheMode(QGraphicsItem.DeviceCoordinateCache)
        """
        self.setFlag(QGraphicsItem.ItemIsSelectable)

        # additional members
        self.name = None # (block name)

        super(TimPolygonItem, self).setBrush(QBrush())
        self._originalBrush = self.brush()

        # use gradient color as mouse hovering effect
        self.hover_color = QColor('orange')
        self.hover_color.setAlpha(200)
        rect = self.boundingRect()
        self._hover_gradient = QRadialGradient(rect.center(), min(rect.width(), rect.height()))
        self._hover_gradient.setColorAt(0, self.brush().color());
        self._hover_gradient.setColorAt(1, self.hover_color);

    def setBrush(self, brush):
        """ This is to fix the issue of mouse hovering and setBrush() is called
        externally. Note this is to overwerite when the item is set to a color
        externally.  Within most internal methods, the super().setBrush() should
        be used instead.
        """
        self._originalBrush = brush
        super(TimPolygonItem, self).setBrush(brush)

    def hoverEnterEvent (self, e):
        self._originalBrush = self.brush()
        # need to ensure the gradient color is correct in centre
        self._hover_gradient.setColorAt(0, self.brush().color());
        super(TimPolygonItem, self).setBrush(QBrush(self._hover_gradient))
        QGraphicsPolygonItem.hoverEnterEvent(self,e)

    def hoverLeaveEvent(self, e):
        super(TimPolygonItem, self).setBrush(self._originalBrush)
        QGraphicsPolygonItem.hoverLeaveEvent(self,e)

    def paint(self, painter, option, widget = None):
        # remove state from option, so it draws without selection marquee
        option.state = option.state & ~QStyle.State_Selected
        super(TimPolygonItem, self).paint(painter, option, widget)
        # add own painting
        if self.isSelected():
            # TODO: not good to know things about scene.
            if hasattr(self.scene(), 'isSingleSelectionMode') and self.scene().isSingleSelectionMode():
                brush = QBrush(QColor('green'), Qt.DiagCrossPattern)
            else:
                brush = QBrush(QColor('magenta'), Qt.DiagCrossPattern)
            # The pattern will get scaled by scene/view, set an inverted device
            # transform here to undo the effect.  Unlike Qt, PyQt returns a
            # tuple (QTransform, invertible)
            # TODO: not good to refer to view (and only the first one)
            matrix = self.deviceTransform(self.scene().views()[0].viewportTransform()).inverted()[0]
            brush.setTransform(matrix)
            painter.setBrush(brush)
            painter.drawPolygon(self.polygon())

class TimTextItem(QGraphicsSimpleTextItem):
    """ simple text item centre aligned """
    def __init__(self, text, parent=None):
        super(TimTextItem, self).__init__(text, parent)
        self.scale(1,-1)
        self.scale(1,1)
    def boundingRect(self):
        """ reimplement boundingRect() to centre around the centre (instead of top left of original textitem) """
        _bounds = super(TimTextItem, self).boundingRect()
        offset = QPointF(-_bounds.width() / 2.0,-_bounds.height() / 2.0)
        _bounds.translate(offset)
        return _bounds
    def paint(self, painter, option, widget = None):
        """ translate paint() as well """
        _bounds = self.boundingRect()
        painter.translate(_bounds.left(), _bounds.top())
        super(TimTextItem, self).paint(painter, option, widget)
        # http://pyqt.sourceforge.net/Docs/PyQt4/qgraphicsitem.html#paint
        # Make sure to constrain all painting inside the boundaries of boundingRect()
        # to avoid rendering artifacts (as QGraphicsView does not clip the painter
        # for you). In particular, when QPainter renders the outline of a shape using
        # an assigned QPen, half of the outline will be drawn outside, and half inside,
        # the shape you're rendering (e.g., with a pen width of 2 units, you must
        # draw outlines 1 unit inside boundingRect()). QGraphicsItem does not support
        # use of cosmetic pens with a non-zero width.
        """
this isn't that slow it seems
>>> for s in window.s_manager.scenelist:
...     for t in s._textitems:
...             t.scale(5,5)

But... http://qt-project.org/doc/qt-5.0/qtwidgets/qgraphicsitem-compat.html
Compatibility Members for QGraphicsItem, The following class members are part of the Qt compatibility layer. We advise against using them in new code.
    void QGraphicsItem::scale(qreal sx, qreal sy)
Use
    setTransform(QTransform::fromScale(sx, sy), true);
instead.
Scales the current item transformation by (sx, sy) around its origin. To scale from an arbitrary point (x, y), you need to combine translation and scaling with setTransform().
Example:
    // Scale an item by 3x2 from its origin
    item->scale(3, 2);

    // Scale an item by 3x2 from (x, y)
    item->setTransform(QTransform().translate(x, y).scale(3, 2).translate(-x, -y));
        """

class TimArrowItem(QGraphicsPathItem):
    def __init__(self, fromP, toP, length, parent=None):
        self._fromP = fromP
        self._toP = toP
        self._ratio = 0.1
        from math import pow, sqrt
        self._dx, self._dy = toP.x()-fromP.x(), toP.y()-fromP.y()
        self._lengthFromTo = sqrt(pow(self._dx,2.0) + pow(self._dy,2.0))

        path = self._makePath(length)
        super(TimArrowItem, self).__init__(path,parent)

    def _makePath(self,length):
        # path will be p1 - p2 - p3 - p4 - p2

        # improve performance ~10% of initial arrow creation
        if length == 0.0:
            return QPainterPath(self._fromP)

        p2x = self._fromP.x() + self._dx * length / self._lengthFromTo
        p2y = self._fromP.y() + self._dy * length / self._lengthFromTo
        p2 = QPointF(p2x,p2y)

        pcx = self._fromP.x() + self._dx * (1.0-self._ratio) * length / self._lengthFromTo
        pcy = self._fromP.y() + self._dy * (1.0-self._ratio) * length / self._lengthFromTo
        ddx = self._dy * self._ratio *length / self._lengthFromTo
        ddy = self._dx * self._ratio *length / self._lengthFromTo
        p3 = QPointF(pcx+ddx,pcy-ddy)
        p4 = QPointF(pcx-ddx,pcy+ddy)

        path = QPainterPath(self._fromP)
        path.lineTo(p2)
        path.lineTo(p3)
        path.lineTo(p4)
        path.lineTo(p2)
        return path

    def setLength(self,length):
        path = self._makePath(length)
        self.setPath(path)

def testGraphicsItems(scene):
    if scene is None: return

    scene.setSceneRect(0.0,0.0,100.0,100.0)

    nodes = [
    ( 5.0,10.0),
    (55.0,10.0),
    (55.0,20.0)]

    points = QPolygonF()
    for node in nodes: points.append(QPointF(node[0],node[1]))
    itemCol = TimPolygonItem(points)
    itemCol.setAcceptsHoverEvents(True)
    scene.addItem(itemCol)

    itemText = TimTextItem('TEST') # make child, so hover not broken
    itemText.setPos(5.0,10.0)
    scene.addItem(itemText)

    itemArrow = TimArrowItem(QPointF(5.0,10.0),QPointF(10.0,25.0),100.0)
    scene.addItem(itemArrow)

    #itemText = QGraphicsSimpleTextItem('TEST',itemCol)
    #itemText.setPos(5.0,10.0)


if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    view = QGraphicsView()
    view.show()

    scene = QGraphicsScene()
    testGraphicsItems(scene)
    view.setScene(scene)

    app.exec_()
