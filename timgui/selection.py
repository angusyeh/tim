"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from graphics_scenes import *
from ui_control_mode import Ui_SceneModeControl
from t2model import Model

class BlockRockEntry(QObject):
    """ given a string (regular expression allowed) this class can provide
    a list of all matching block names and their rocktype names """
    def __init__(self,model,entry):
        """ a tim._model_data.Model() object is required, entry """
        super(BlockRockEntry,self).__init__()
        self._model = model # should I connect or just keep a ref?
        self._entry_match = []    # keeps a list of names of matched blocks
        self._entry_rocks = set() # keeps a set of rocktype names of matched blocks
        self.entry = entry

    def __repr__(self):
        return self.entry

    def getEntry(self):
        """ original entry string """
        return self._entry
    def setEntry(self,e):
        """ entry should be a string that match block name, can be regular expression """
        self._entry = str(e)
        import re
        self._r = re.compile(str(e))
        self.refresh()
    entry = property(getEntry, setEntry)

    def refresh(self):
        """ this should be called when model updated """
        self._entry_match, self._entry_rocks = [], set()
        if self._model is None: return # no blocks or rocks if no model
        if self._model.geo():
            for i,b in enumerate(self._model.geo().block_name_list):
                if self._r.match(b) is not None:
                    self._entry_match.append(b)
                    if self._model.dat():
                        if self._model.dat_simulator in ['waiwera']:
                            ridx = self._model.getData(self._model.BLOCK, 'Rock Type Index')[i]
                            self._entry_rocks.add(ridx)
                        elif self._model.dat_simulator in ['autough2', 'tough2']:
                            self._entry_rocks.add(self._model.dat().grid.block[b].rocktype.name)

    @property
    def blocks(self):
        """ a list of matched block names, read-only """
        return self._entry_match

    @property
    def rocks(self):
        """ a list of rocktype names of matched blocks, read-only """
        return list(self._entry_rocks)

class Selection(QAbstractTableModel):
    """ A class that keeps track of overall selection of eg. blocks.
    See Qt's Model/View programming, inherits QAbstractTableModel for
    use with QTableView. """
    def __init__(self):
        super(Selection, self).__init__()
        self._model = None
        self._total_selection = set()
        self._entries = []

    def selected(self):
        """ return set of selected blocks. """
        self._total_selection = set()
        for e in self._entries:
            for b in e.blocks:
                self._total_selection.add(b)
        return self._total_selection

    def has(self,sel):
        """ detect if entry already exist """
        return sel in [e.entry for e in self._entries]

    def add(self,sel):
        """ Add selection entry.

        Will avoid adding repeated entry, but it's okay if two different entries
        matches some repeating blocks (self.selected() is a set of selected
        blocks, not entries.) """
        if self.has(sel):
            return
        i = len(self._entries)
        self.beginInsertRows(QModelIndex(),i,i)
        self._entries.append(BlockRockEntry(self._model,sel))
        self.endInsertRows()

    def remove(self,sel):
        i = None
        for ie,e in enumerate(self._entries):
            if e.entry == sel:
                i = ie
                break
        if i is None: return
        self.beginRemoveRows(QModelIndex(), i, i)
        self._entries.pop(i)
        self.endRemoveRows()

    def clear(self):
        self.beginResetModel()
        self._entries, self._selection = [],set()
        self.endResetModel()

    def rowCount(self, index=QModelIndex()):
        return len(self._entries)

    def columnCount(self, index=QModelIndex()):
        return 3

    def headerData(self, section, orientation, role = Qt.DisplayRole):
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                if section == 0:
                    return QVariant('Selection')
                elif section == 1:
                    return QVariant('Matched Blocks')
                elif section == 2:
                    return QVariant('Rocktypes')
            elif orientation == Qt.Vertical:
                return QVariant(int(section+1))
        return QVariant()

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid() or not (0 <= index.row() < self.rowCount()):
            return QVariant()

        e = self._entries[index.row()]
        column = index.column()
        if role == Qt.DisplayRole:
            if column == 0:
                return QVariant(e.entry)
            elif column == 1:
                return QVariant(','.join(e.blocks))
            elif column == 2:
                # column "rock types of selected blocks", for information only,
                # not so critical, e.rocks are:
                #   au/tough2 - list of rock type names
                #   waiwera - list of rock type indices
                return QVariant(','.join([str(x) for x in e.rocks]))
        return QVariant()

    def _modelReloaded(self):
        self.beginResetModel()
        for e in self._entries:
            e.refresh()
        self.endResetModel()
    def _modelEdited(self,(mdtype,valName,status)):
        if mdtype==Model.BLOCK and valName=='Rock Type' and status==Model.UPDATED:
            self._modelReloaded()

    def connectModel(self,model):
        """ only connects to a model at any given time, all entries re-created with new model """
        if self._model:
            self.disconnect(self._model,SIGNAL("model_geo_reloaded"),self._modelReloaded)
            self.disconnect(self._model,SIGNAL("model_dat_reloaded"),self._modelReloaded)
            self.disconnect(self._model,SIGNAL("model_raw_data_changed"), self._modelEdited)
        self._model = model
        if self._model:
            self.connect(self._model,SIGNAL("model_geo_reloaded"),self._modelReloaded)
            self.connect(self._model,SIGNAL("model_dat_reloaded"),self._modelReloaded)
            self.connect(self._model,SIGNAL("model_raw_data_changed"), self._modelEdited)

        # each entry only accepts a static model, so re-make all when model changed to a diff obj
        self.beginResetModel()
        original = [e.entry for e in self._entries]
        self._entries = [BlockRockEntry(self._model,e) for e in original]
        self.endResetModel()


