"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *

class FloatingChildWidgetBase(object):
    """ use this as base class for floating mechanism inside a parent widget,
    parent needs to inherit FloatingParentWidgetBase class to be able to work,
    for highlighting effects to work, must implement _setHighlight() and _unsetHighlight() """
    (FLOATING_OTHER,
        FLOATING_TOPLEFT, FLOATING_LEFT, FLOATING_BOTTOMLEFT,
        FLOATING_TOPRIGHT, FLOATING_RIGHT, FLOATING_BOTTOMRIGHT,
        FLOATING_TOP, FLOATING_BOTTOM) = (0,1,2,3,4,5,6,7,8)
    FLOATING_CURSORS = [Qt.SizeAllCursor,
        Qt.SizeFDiagCursor, Qt.SizeHorCursor, Qt.SizeBDiagCursor,
        Qt.SizeBDiagCursor, Qt.SizeHorCursor, Qt.SizeFDiagCursor,
        Qt.SizeVerCursor, Qt.SizeVerCursor]
    def mousePressEvent(self,e):
        """ left button allows user to move widget inside parent """
        # probably should make this an option of ratio or something
        if self.minimumWidth() == 0 or self.minimumHeight() == 0:
            self.setMinimumSize(self.width(),self.height())
        if e.button() == Qt.LeftButton:
            self._offset = e.pos()
            self._mode = self._controlHandle(e.pos())

    def mouseReleaseEvent(self,e):
        """ left button allows user to move widget inside parent """
        if e.button() == Qt.LeftButton:
            self._offset = QPoint()
            self._mode = None

    def mouseMoveEvent(self,e):
        """ left button allows user to move widget inside parent """
        if e.buttons() == Qt.LeftButton:
            if self._mode == self.FLOATING_OTHER:
                move_to = self.mapToParent(e.pos() - self._offset)
                xmax = self.parent().width() - self.width()
                ymax = self.parent().height() - self.height()
                if move_to.x() < 0: move_to.setX(0)
                if move_to.x() > xmax: move_to.setX(xmax)
                if move_to.y() < 0: move_to.setY(0)
                if move_to.y() > ymax: move_to.setY(ymax)
                self.move(move_to)
                return
            elif self._mode is not None:
                self._resize(e,self._mode)
                return
        self.setCursor(self.FLOATING_CURSORS[self._controlHandle(e.pos())])

    def mouseDoubleClickEvent(self,e):
        """ emit a signal """
        if e.buttons() == Qt.LeftButton:
            self.emit(SIGNAL("double_clicked"))

    def _resize(self,e,mode):
        """ call this with mouseMoveEvent to resize self """
        def left_lim():
            """ for left resize, tox is move limit, tow is width limit
            Note, minimum size is achieved by built-in mechanism """
            tox = self.mapToParent(e.pos()).x()
            tox = min(tox,self.mapToParent(QPoint(self.width()-self.minimumWidth(),0)).x())
            tox = max(tox,0)
            tow = self.width()-e.pos().x()
            tow = min(tow,self.width()-self.mapFromParent(QPoint(0,0)).x())
            return(tox,tow)
        def top_lim():
            """ for top resize, toy is move limit, toh is height limit
            Note, minimum size is achieved by built-in mechanism """
            toy = self.mapToParent(e.pos()).y()
            toy = min(toy,self.mapToParent(QPoint(0,self.height()-self.minimumHeight())).y())
            toy = max(toy,0)
            toh = self.height()-e.pos().y()
            toh = min(toh,self.height()-self.mapFromParent(QPoint(0,0)).y())
            return(toy,toh)
        max_right_bottom = self.mapFromParent(self.parent().rect().bottomRight())
        def right_lim():
            """ returns proper width to use when resize from right """
            return min(e.pos().x(),max_right_bottom.x())
        def bottom_lim():
            """ returns proper height to use when resize from bottom """
            return min(e.pos().y(),max_right_bottom.y())
        if mode == self.FLOATING_RIGHT:
            self.resize(right_lim(),self.height())
        elif mode == self.FLOATING_BOTTOM:
            self.resize(self.width(),bottom_lim())
        elif mode == self.FLOATING_BOTTOMRIGHT:
            self.resize(right_lim(),bottom_lim())
        elif mode == self.FLOATING_LEFT:
            tox, tow = left_lim()
            toy, toh = self.pos().y(), self.height()
            self.move(tox,toy)
            self.resize(tow,toh)
        elif mode == self.FLOATING_TOP:
            tox, tow = self.pos().x(), self.width()
            toy, toh = top_lim()
            self.move(tox,toy)
            self.resize(tow,toh)
        elif mode == self.FLOATING_TOPLEFT:
            tox, tow = left_lim()
            toy, toh = top_lim()
            self.move(tox,toy)
            self.resize(tow,toh)
        elif mode == self.FLOATING_TOPRIGHT:
            tox, tow = self.pos().x(), right_lim()
            toy, toh = top_lim()
            self.move(tox,toy)
            self.resize(tow,toh)
        elif mode == self.FLOATING_BOTTOMLEFT:
            tox, tow = left_lim()
            toy, toh = self.pos().y(), bottom_lim()
            self.move(tox,toy)
            self.resize(tow,toh)

    def _controlHandle(self,point):
        """ return which "control points" (resize and moving handles) the point is
        located, point should be q QPoint """
        tol, mingrip = 0.05, 5 # tol relative to widget size, mingrip is abs min size
        left = max(mingrip,self.width()*tol)
        right = self.width() - left
        top = max(mingrip,self.height()*tol)
        bottom = self.height() - top
        if point.x() <= left:
            if point.y() <= top: return self.FLOATING_TOPLEFT
            if point.y() >= bottom: return self.FLOATING_BOTTOMLEFT
            else: return self.FLOATING_LEFT
        if point.x() >= right:
            if point.y() <= top: return self.FLOATING_TOPRIGHT
            if point.y() >= bottom: return self.FLOATING_BOTTOMRIGHT
            else: return self.FLOATING_RIGHT
        if point.y() <= top: return self.FLOATING_TOP
        if point.y() >= bottom: return self.FLOATING_BOTTOM
        return self.FLOATING_OTHER

    def enterEvent(self, e):
        self.setMouseTracking(True)
        self._orig_cursor = self.cursor()
        self._setHighlight()

    def leaveEvent(self,e):
        self.setCursor(self._orig_cursor)
        self._unsetHighlight()

    def moveEvent(self,e):
        """ remembers relative pos inside parent whenever moved """
        # e.pos() here is in parent coordinates
        pc = self.mapToParent(QPoint(int(self.width()/2.0),int(self.height()/2.0)))
        xmax = self.parent().width() - self.width()
        ymax = self.parent().height() - self.height()
        self._relative_pos_x = float(pc.x()) / float(self.parent().width())
        self._relative_pos_y = float(pc.y()) / float(self.parent().height())

    def ensureInParent(self):
        """ to ensure self stays inside the parent widget """
        xmax = self.parent().width() - self.width()
        ymax = self.parent().height() - self.height()
        # initialise the desirable position, stays the same relatively
        pc = QPoint(self.width()/2.0, self.height()/2.0)
        p = QPoint(self._relative_pos_x * self.parent().width(),
                   self._relative_pos_y * self.parent().height()) - pc
        # if outside any boundary of parent, bring self back
        if p.x() < 0: p.setX(0)
        if p.y() < 0: p.setY(0)
        if p.x() > xmax: p.setX(xmax)
        if p.y() > ymax: p.setY(ymax)
        self.move(p)

class FloatingParentWidgetBase(object):
    """ use this as base class for parent of a floating child widget """
    def resizeEvent(self,e):
        """ informs floating children to ensire stay inside parent """
        for c in self.children():
            try:
                c.ensureInParent()
            except:
                pass

class TestChildWidget(FloatingChildWidgetBase,QLabel):
    def __init__(self, parent=None):
        super(TestChildWidget, self).__init__(parent)
        self.setText('TEST TEST\nTEST TEST')
    def _setHighlight(self):
        self._orig_frame_style = self.frameStyle()
        self._orig_line_width = self.lineWidth()
        self.setFrameStyle(QFrame.Panel | QFrame.Plain)
        self.setLineWidth(1)
    def _unsetHighlight(self):
        self.setFrameStyle(self._orig_frame_style)
        self.setLineWidth(self._orig_line_width)

class TestParentWidget(FloatingParentWidgetBase,QGraphicsView):
    def __init__(self, parent=None):
        super(TestParentWidget, self).__init__(parent)

if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)

    parent = TestParentWidget()
    child = TestChildWidget(parent)

    parent.show()

    app.exec_()

