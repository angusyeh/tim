"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

"""@package docstring
Common graph related base widgets.

"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

import logging
import settings

class MplCanvas(FigureCanvas):
    """A basic widget of PyQt-matplotlib figure.

    """
    def __init__(self, parent):
        self.fig = Figure(facecolor='w', edgecolor='w')
        # matplotlib: axes.hold is deprecated
        # self.fig.gca().hold(False)
        super(MplCanvas, self).__init__(self.fig)
        self.setParent(parent)

        # QSizePolicy.Preferred, QSizePolicy.Expanding
        FigureCanvas.setSizePolicy(self, QSizePolicy.Expanding, QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)
        self.clear_fig()

    def clear_fig(self):
        """Empty the plot, show only blank.

        This should make the plot showing nothing at all.  All data will be
        be cleared as well.  Returns the current axes, which allows external
        control of the axes.
        """
        self.fig.gca().clear()
        self.fig.gca().axis('off')
        self.draw()


def not_number(alist):
    """check if a list is of numeric or not, but count empty list as numeric.
    Also only checking 1st element, used only for nice lists."""
    if len(alist) == 0:
        return False
    try:
        float(alist[0])
        return False
    except ValueError:
        return True


def set_log_scale(axes, plot_type='linear'):
    """ sets/changes a matplotlib's axes log scale """
    if plot_type == 'linear':
        axes.set_xscale('linear')
        axes.set_yscale('linear')
    elif plot_type == 'semilogx':
        d = {'basex': 10,
             'subsx': None,
             'nonposx': 'mask',
             }
        axes.set_xscale('log', **d)
        axes.set_yscale('linear')
        # achive .ticklabel_format(useOffset=False) for semi log scale
        axes.set_yticklabels(axes.get_yticks())
    elif plot_type == 'semilogy':
        d = {'basey': 10,
             'subsy': None,
             'nonposy': 'mask',
             }
        axes.set_yscale('log', **d)
        axes.set_xscale('linear')
        # achive .ticklabel_format(useOffset=False) for semi log scale
        axes.set_xticklabels(axes.get_xticks())
    elif plot_type == 'loglog':
        dx = {'basex': 10,
              'subsx': None,
              'nonposx': 'mask',
              }
        dy = {'basey': 10,
              'subsy': None,
              'nonposy': 'mask',
              }
        axes.set_xscale('log', **dx)
        axes.set_yscale('log', **dy)
    else:
        raise Exception('Plot scale type %s not supported in set_log_scale()' % plot_type)

class LinePlotWidget(MplCanvas):
    """A convenience graphing widget, used for line plots.

    Controlling the figure axes by calling methods of self.fig.gca().  See
    online documentation on: http://matplotlib.org/api/axes_api.html

    The main difference from the standard matplotlib ploting is the ease of
    add/update/remove line by the line's key.  Also the line style/color/markers
    automatically cycles a preset that is similar to default gnuplot.
    """
    def __init__(self, parent=None):
        super(LinePlotWidget, self).__init__(parent)
        self._plot_type = 'linear'
        self.empty() # to show nothing at all, overwrite default empty axis

        # setup context menu functions
        from functools import partial
        self._menu = QMenu(self)
        self._menu._slots, self._menu._actions = [], []
        self._menu._group = QActionGroup(self._menu)
        for t in self.supportedPlotTypes():
            self._menu._slots.append(partial(self.setPlotType, t))
            act = self._menu.addAction(t, self._menu._slots[-1])
            act.setCheckable(True)
            self._menu._actions.append(act)
            self._menu._group.addAction(act)
            if t == self.getPlotType():
                act.setChecked(True)
        # menu for saving figure
        self._menu.addSeparator()
        self._menu.addAction("Export figure as image", self.saveFigure)

    def redraw(self):
        set_log_scale(self.fig.gca(), self._plot_type)
        self.fig.gca().relim()
        self.fig.gca().autoscale_view()
        self.fig.gca().grid(True)
        if self._plot_type == 'linear':
            self.fig.gca().ticklabel_format(useOffset=False)
        if self.fig.gca().lines:
            self.fig.gca().legend(loc='best',ncol=2)
            self.fig.gca().legend().draggable()
        else:
            # ugly work-around due to .legend() returns None. Any better way?
            self.fig.gca().legend(()).set_visible(False)
        self.draw()

    def setPlotType(self, plot_type='linear'):
        if plot_type in self.supportedPlotTypes():
            self._plot_type = plot_type
            self.redraw()
        else:
            logging.error('Plot scale type %s not supported in set_log_scale()' % plot_type)
            logging.error("Plot's scale type is still %s" % self._plot_type)

    def supportedPlotTypes(self):
        """ Gives supported scale types, useful for provide menu options """
        return ['linear', 'semilogx', 'semilogy', 'loglog']

    def getPlotType(self):
        return self._plot_type

    def lineCount(self):
        """ reports how many lines are there in the plot """
        return len(self.fig.gca().get_lines())

    def resetStyle(self):
        """reset the cycling line styles use for lines."""
        from itertools import cycle
        self._linestyles = cycle(['-', '--', ':'])
        self._linecolors = cycle(['r','b','g','c','m','y','k'])
        self._linemarkers = cycle(['+','*','D','o','x','^','s','h','2','v'])

    def empty(self):
        """Empty the plot, show only blank.

        This should make the plot showing nothing at all.  All data will be
        be cleared as well.  Returns the current axes, which allows external
        control of the axes.
        """
        self.resetStyle()
        self.fig.gca().clear()
        self.fig.gca().axis('off')
        self.redraw()
        return self.fig.gca()

    def setLabels(self,xlabel='x',ylabel='y',title=''):
        """Sets the labels of plot.

        It seems to work only after plot().  To be checked.
        """
        #self.fig.gca().axis('on')
        if xlabel:
            self.fig.gca().set_xlabel(str(xlabel))
        if ylabel:
            self.fig.gca().set_ylabel(str(ylabel))
        if title:
            self.fig.gca().set_title(str(title))
        self.redraw()


    def addLine(self,x,y,name):
        """Add an x,y series into the plot."""
        if not_number(x) or not_number(y):
            print 'Error, cannot plot x, y that is not number.'
            x,y = [], []
        if self.lineCount():
            import matplotlib.lines
            line = matplotlib.lines.Line2D(x,y,
                label=name,
                linestyle=self._linestyles.next(),
                color=self._linecolors.next(),
                marker=self._linemarkers.next(),
                #markersize=5,
                )
            self.fig.gca().add_line(line)
        else:
            # if no existing lines yet, plot() return a list of lines
            line = self.fig.gca().plot(x,y,
                label=name,
                linestyle=self._linestyles.next(),
                color=self._linecolors.next(),
                marker=self._linemarkers.next(),
                #markersize=5,
                )[0]
            self.fig.gca().axis('on')
        self.redraw()

    def updateLine(self,i,x=None,y=None,name=None):
        """Update either of xdata, ydata, or label of an existing line in the
        plot."""
        if 0 <= i < self.lineCount():
            line = self.fig.gca().get_lines()[i]
            xo, yo = line.get_data()
            no = line.get_label()

            if x is None: x = xo
            if y is None: y = yo
            if name is None: name = no

            if not_number(x) or not_number(y):
                print 'Error, cannot plot x, y that is not number.'
                x,y = [], []

            self.fig.gca().lines[i].set_data(x,y)
            self.fig.gca().lines[i].set_label(name)
            self.redraw()

    def removeLine(self,i):
        """Remove a single line, if index is not correct, nothing will be done.
        """
        if 0 <= i < self.lineCount():
            self.fig.gca().lines.remove(self.fig.gca().lines[i])
            self.redraw()

    def saveFigure(self, fname=''):
        supported_iamge_format = ';;'.join([
           "Portable Network Graphics (*.png)",
           "Portable Document Format (*.pdf)",
           "Encapsulated Postscript Vector Graphics (*.eps)",
           "PostScript (*.ps)",
           "Scalable Vector Graphics (*.svg)",
           "Compressed Scalable Vector Graphics (*.svgz)",
           ])
        if not fname:
            fname = QFileDialog.getSaveFileName(self,
                caption="Save Figure as File",
                directory=settings._plotting_path,
                filter=supported_iamge_format,
                selectedFilter=supported_iamge_format[2],
                )
        if not fname.isEmpty():
            settings._plotting_path = QFileInfo(fname).path()
            try:
                self.fig.savefig(str(fname))
            except Exception as e:
                logging.error("Failed to save plot.")
                logging.error(str(e))

    def contextMenuEvent(self, event):
        """ Pop up context menu """
        for i,t in enumerate(self.supportedPlotTypes()):
            if t == self.getPlotType():
                self._menu._actions[i].setChecked(True)
        self._menu.exec_(event.globalPos())

    # should I do this in the LinePlotWidget class so that the mouse press event can be passed on?
    # def mousePressEvent(self,e):
    #     """ allow parent to receive mousePressEvent(), otherwise will be absorbed
    #     and do nothing """
    #     if self.parent():
    #         self.parent().mousePressEvent(e)

if __name__ == '__main__':
    import unittest
    class TestGraphWidgets(unittest.TestCase):
        def test_LinePlot(self):
            import sys
            app = QApplication(sys.argv)
            # -----
            win = LinePlotWidget()

            win.addLine([1.,2.,2.7,3.],[2.0,5.0,8.0,7.0],'1st Line')
            self.assertEqual(win.lineCount(), 1,
                msg='should have one line after adding one to an empty fig')

            win.addLine([1.3,2.5,2.9],[2.0,5.0,9.0],'2nd Line')
            win.addLine([1.1,2.1,2.5],[2.1,5.1,9.1],'3rd Line')
            win.updateLine(0,[1.,2.,2.7,3.],[7.0,8.0,5.0,2.0],'1st Line')
            win.removeLine(1)
            win.setLabels('this is x','y title','main title')

            self.assertEqual(win.lineCount(), 2,
                msg='after adding 3 lines, removing 1 line, should have 2 lines left')
            # -----
            win.show()
            app.exec_()

            win.empty()
            self.assertEqual(win.lineCount(), 0,
                msg='should have zero line after empty')

    unittest.main()

