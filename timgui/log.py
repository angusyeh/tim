"""
This deals with things the whole application needs, including logging and
critical error handling.

Upon import, root logger will be setup so to write to both console and a log
file.  Because Qt event loop will continue even if exception is unhandled
without informing users obvisously, sys exception is hooked to use logger.

Call module function getLogFilename() to obtain the location of log file.  The
file should be the application's launch directory.
"""

"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""


#
# Logging
#
import logging
import sys
import os

def initLogger():
    """ initialise python built-in logging module for global use """
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    # to console
    con_formatter = logging.Formatter("-%(levelname)-10s %(message)s")
    con = logging.StreamHandler()
    con.setLevel(logging.INFO)
    con.setFormatter(con_formatter)
    logger.addHandler(con)

    # to log file
    logf_formatter = logging.Formatter("-%(levelname)-10s %(message)s\n"\
        "            %(filename)s:%(lineno)d")
    logf = logging.FileHandler('tim.log')
    logf.setLevel(logging.DEBUG)
    logf.setFormatter(logf_formatter)
    logger.addHandler(logf)

def logAppInfo():
    """ record application version info """
    import info
    logging.info('%s %s' % (info.long_name, info.version))
    if info.long_version:
        logging.info('Source: %s' % info.long_version)
    logging.info('Python executable: %s' % sys.executable)
    logging.info('Script location: %s' % sys.path[0])
    logging.info('Current working directory: %s' % os.getcwd())

initLogger()
logAppInfo()

def getLogFilename():
    # TODO: make this more robust
    logger = logging.getLogger()
    return logger.handlers[1].baseFilename

#
# system exception handling, log uncatched exceptions
#

def handleSysException(etype, evalue, etraceback):
    """ used to replace the default handling of exception, write to logging as
    well """
    logging.critical('Unhandled Exception! This is embarassing, please contact us!',
        exc_info=(etype, evalue, etraceback))

sys.excepthook = handleSysException

