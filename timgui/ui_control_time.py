"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from units import Unit, defaultUnit, timeOffset

class Ui_TimeControl(QWidget):
    def __init__(self, parent=None):
        super(Ui_TimeControl, self).__init__(parent)
        self._model = None

        self._prev_button = QPushButton(QIcon(":/prev.png"),'')
        self._next_button = QPushButton(QIcon(":/next.png"),'')
        self._first_button = QPushButton(QIcon(":/first.png"),'')
        self._last_button = QPushButton(QIcon(":/last.png"),'')
        self._slider = QSlider()
        self._slider.setOrientation(Qt.Horizontal)
        self._slider.setTickPosition(QSlider.TicksBelow)
        self._label = QLabel()
        self._label_i = QLabel()

        self.default_unit_button = QPushButton('Set Initial Time')
        self.default_unit_button.setEnabled(False)

        self._prev_button.setToolTip("Previous Step")
        self._next_button.setToolTip("Next Step")
        self._first_button.setToolTip("First Step")
        self._last_button.setToolTip("Last Step")

        self.connect(self._prev_button,SIGNAL("clicked ()"),self.prev)
        self.connect(self._next_button,SIGNAL("clicked ()"),self.next)
        self.connect(self._first_button,SIGNAL("clicked ()"),self.first)
        self.connect(self._last_button,SIGNAL("clicked ()"),self.last)

        # layout
        layout_slider = QBoxLayout(QBoxLayout.LeftToRight)
        layout_slider.addWidget(self._label_i)
        layout_slider.addWidget(self._first_button)
        layout_slider.addWidget(self._prev_button)
        layout_slider.addWidget(self._slider)
        layout_slider.addWidget(self._next_button)
        layout_slider.addWidget(self._last_button)

        layout_left = QBoxLayout(QBoxLayout.TopToBottom)
        layout_left.addWidget(self.default_unit_button,0,Qt.AlignLeft)
        layout_left.addWidget(self._label,0,Qt.AlignTop)

        layout = QBoxLayout(QBoxLayout.LeftToRight)
        layout.addLayout(layout_left,1)
        layout.addLayout(layout_slider,5)
        self.setLayout(layout)

        self.connect(self._slider,
            SIGNAL("valueChanged (int)"),
            self._changeListingStep)
        self.connect(self._slider,
            SIGNAL("sliderMoved (int)"),
            self._updateLabel)

        self.connect(self.default_unit_button, SIGNAL("clicked ()"), self._promptUnitControl)

    #lydia
    def _promptUnitControl(self):
        #TODO: change this to only work once things have been loaded? or maybe don't show button until then?
        self.emit(SIGNAL("show_units_control"))


    def _updateLabel(self):
        if self._model.lst():
            steptime = self._model.lst().fulltimes[self._slider.sliderPosition()]
            ss = steptime

            #set message displaying simulation time
            dispUnit = defaultUnit('second')
            if Unit(dispUnit) != Unit('second'):
                sim_time = Unit.Quantity(ss,'second').to(dispUnit).magnitude
                if dispUnit[-1] != 's':
                    dispUnit += 's'
                sim_time_msg = 'Time since simulation beginning: %.2f seconds = %.2f %s' % (ss,sim_time,dispUnit)
            else:
                sim_time_msg = 'Time since simulation beginning: %.2f seconds' % ss

            #if a time offset has been set show the current 'real time'
            if timeOffset() != 0.0 :
                date = Unit.Quantity(ss,'t2sec').to('year').magnitude
                date_msg = 'Date: %.2f (years)' % date
            else:
                date_msg = ''

            self._label.setText('\n'.join([date_msg,sim_time_msg]))
            self._label_i.setText('(Step %i/%i)' % (self._slider.sliderPosition()+1,self._model.lst().num_fulltimes))

        else:
            self._label.setText('')
            self._label_i.setText('')

    def updateLabel(self):
        self._updateLabel()

    def _changeListingStep(self,idx):
        if self._model.lst():
            # show busy, probably need to find a better way
            self.setCursor(QCursor(Qt.WaitCursor))
            self._label.setText("Loading...")

            QApplication.instance().processEvents() #necessary to flush event queue and make label actually change to "loading"

            print 'loading time step %i...' % (idx + 1)
            self._model.changeListingStep(idx)
            print 'done.'

            self._updateLabel()
            self.unsetCursor()

            ### from qobject class reference: To avoid never ending notification loops you can temporarily block signals with blockSignals().

    def first(self):
        if self._model.lst():
            if self._model.lst().index == 0: return
            self._slider.setValue(0) #use setValue rather than self._changeListingStep so that _changeListingStep is not called twice unnecessarily

    def last(self):
        if self._model.lst():
            if self._model.lst().index == (self._model.lst().num_fulltimes - 1): return
            self._slider.setValue(self._model.lst().num_fulltimes - 1)

    def next(self):
        if self._model.lst():
            cur = self._model.lst().index
            if cur == (self._model.lst().num_fulltimes - 1): return
            self._slider.setValue(cur + 1)

    def prev(self):
        if self._model.lst():
            cur = self._model.lst().index
            if cur == 0: return
            self._slider.setValue(cur - 1)


    def _updateSlider(self):
        """ update the maximum value of slider, usually needed when t2listing
        is reloaded """
        if self._model.lst():
            lst = self._model.lst()
            self._slider.setValue(lst.index)
            self._slider.setMaximum(lst.num_fulltimes-1)
            self._slider.setMinimum(0)
            self._slider.setSingleStep(1)
            self._slider.setTracking(False)
            self.default_unit_button.setEnabled(True)
        else:
            self._slider.setMaximum(0)
            self._slider.setMinimum(0)
            self._slider.setSingleStep(1)
            self._slider.setTracking(False)
            self.default_unit_button.setEnabled(False)
        self._updateLabel()

    def _syncSliderValue(self,idx):
        """ slider to reflect model """
        if self._model.lst():
            self._slider.setValue(self._model.lst().index)

    def connectModel(self,model):
        if self._model:
            # disconnect
            self.disconnect(self._model,SIGNAL("model_lst_reloaded"),self._updateSlider)
            self.disconnect(self._model,SIGNAL("model_lst_step_changed"),self._syncSliderValue)
        self._model = model
        self.connect(self._model,SIGNAL("model_lst_reloaded"),self._updateSlider)
        self.connect(self._model,SIGNAL("model_lst_step_changed"),self._syncSliderValue)

if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    form = Ui_TimeControl()
    form._label_i.setText('(Step 18/67)')
    form._label.setText('Date: 17.00 (years)\nTime since simulation beginning: 536486000.00 seconds = 17.00 years')

    #form.accepted.connect(updateData)
    #form.testing()
    form.show()
    app.exec_()
