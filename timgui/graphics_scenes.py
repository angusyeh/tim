"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *

import logging

# graphics scene/items
from graphics_items import TimPolygonItem, TimTextItem, TimArrowItem
from graphics_items import FixedSizePoint

from colorbar_and_scale import myColorBar, Scale
from t2model import Model

import numpy as np
from units import Unit

mv_NO_INDEX = False

MIN_MARQUEE_RATIO = 0.02

class MyScene(QGraphicsScene):
    """ this class is responsible for most creation and manipulation of
    selection, polyline etc. connect to selection/line manager"""
    BROWSE_MODE = 0
    MULTI_SELECTION_MODE = 1
    SINGLE_SELECTION_MODE = 2
    MARQUEE_SELECTION_MODE = 3
    LINE_CREATION_MODE = 4
    WALL_CREATION_MODE = 5
    MARQUEE_ZOOM_MODE = 6
    PAINT_ROCK_MODE = 7
    def __init__(self,parent=None):
        super(MyScene, self).__init__(parent)
        if mv_NO_INDEX:
            self.setItemIndexMethod(QGraphicsScene.NoIndex)

        self._mode = self.LINE_CREATION_MODE
        self._prev_mode = self.MULTI_SELECTION_MODE
        # for line creation, use empty [] instead of None
        self._polyline_pts = []                # a list of QPointF
        self._polyline_items = []        # QtGraphicsItems, dots and lines
        # the line that follows the mouse
        self._live_line_item = None # QtGraphicsLineItem, None if not tracking
        # the marquee/rectangle follows the mouse
        self._live_marquee_item = None
        # for silencing mouse moving message
        self._silence = False

        # ugly, for correctly remembering if is single selection mode
        self._is_single_mode = False

    def isSingleSelectionMode(self):
        # ugly, for correctly remembering if is single selection mode
        return self._is_single_mode

    def currentMode(self):
        return self._mode

    def setMode(self,mode):
        """ changing from current to new mode, current mode stored as
        self._prev_mode, signal emitted when done """
        if mode == self._mode: return

        # There are roughly two kinds of modes:
        # - persistent mode, a general mode that user usually stays with for a while
        # - tool mode, generally used it once, unset when finished with

        if self._mode == self.LINE_CREATION_MODE:
            self._liveLineStop()

        # current mode wil be remembered, apart from those listed in tool_modes
        tool_modes = [self.LINE_CREATION_MODE, self.MARQUEE_SELECTION_MODE, self.MARQUEE_ZOOM_MODE]
        if self._mode not in tool_modes:
            self._prev_mode = self._mode

        # ugly, for correctly remembering if is single selection mode
        if mode is self.SINGLE_SELECTION_MODE:
            self._is_single_mode = True
        if mode is self.MULTI_SELECTION_MODE:
            self._is_single_mode = False

        self._mode = mode
        self.emit(SIGNAL("scene_mode_changed"), self._mode)
        return self._mode

    def unsetMode(self):
        """ returns the scene's mode after this method call """
        return self.setMode(self._prev_mode)

    def mousePressEvent(self, e):
        if (e.button() == Qt.MidButton) or (self._mode == self.BROWSE_MODE):
            self._silence = True
            for v in self.views():
                v.panStart(v.mapFromScene(e.scenePos()))

        if self._mode == self.MULTI_SELECTION_MODE:
            if e.button() == Qt.LeftButton:
                items = self._modelItemsOnly(self.items(e.scenePos()))
                for i in items:
                    i.setSelected(not i.isSelected())
                    self.emit(SIGNAL("scene_multi_selected"),i.name,i.objType,i.isSelected())
        elif self._mode == self.SINGLE_SELECTION_MODE:
            if e.button() == Qt.LeftButton:
                self.clearSelection()
                items = self._modelItemsOnly(self.items(e.scenePos()))
                for i in items:
                    i.setSelected(True)
                    self.emit(SIGNAL("scene_single_selected"),i.name,i.objType)
        elif self._mode == self.LINE_CREATION_MODE:
            if e.button() == Qt.RightButton:
                self._liveLineStop()
                self.unsetMode()
            elif e.button() == Qt.LeftButton:
                self._polylineAddPoint(e.scenePos())
                self._liveLineStart(e.scenePos())
        elif self._mode == self.MARQUEE_ZOOM_MODE:
            if e.button() == Qt.LeftButton:
                self._liveMarqueeStart(e.scenePos())
            elif e.button() == Qt.RightButton:
                for v in self.views():
                    v.zoomOut()

    def mouseMoveEvent(self, e):
        # this is needed for hover to work
        super(MyScene, self).mouseMoveEvent(e)

        if self._live_line_item is not None:
            self._liveLineUpdate(e.scenePos())
        if self._live_marquee_item is not None:
            self._liveMarqueeUpdate(e.scenePos())
        # let view update live panning, don't do it here
        if self._silence: return
        # might not be the best place, although very 'central': all scenes will
        # have this automatically, and listening only needs to scenes, instead
        # of all items.  seems fast enough.
        pos_str = '%.2f, %.2f' % (e.scenePos().x(), e.scenePos().y())
        self.emit(SIGNAL("scene_message"), 'Coordinates',pos_str)
        on_items = []
        for itm in self.items(e.scenePos()):
            try:
                self.emit(SIGNAL("scene_message"), itm.objType,itm.name)
                on_items.append(itm.objType)
            except:
                pass
        for i_typ in ['Block','Connection','Well']:
            if i_typ not in on_items:
                self.emit(SIGNAL("scene_message"), i_typ,'')

    def mouseReleaseEvent(self, e):
        if (e.button() == Qt.MidButton) or (self._mode == self.BROWSE_MODE):
            for v in self.views():
                v.panStop()
            self._silence = False

        elif self._mode == self.MARQUEE_ZOOM_MODE:
            if self._live_marquee_item is not None:
                rect = self._liveMarqueeStop()
                s_rect = self.sceneRect()
                marq_tol = MIN_MARQUEE_RATIO * min(abs(s_rect.width()), abs(s_rect.height()))
                if max(abs(rect.width()), abs(rect.height())) > marq_tol:
                    for v in self.views():
                        v.fitInView(rect,Qt.KeepAspectRatio)
                else:
                    for v in self.views():
                        v.zoomIn()

    def mouseDoubleClickEvent(self, e):
        # make unfunctional, so mousePressEvent() as desired
        if self._mode == self.LINE_CREATION_MODE:
            self._liveLineStop()
            self.unsetMode()
        elif self._mode == self.BROWSE_MODE:
            if e.button() == Qt.LeftButton:
                self.emit(SIGNAL("browse_mode_double_clicked"))
        return

    def clearPolyline(self):
        """ clear polyline items (graphically) and pts (list of QPointF)"""
        for i in self._polyline_items:
            self.removeItem(i)
        self._polyline_items = []
        self._polyline_pts = []
        self._liveLineStop()

    def loadPolyline(self,polyline=[]):
        #if len(polyline) <= 1 : return # do not load if less than two point
        self.clearPolyline()
        for p in polyline:
            sp = QPointF(p[0],p[1])
            self._polylineAddPoint(sp)
        self.setMode(self.LINE_CREATION_MODE)

    def getPolyline(self):
        """ a list of numpy array (PyTOUGH style)"""
        # PyTOUGH: a list of np.array, each np.array is a point of x, y. eg.
        #    [ np.array([2775133.9, 6282885.4]),
        #      np.array([2780517.2, 6280577.0]),
        #      np.array([2780617.2, 6280077.0]) ]
        return [np.array([p.x(),p.y()]) for p in self._polyline_pts]

    def selectInsidePolygon(self):
        """ select all items inside currently loaded polygon when .objType == obj_type """
        # for some (still unknown) reason, QGraphicsScene.items() didn't work
        # properly, which didn't call QGraphicsItem.shape() at all.  Here use
        # Qt.IntersectsItemShape instead to (effeciently) narrow down itmes, and
        # then check with QGraphicsItem.collidesWithPath() with
        # Qt.ContainsItemShape to get final polygons.
        if self._polyline_pts:
            self.setMode(self.MULTI_SELECTION_MODE)
            path = QPainterPath()
            path.addPolygon(QPolygonF(self._polyline_pts))
            path.closeSubpath()
            for i in self._modelItemsOnly(self.items(path,Qt.IntersectsItemShape)):
                if i.collidesWithPath(path, Qt.ContainsItemShape):
                    n,t = i.name,i.objType # to make exception trigger if not model objects
                    i.setSelected(True)
                    self.emit(SIGNAL("scene_multi_selected"),n,t,i.isSelected())

    def selectIntersectPolyline(self):
        """ select all items (selectable model items) inside currently loaded polyline """
        if self._polyline_pts:
            def pairwise(iterable):
                """s -> (s0,s1), (s1,s2), (s2, s3), ... stops at last pair (lst[-1],
                lst[-2]).  return is an iterator"""
                from itertools import tee, izip
                a, b = tee(iterable)
                next(b, None)
                return izip(a, b)
            # compose list of QPainterPath to be checked intersection
            cross = []
            for pair in pairwise(self._polyline_pts):
                path = QPainterPath()
                path.addPolygon(QPolygonF(list(pair)))
                path.closeSubpath()
                cross.append(path)

            self.setMode(self.MULTI_SELECTION_MODE)
            for pp in cross:
                for i in self._modelItemsOnly(self.items(pp,Qt.IntersectsItemShape)):
                    n,t = i.name,i.objType # to make exception trigger if not model objects
                    i.setSelected(True)
                    self.emit(SIGNAL("scene_multi_selected"),n,t,i.isSelected())

    def _modelItemsOnly(self,items):
        """ given a list of QGraphicsItems, only returns model related items.
        The detection relies on the existence of .name and .objType properties, which
        should be defined by all GraphicsItems representing model objects. """
        return [i for i in items if hasattr(i,'name') and hasattr(i,'objType')]

    def _liveMarqueeStart(self,pos):
        """ activate/start a "live" marquee/rectangle """
        if self._live_marquee_item is not None:
            self._liveMarqueeStop()
        self._live_marquee_item = QGraphicsRectItem(QRectF(pos,pos))
        self.addItem(self._live_marquee_item)

    def _liveMarqueeUpdate(self,pos):
        """ update the "live" marquee/rectangle """
        if self._live_marquee_item is None: return
        rect = self._live_marquee_item.rect()
        rect.setBottomRight(pos)
        self._live_marquee_item.setRect(rect)

    def _liveMarqueeStop(self):
        """ stop the "live" marquee/rectangle, return a QRectF object """
        if self._live_marquee_item is None: return
        self.removeItem(self._live_marquee_item)
        rect = self._live_marquee_item.rect()
        self._live_marquee_item = None
        return rect

    def _liveLineStart(self,head_scene_pos):
        """ activate/start a "live" line with one end fixed at
        head_scene_pos, and the other end moving with mouse,
        head_scene_pos should be an QPointF object """
        if self._live_line_item is not None:
            self._liveLineStop()
        line = QLineF(head_scene_pos, head_scene_pos)
        self._live_line_item = QGraphicsLineItem(line)
        self.addItem(self._live_line_item)

    def _liveLineUpdate(self,scene_pos):
        """ update the moving end of the "live" line to scene_pos,
        scene_pos is a QPointF object.  usually this is called inside
        mouseMoveEvent() """
        if self._live_line_item is None: return
        line = self._live_line_item.line()
        line.setP2(scene_pos)
        self._live_line_item.setLine(line)

    def _liveLineStop(self):
        """ stop the live line, and cleanup, returns a QLineF object """
        if self._live_line_item is None: return
        self.removeItem(self._live_line_item)
        line = self._live_line_item.line()
        self._live_line_item = None
        return line

    def _polylineAddPoint(self,scene_pos):
        """ add points/line (for slice creation etc) """
        itemPoint = FixedSizePoint(scene_pos.x(), scene_pos.y(), 
            size=5.0, color='red')
        self.addItem(itemPoint)
        self._polyline_items.append(itemPoint)
        if len(self._polyline_pts) > 0:
            line = QLineF(self._polyline_pts[-1],scene_pos)
            itemLine = QGraphicsLineItem(line)
            self.addItem(itemLine)
            self._polyline_items.append(itemLine)
        self._polyline_pts.append(scene_pos)
        self.emit(SIGNAL("scene_polyline_updated"), self.getPolyline())

class SceneData(MyScene):
    """ this is a "scene" class that wraps around a QGraphicsScene and its
    data.  It mainly keeps the ref to actual QGrraphicsItems and handles the
    actual model data:
        - cache model blocks (those exist in the scene) values
        - cache model blocks texts
        - updates color and text if needed
        (similar things with flow arrows)
        (and maybe other things like well tracks etc are another 'layer')
    """
    BLOCK_ITEM, CONNE_ITEM, WELL_ITEM = 'Block', 'Connection', 'Well'
    def __init__(self,model):
        super(SceneData, self).__init__()
        self.name = ''
        self._model = model

        ## important, (almost) everything ordered exactly as these
        self._blocknames, self._blockindex = [], []
        self._conneindex = []

        ## keeping ref to actual QGraphicsItems
        self._polyitems = []    # same order as _blocknames / _blockindex
        self._textitems = []    # same order as _blocknames / _blockindex
        self._arrowitems = []   # same order as _conneindex
        # additional dict for access by name
        self._blockitems = {}
        self._blocktextitems = {}
        self._conneitems = {}

        ## caching and current settings
        # storage of values and texts, dictionary of list (val or txt)
        self._cacheColor, self._cacheText = {}, {}
        self._cacheArrow = {}
        # keep values np.array with units
        self._cacheBlockValues, self._cacheFlowValues = {}, {}
        # apply unit when getting data from model object
        self._toApplyUnitBlock, self._toApplyUnitFlow = {}, {}
        # current
        self._currentColorValName = 'No Fill'
        self._currentTextValName = 'No Text'
        self._currentArrowValName = 'No Arrow'
        self._currentColorBar = None
        self._currentTextFormatter = None
        self._currentArrowScale = None

        ## additional graphical items (only stored by names)
        # TODO: make these extensible, maybe also incl bookmarks/geners etc.
        self._wellitems = {}
        self._welltextitems = {}
        self._wellheaditems = {}
        # control well label position, head or tail
        self._wellpos = {}
        self._welltexthead = {}

        # extra visual items eg. wells, GIS objs
        self.item_group = {
            'well': QGraphicsItemGroup(),
            'well_text': QGraphicsItemGroup(),
            'gis': QGraphicsItemGroup(),
            'gis_text': QGraphicsItemGroup(),
        }
        for k,g in self.item_group.iteritems():
            g.setVisible(False)
            self.addItem(g)
            g.setZValue(150.0)

        ## completer for easy item access
        self._completer = None # QCompleter for QLineEdit
        self._item = None # reference to QGraphicsItems in scene
        # NOTE these needs to be initialised after the scene is actually setup

    def setupGraphicsPostponed(self, *args):
        """
        This creates an empty scene.  This is called first time when scenes are
        *created*.  The actual (CPU intensive) work is then done when user wants
        to see it.  .setupIfNotYet() should be called just before a scene is
        attached to a view.

        The arguments used when this method is called will be kept and used
        later when actually calling .setupGraphics().
        """
        self._already_setup = False
        self._setup_args = args

    def setupIfNotYet(self):
        """ Good idea to always call this before attach the scene to a view.
        Will call the actual .setupGraphics() method to create/insert graphics
        items.
        """
        if self._already_setup is False:
            self.setupGraphics(*self._setup_args)
            self._already_setup = True

    def setupGraphics(self,creator):
        """
        This must be implemented by subclass to set up all the required (visual)
        grapics items.

        Creator is information used to create the scene.  It contains layer
        index for layer plot, or line/track for slice, unfortunately the
        implementer needs to know what this creator is.

        Use methods ._addBlock(), ._addConnection(), ._addWell() in the
        implementation to fill in the standard graphics that formed the core
        feature of the application.
        """
        raise NotImplementedError("Please Implement this method")

    def completer(self):
        """ return a QCompleter includes all valid names in the scene """
        if self._completer is None:
            if self._item is None:
                self.setUpItemPos()
            tags = []
            for k, v in self._item.iteritems():
                if k == 'e':
                    # default, eleme, no prefix
                    tags += [iname for iname in v.keys()]
                else:
                    tags += [k + ':' + iname for iname in v.keys()]
            self._tags = tags
            self._completer = QCompleter(sorted(tags, key=lambda s: s.lower()))
            # self._completer.setCaseSensitivity(Qt.CaseSensitive)
            self._completer.setCaseSensitivity(Qt.CaseInsensitive)
            self._completer.setModelSorting(QCompleter.CaseInsensitivelySortedModel)
            # the default QCompleter.PopupCompletion mode interferes when user
            # types text not in the completion model, eg. 'gi:' for global search
            self._completer.setCompletionMode(QCompleter.UnfilteredPopupCompletion)
        return self._completer

    def setUpItemPos(self):
        """ create a dict for easy finding of item position """
        logging.debug('%s.setUpItemPos() called: %i block texts %i well texts' % (
            self.name, len(self._blocktextitems), len(self._welltextitems)))
        def wai_cell_idx(block_index):
            """ return Waiwera cell index (as str) from a standard block index
            (mulgrid) """
            return str(block_index - self._model.geo().num_atmosphere_blocks)
        self._item = {
            'e': {k:it.scenePos() for k,it in self._blocktextitems.iteritems()},
            'w': {k:it.scenePos() for k,it in self._welltextitems.iteritems()},
            # add Waiwera cell index as alias to find blocks
            'i': {wai_cell_idx(i):self._blocktextitems[b].scenePos() for b,i in zip(
                self._blocknames, self._blockindex)},
            }
        # additional alias for items without leading and trailing spaces
        for typ,its in self._item.iteritems():
            self._item[typ].update(
                {k.strip():v for k,v in its.iteritems() if k.strip() != k})

    def getItemPos(self, tag):
        """ return pos() of a named item of certain type, note tag should
        be one of the entry defined in completer() """
        if self._item is None:
            self.setUpItemPos()
        ts = tag.split(':')
        if len(ts) > 1:
            try:
                its = self._item[ts[0]]
            except KeyError:
                logging.error('item_type %s was not recognised.' % ts[0])
                return None
            name = ts[1]
        else:
            # if no prefix, assume it's eleme
            its = self._item['e']
            name = ts[0]
        try:
            return its[name]
        except KeyError:
            logging.error('Cannot find item %s in scene %s' % (name, self.name))
            return None

    def setSingleSelect(self,i_name,i_type):
        """ clear selection and only attempt to select specified """
        self.clearSelection()
        try:
            if i_type == SceneData.BLOCK_ITEM:
                self._blockitems[i_name].setSelected(True)
            elif i_type == SceneData.CONNE_ITEM:
                self._conneitems[i_name].setSelected(True)
            elif i_type == SceneData.WELL_ITEM:
                self._wellitems[i_name].setSelected(True)
        except KeyError:
            # do nothing, so can send set select to all scenes in broadcast fashion
            pass

    def setMultiSelectUpdate(self,i_name,i_type,sel):
        """ update selectionstatus of a single item to sel """
        try:
            if i_type == SceneData.BLOCK_ITEM:
                self._blockitems[i_name].setSelected(sel)
            elif i_type == SceneData.CONNE_ITEM:
                self._conneitems[i_name].setSelected(sel)
            elif i_type == SceneData.WELL_ITEM:
                self._wellitems[i_name].setSelected(sel)
        except KeyError:
            # do nothing, so can send set select to all scenes in broadcast fashion
            pass

    def setMultiSelectComplete(self,i_names,i_type):
        """ make a complete multiple selection, all cleared, only i_names selected """
        self.clearSelection()
        if i_type == SceneData.BLOCK_ITEM:
            its = self._blockitems
        elif i_type == SceneData.CONNE_ITEM:
            its = self._conneitems
        elif i_type == SceneData.WELL_ITEM:
            its = self._wellitems
        for name in i_names:
            try:
                its[name].setSelected(True)
            except KeyError:
                # do nothing, so can send set select to all scenes in broadcast fashion
                pass

    def toggleWellLabelPosition(self, pos=None, wells=['.....']):
        """ well label at head if True, at tail if False, toggle if None """
        ws = []
        import re
        patterns = [re.compile(name) for name in wells]
        ws = [w for w in self._wellitems.keys() if any([p.match(w) for p in patterns])]
        for w in ws:
            if pos is None:
                self._welltexthead[w] = not self._welltexthead[w]
            else:
                self._welltexthead[w] = pos
            # set position
            if self._welltexthead[w]:
                p = self._wellpos[w][0]
            else:
                p = self._wellpos[w][1]
            self._welltextitems[w].setPos(p[0], p[1])

    def setGroupVisible(self, visible=None, group=None):
        """ is visible is None, toggle visibility, if group is None, apply to
        all, otherwise apply to group with name match the group string """
        for k,g in self.item_group.iteritems():
            if group is None or group==k:
                if visible is None:
                    visible = not g.isVisible()
                g.setVisible(visible)

    # TODO: similar interface can be added into well list manager
    # def setWellVisible(self,visible,wells=['.....'],include=['Track','Head','Text']):
    #     ws = []
    #     import re
    #     patterns = [re.compile(name) for name in wells]
    #     ws = [w for w in self._wellitems.keys() if any([p.match(w) for p in patterns])]
    #     for w in ws:
    #         if 'Track' in include:
    #             self._wellitems[w].setVisible(visible)
    #         if 'Text' in include:
    #             self._welltextitems[w].setVisible(visible)
    #         if 'Head' in include:
    #             self._wellheaditems[w].setVisible(visible)

    # TODO: similar interface can be added into well list manager
    # def setWellColor(self,color,wells=['.....'],include=['Track','Head','Text']):
    #     if color is None:
    #         # PyQt4 4.11.4 no longer accepts QColor(None), so make default black
    #         # here and to prevent invalid QColor initialisation
    #         color = 'black'
    #     ws = []
    #     import re
    #     patterns = [re.compile(name) for name in wells]
    #     ws = [w for w in self._wellitems.keys() if any([p.match(w) for p in patterns])]
    #     for w in ws:
    #         if 'Track' in include:
    #             self._wellitems[w].setPen(QPen(QColor(color)))
    #         if 'Text' in include:
    #             self._welltextitems[w].setBrush(QBrush(QColor(color)))
    #         if 'Head' in include:
    #             self._wellheaditems[w].setBrush(QBrush(QColor(color)))

    def setWellTextSize(self, size):
        """ should do nothing if scene hasn't been setup, still no items """
        if self._welltextitems:
            t0 = self._welltextitems[self._welltextitems.keys()[0]]
            scale = size / t0.transform().m11()
            for t in self._welltextitems.itervalues():
                t.scale(scale, scale)

    def setBlockTextSize(self, size):
        """ should do nothing if scene hasn't been setup, still no items """
        if self._textitems:
            scale = size / self._textitems[0].transform().m11()
            for i,t in enumerate(self._textitems):
                t.scale(scale, scale)

    def clearColorCache(self,valName):
        """ clear the cache, and if the scene and color is currently shown, the
        scene will be updated immediately (which is very expensive).  Otherwise
        do not update, as a scene is being shown (set to view), it will
        automatically refresh itself.  """
        if valName in self._cacheColor:
            del self._cacheColor[valName]
            if valName == self._currentColorValName:
                if self.views():
                    self.updateColor(valName,self._currentColorBar)

    def clearTextCache(self,valName):
        """ similar to clearColorCache() """
        if valName in self._cacheText:
            del self._cacheText[valName]
            if valName == self._currentTextValName:
                if self.views():
                    self.updateText(valName, self._currentTextFormatter)

    def clearArrowCache(self,valName):
        """ similar to clearColorCache() """
        if valName in self._cacheArrow:
            del self._cacheArrow[valName]
            if valName == self._currentArrowValName:
                if self.views():
                    self.updateArrow(valName,self._currentArrowScale)

    def updateColor(self,valueName,colorBar):
        valName = str(valueName)
        if valName not in self._cacheColor:
            if valName == 'No Fill':
                # PyQt4 4.11.4 no longer accepts QColor(None), I am not sure why
                # the invalid color created by QColor() is different from what
                # QColor(None) used to give.
                self._cacheColor[valName] = [QColor('transparent')] * len(self._blocknames)
            else:
                bvals = self.getBlockValues(valName)
                try:
                    self._cacheColor[valName] = [colorBar.color(v.magnitude) for v in bvals]
                except AttributeError:
                    self._cacheColor[valName] = [colorBar.color(v) for v in bvals]
        for i in range(len(self._blocknames)):
            self._polyitems[i].setBrush(QBrush(self._cacheColor[valName][i]))
        self._currentColorValName = valName
        self._currentColorBar = colorBar

    def updateText(self, valueName, formatter=None):
        font = QFont()
        font.setPointSize(20)
        def to_string(vs):
            # assumes vs is np.array WITHOUT unit
            if np.issubdtype(vs.dtype, np.float_) and formatter is not None:
                vvs = [formatter.format(v) for v in vs]
            else:
                vvs = [str(v) for v in vs]
            return vvs

        valName = str(valueName)
        if valName not in self._cacheText:
            if valName == 'No Text':
                self._cacheText[valName] = ['' for b in self._blocknames]
            else:
                bvals = self.getBlockValues(valName)
                try:
                    bvals = bvals.magnitude
                except AttributeError:
                    pass
                self._cacheText[valName] = to_string(bvals)
        for i in range(len(self._blocknames)):
            self._textitems[i].setText(self._cacheText[valName][i])
            self._textitems[i].setFont(font)
            #self._textitems[i].setPos(self._textitems[i].boundingRect().center())
        self._currentTextValName = valName
        self._currentTextFormatter = formatter

    def updateArrow(self,valueName,scale):
        valName = str(valueName)
        if valName not in self._cacheArrow:
            if valName == 'No Arrow':
                self._cacheArrow[valName] = [0. for c in self._conneindex]
            else:
                fvals = self.getFlowValues(valName)
                try:
                    self._cacheArrow[valName] = [v.magnitude*scale.val for v in fvals]
                except AttributeError:
                    self._cacheArrow[valName] = [v*scale.val for v in fvals]
        for i in range(len(self._conneindex)):
            self._arrowitems[i].setLength(self._cacheArrow[valName][i])
        self._currentArrowValName = valName
        self._currentArrowScale = scale

    def clearBlockCache(self, valName):
        """ Call this when the cached block values no longer matches the source
        data in model object.  Dependent color and text cache will be cleared
        also.
        """
        valName = str(valName)
        if valName in self._cacheBlockValues:
            if hasattr(self._cacheBlockValues[valName], 'units'):
                # save previously used units, some _cacheBlockValues (like
                # rocktype texts) may not have .units
                self._toApplyUnitBlock[valName] = self._cacheBlockValues[valName].units
            del self._cacheBlockValues[valName]
            self.clearColorCache(valName)
            self.clearTextCache(valName)

    def clearFlowCache(self, valName):
        """ Call this when the cached flow values no longer matches the source
        data in model object.  Dependent arrow cache will be cleared also.
        """
        valName = str(valName)
        if valName in self._cacheFlowValues:
            del self._cacheFlowValues[valName]
            self.clearArrowCache(valName)

    def getBlockValues(self, valName):
        """ Returns the scene's BLOCK values of valName.  This will be from
        self._model.getData(), recalling that data caching (values, not
        converted colors/texts) is done inside model data, not scene itself. """
        valName = str(valName)
        if valName not in self._cacheBlockValues:
            data = self._model.getData(Model.BLOCK,valName)
            unit = self._model.getDataUnit(Model.BLOCK,valName)
            try:
                self._cacheBlockValues[valName] = data[self._blockindex] * Unit(unit)
                if valName in self._toApplyUnitBlock:
                    self._cacheBlockValues[valName].ito(self._toApplyUnitBlock[valName])
                    del self._toApplyUnitBlock[valName]
            except TypeError:
                # Pint unit does not support this type, probably strings
                self._cacheBlockValues[valName] = data[self._blockindex]
        return self._cacheBlockValues[valName]

    def getFlowValues(self, valName):
        valName = str(valName)
        if valName not in self._cacheFlowValues:
            data = self._model.getData(Model.FLOW,valName)
            unit = self._model.getDataUnit(Model.FLOW,valName)
            try:
                self._cacheFlowValues[valName] = data[self._conneindex] * Unit(unit)
                if valName in self._toApplyUnitFlow:
                    self._cacheFlowValues[valName].ito(self._toApplyUnitFlow[valName])
                    del self._toApplyUnitFlow[valName]
            except TypeError:
                # Pint unit does not support this type, probably strings
                self._cacheFlowValues[valName] = data[self._conneindex]
        return self._cacheFlowValues[valName]

    def getBlockValueLimit(self,valName,return_bname=False):
        """ get limits (min,max) of a scene's block values, if return_bname
        is True, it will return block names of these instead. """
        vname = str(valName)
        if vname == 'No Fill':
            values = [None for b in self._blocknames]
        else:
            values = self.getBlockValues(vname)
        # get both max/min values and indices
        maxv,maxi = max( (v, i) for i, v in enumerate(values) )
        minv,mini = min( (v, i) for i, v in enumerate(values) )
        if return_bname:
            return (self._blocknames[mini],self._blocknames[maxi])
        else:
            return (minv,maxv)

    def changeBlockUnit(self, valName, newunit):
        """ This will change the unit of the cached block values, dependent
        color and text cache will be cleared also.  If valName not obtained
        (from model) yet, the unit will be applied upon .getData().
        """
        valName = str(valName)
        if valName in self._cacheBlockValues:
            if hasattr(self._cacheBlockValues[valName], 'units'):
                self._cacheBlockValues[valName].ito(newunit)
                self.clearColorCache(valName)
                self.clearTextCache(valName)
            else:
                logging.warning('Variable %s does not support units.' % valName)
        else:
            self._toApplyUnitBlock[valName] = newunit

    def changeFlowUnit(self, valName, newunit):
        """ This will change the unit of the cached flow values, dependent arrow
        cache will be cleared.  If valName not obtained (from model) yet, the
        unit will be applied upon .getData().
        """
        valName = str(valName)
        if valName in self._cacheFlowValues:
            if hasattr(self._cacheFlowValues[valName], 'units'):
                self._cacheFlowValues[valName].ito(newunit)
                self.clearArrowCache(valName)
            else:
                logging.warning('Variable %s does not support units.' % valName)
        else:
            self._toApplyUnitFlow[valName] = newunit

    def changeTextFormat(self, valName, newformat):
        """
        This will change the precision of the cached value texts, dependent
        text cache will be cleared.  If valName not obtained (from model) yet,
        the unit will be applied upon .getData().
        """
        valName = str(valName)
        if valName in self._cacheText:
            self.clearTextCache(valName)
        else:
            self._currentTextFormatter = newformat

    def getBlockUnit(self, valName):
        """ If the variable does not support units (eg. strings such as
        RockType), None will be returned.
        """
        valName = str(valName)
        if valName in self._cacheBlockValues:
            if hasattr(self._cacheBlockValues[valName], 'units'):
                u = str((self._cacheBlockValues[valName]).units)
                if u == 'dimensionless':
                    return ''
                else:
                    return u
        else:
            if valName in self._toApplyUnitBlock:
                return self._toApplyUnitBlock[valName]
        return None

    def getFlowUnit(self, valName):
        """ If the variable does not support units, None will be returned.
        """
        valName = str(valName)
        if valName in self._cacheFlowValues:
            if hasattr(self._cacheFlowValues[valName], 'units'):
                u = str((self._cacheFlowValues[valName]).units)
                if u == 'dimensionless':
                    return ''
                else:
                    return u
        else:
            if valName in self._toApplyUnitFlow:
                return self._toApplyUnitFlow[valName]
        return None

    def _addBlock(self,name,index,nodes,centre):
        self._blocknames.append(name)
        self._blockindex.append(index)

        polygon = QPolygonF()
        for node in nodes:
            polygon.append(QPointF(node[0], node[1]))
        itemBlk = TimPolygonItem(polygon)
        itemBlk.setAcceptsHoverEvents(True)
        itemBlk.objType = SceneData.BLOCK_ITEM
        itemBlk.name = name
        itemBlk.setZValue(0.0)

        self._polyitems.append(itemBlk)
        self._blockitems[name] = itemBlk

        # column names
        itemText = TimTextItem('',itemBlk) # make child, so hover not broken
        itemText.objType = SceneData.BLOCK_ITEM
        itemText.name = name
        itemText.setPos(centre[0],centre[1])
        itemText.setZValue(100.0)

        #self.addItem(itemText) # not need this if child
        self._textitems.append(itemText)
        self._blocktextitems[name] = itemText

        # faster to add item into scene at the end, after setPos()
        # this improved a bout 10%
        self.addItem(itemBlk) # TIME 30%

    def _addConnection(self,name,index,pfrom,pto):
        self._conneindex.append(index)

        pf = QPointF(pfrom[0], pfrom[1])
        pt = QPointF(pto[0], pto[1])
        itemArrow = TimArrowItem(pf,pt,0.)
        itemArrow.objType = SceneData.CONNE_ITEM
        itemArrow.name = name
        itemArrow.setZValue(50.0)

        self.addItem(itemArrow)
        self._arrowitems.append(itemArrow)
        self._conneitems[name] = itemArrow

    def _addWell(self,name,index,phead,wpos,tooltip=''):
        # well head, parents of the rest of well items, apart from text
        circle_r = 1.
        # itemCircle = QGraphicsEllipseItem(
        #     phead[0]-circle_r, phead[1]-circle_r,
        #     circle_r*2.0, circle_r*2.0)
        # old method will cause size to change as zoom in/out, see:
        # https://stackoverflow.com/questions/20125130/fixed-size-qgraphicsitems-in-multiple-views
        itemCircle = QGraphicsEllipseItem(
            0, 0,
            circle_r*2.0, circle_r*2.0)
        itemCircle.setPos(phead[0], phead[1])
        itemCircle.setFlag(QGraphicsItem.ItemIgnoresTransformations)
        itemCircle.setBrush(QBrush(QColor("black")))
        # well name, at well head to start with
        itemText = TimTextItem(name)
        itemText.setPos(phead[0],phead[1])
        # well track, child of itemCircle
        path = QPainterPath(QPointF(wpos[0][0],wpos[0][1]))
        for i in range(1,len(wpos)):
            path.lineTo(QPointF(wpos[i][0],wpos[i][1]))
        itemWell = QGraphicsPathItem(path)
        # add whole well
        itemWell.setVisible(True)
        itemWell.objType = SceneData.WELL_ITEM
        itemWell.name = name
        itemText.setVisible(True)
        itemText.objType = SceneData.WELL_ITEM
        itemText.name = name
        itemCircle.setVisible(True)
        itemCircle.objType = SceneData.WELL_ITEM
        itemCircle.name = name
        if tooltip:
            itemWell.setToolTip(tooltip)
            itemText.setToolTip(tooltip)
            itemCircle.setToolTip(tooltip)
        self.addItem(itemWell)
        self.addItem(itemText)
        self.addItem(itemCircle)
        # keep ref so can set visiility
        self._wellitems[name] = itemWell
        self._welltextitems[name] = itemText
        self._wellheaditems[name] = itemCircle
        # control well label position, head or tail
        self._wellpos[name] = (phead, wpos[-1]) # (head, tail)
        self._welltexthead[name] = True

        # add wells to GIS group
        self.item_group['well'].addToGroup(itemWell)
        self.item_group['well'].addToGroup(itemCircle)
        self.item_group['well_text'].addToGroup(itemText)

    def syncWellSetting(self, setting, default=None):
        """ apply visibility settings on well items.

        setting example:
        {
            'WK  1': {
                'Track': {'color': 'red', 'visible': True},
                'Text': {'color': 'orange', 'visible': False},
                'Head': {'color': 'green', 'visible': True},
            },
            'WK  2': {
                # no 'Track', use default
                'Text': {
                    # no color, use default
                    'visible': False
                },
                'Head': {'color': 'green', 'visible': True},
            },
        }
        """
        if default is None:
            default = {
                'Track': {'color': 'black', 'visible': True},
                'Text': {'color': 'black', 'visible': True},
                'Head': {'color': 'black', 'visible': True},
            }
        for wn in self._wellitems.keys():
            if wn in setting:
                w = setting[wn]
                if 'Track' in w:
                    if 'color' in w['Track']:
                        self._wellitems[wn].setPen(QPen(QColor(w['Track']['color'])))
                    else:
                        self._wellitems[wn].setPen(QPen(QColor(default['Track']['color'])))
                    if 'visible' in w['Track']:
                        self._wellitems[wn].setVisible(w['Track']['visible'])
                    else:
                        self._wellitems[wn].setVisible(default['Track']['visible'])
                if 'Text' in w:
                    if 'color' in w['Text']:
                        self._welltextitems[wn].setBrush(QBrush(QColor(w['Text']['color'])))
                    else:
                        self._welltextitems[wn].setBrush(QBrush(QColor(default['Text']['color'])))
                    if 'visible' in w['Text']:
                        self._welltextitems[wn].setVisible(w['Text']['visible'])
                    else:
                        self._welltextitems[wn].setVisible(default['Text']['visible'])
                if 'Head' in w:
                    if 'color' in w['Head']:
                        self._wellheaditems[wn].setBrush(QBrush(QColor(w['Head']['color'])))
                    else:
                        self._wellheaditems[wn].setBrush(QBrush(QColor(default['Head']['color'])))
                    if 'visible' in w['Head']:
                        self._wellheaditems[wn].setVisible(w['Head']['visible'])
                    else:
                        self._wellheaditems[wn].setVisible(default['Head']['visible'])
            else:
                # set as default
                self._wellitems[wn].setPen(QPen(QColor(default['Track']['color'])))
                self._wellitems[wn].setVisible(default['Track']['visible'])
                self._welltextitems[wn].setBrush(QBrush(QColor(default['Text']['color'])))
                self._welltextitems[wn].setVisible(default['Text']['visible'])
                self._wellheaditems[wn].setBrush(QBrush(QColor(default['Head']['color'])))
                self._wellheaditems[wn].setVisible(default['Head']['visible'])


class SliceScene(SceneData):
    def setupGraphics(self,column_track,hide_wells_outside = 500.0):
        geo = self._model.geo()
        if geo is None: raise Exception("SliceScene().setupGraphics() needs at least geo")

        # needs PyTOUGH column_track, a list of tuples of (column,entrypoint,exitpoint)
        if not column_track:
            print 'SliceScene().setupGraphics() received a bad column_track:', column_track
            return # do nothing if track is empty or bad
        slice_start_p = column_track[0][1]
        dout = 0.0
        from geometry import norm
        for ti in range(len(column_track)):
            trackitem = column_track[ti]
            col,points=trackitem[0],trackitem[1:]
            inpoint=points[0]
            if len(points)>1: outpoint=points[1]
            else: outpoint=inpoint
            din = dout
            dout = din + norm(outpoint-inpoint)
            for li in range(1,geo.num_layers):
                lay = geo.layerlist[li]
                if col.surface>lay.bottom:
                    blkname = geo.block_name(lay.name,col.name)
                    blkidx  = geo.block_name_index[blkname]

                    top=geo.block_surface(lay,col)
                    verts = [(din,lay.bottom),(din,top),(dout,top),(dout,lay.bottom)]
                    xc,yc = (0.5*(din+dout)), (lay.centre)

                    self._addBlock(blkname,blkidx,verts,(xc,yc))

                    # vertical connection
                    #if 0 <= ti <= (len(column_track)-1):
                    a = geo.block_name(lay.name, col.name)
                    if col.surface <= geo.layerlist[li-1].bottom:
                        # a should connect to atm block
                        if geo.atmosphere_type == 0:
                            b = geo.block_name(geo.layerlist[0].name, geo.atmosphere_column_name)
                        elif geo.atmosphere_type == 1:
                            b = geo.block_name(geo.layerlist[0].name, col.name)
                        else:
                            b = None
                    else:
                        b = geo.block_name(geo.layerlist[li-1].name, col.name)
                    if (b <> None) and ((a,b) in geo.block_connection_name_index):
                        #print a, '|', b
                        conname = (a,b)
                        conidx  = geo.block_connection_name_index[(a,b)]

                        pf = (xc, top)
                        pt = (xc, top-10.)

                        self._addConnection(conname,conidx,pf,pt)

                    # horizontal connection
                    if ti < (len(column_track)-1):
                        col_next = column_track[ti+1][0]
                        if col_next in col.neighbour:
                            ca,cb = col.name, col_next.name
                            if (ca,cb) in geo.connection:
                                to_right = True
                                a = geo.block_name(lay.name,ca)
                                b = geo.block_name(lay.name,cb)
                            else:
                                to_right = False
                                a = geo.block_name(lay.name,cb)
                                b = geo.block_name(lay.name,ca)
                            if (a,b) in geo.block_connection_name_index:
                                #print a, ':',  b
                                conname = (a,b)
                                conidx  = geo.block_connection_name_index[(a,b)]

                                yc1 = geo.block_centre(lay,col)[2]
                                yc2 = geo.block_centre(lay,col_next)[2]
                                yc = min([yc, yc1, yc2])
                                pf = (dout, yc)
                                if to_right:
                                    pt = (dout-10., yc)
                                else:
                                    pt = (dout+10., yc)

                                self._addConnection(conname,conidx,pf,pt)


        # bounding x, y, width, height of scene, supposedly.
        (xo,yo,wo,ho) = (0.0, geo.layerlist[-1].bottom,
            dout, geo.layerlist[0].bottom-geo.layerlist[-1].bottom)
        # exapand with a factor
        xpand = 0.1
        self.setSceneRect(xo-xpand*wo,yo-xpand*ho,wo+wo*xpand*2,ho+ho*xpand*2)

        from geometry import line_projection, polyline_line_distance
        # this part might need to be redone, to be more flexible like general
        # layer on/off in various apps
        ct_wells = [[] for ti in column_track]
        for wel in geo.welllist:
            hpos = [pos[:2] for pos in wel.pos]
            min_d, min_i = None, None
            for i,ti in enumerate(column_track):
                if len(ti) < 3: continue
                line = ti[1:]

                # check if wellhead "parallel" the line segment
                def slice_project(pos):
                    """ Returns 2-D projection of a 3-D point onto the slice plane """
                    hppos, xi = line_projection(pos[:2], line, return_xi = True)
                    if xi < 0.0 or xi > 1.0: return None
                    return np.array([np.linalg.norm(hppos - line[0]), pos[2]])
                phead = slice_project(wel.pos[0])
                if phead is None: continue

                # cloest distance of welltrack to line
                d = polyline_line_distance(hpos, line)
                if d <= hide_wells_outside:
                    if min_i is not None:
                        if d < min_d:
                            min_d = d
                            min_i = i
                    else:
                        min_d = d
                        min_i = i
            # record well plotting right
            if min_i is not None:
                ct_wells[min_i].append(wel.name)

        # debug/info
        for i,w in enumerate(ct_wells):
            if w:
                logging.info('column %s has well %s' % (str(column_track[i][0]), str(w)))

        left = 0.0
        for li,ti in enumerate(column_track):
            if len(ti) < 3:
                print 'funny column_track:', ti

            line = ti[1:]
            def slice_project(pos):
                """ Returns 2-D projection of a 3-D point onto the slice plane """
                hppos, xi = line_projection(pos[:2], ti[1:],return_xi=True)
                if xi > 0.0:
                    return np.array([np.linalg.norm(hppos - line[0]), pos[2]])
                else:
                    return np.array([-np.linalg.norm(hppos - line[0]), pos[2]])
            def slice_translate(pos):
                pos += np.array([left, 0.])
                return pos
            for well_i,wel in enumerate(geo.welllist):
                if wel.name not in ct_wells[li]: continue
                #hpos = [pos[:2] for pos in wel.pos]
                #if polyline_line_distance(hpos, line) > hide_wells_outside: continue

                pwellhead = slice_translate(slice_project(wel.head))
                wpos = np.array([slice_translate(slice_project(pos)) for pos in wel.pos])
                txt = '%s: bottom@%.2f' % (wel.name, wel.pos[-1][2])

                self._addWell(wel.name,well_i,pwellhead,wpos,txt)
            left += norm(ti[2]-ti[1])

class TopSurfaceScene(SceneData):
    def setupGraphics(self,not_used):
        geo = self._model.geo()
        if geo is None: raise Exception("TopSurfaceScene().setupGraphics() needs at least geo")

        # bounding x, y, width, height of scene, supposedly.
        bds = geo.bounds
        wo,ho = bds[1][0]-bds[0][0], bds[1][1]-bds[0][1]
        # exapand with a factor
        xpand = 0.1
        self.setSceneRect(
            bds[0][0]-wo*xpand, bds[0][1]-ho*xpand, wo+wo*xpand*2, ho+ho*xpand*2)

        for col in geo.columnlist:
            # names (for later PyTOUGH look up)
            blkname = geo.block_name(geo.layerlist[geo.num_layers-col.num_layers].name, col.name)
            blkidx  = geo.block_name_index[blkname]

            verts = [(n.pos[0],n.pos[1]) for n in col.node]

            self._addBlock(blkname,blkidx,verts,(col.centre[0],col.centre[1]))

        # this part might need to be redone, to be more flexible like general
        # layer on/off in various apps
        for well_i,wel in enumerate(geo.welllist):
            track = wel.pos_coordinate([0, 1])
            pwellhead = (track[0][0],track[0][1])
            wpos = [(p[0],p[1]) for p in track]
            txt = '%s: bottom@%.2f' % (wel.name, wel.pos[-1][2])

            self._addWell(wel.name,well_i,pwellhead,wpos,txt)

class LayerScene(SceneData):
    def setupGraphics(self,layer_i):
        import time
        geo = self._model.geo()
        if geo is None: raise Exception("LayerScene().setupGraphics() needs at least geo")
        # needs to know which layer!

        # bounding x, y, width, height of scene, supposedly.
        bds = geo.bounds
        wo,ho = bds[1][0]-bds[0][0], bds[1][1]-bds[0][1]
        # exapand with a factor
        xpand = 0.1
        self.setSceneRect(
            bds[0][0]-wo*xpand, bds[0][1]-ho*xpand, wo+wo*xpand*2, ho+ho*xpand*2)

        # populate columns
        #if dat:
        #    all_bs = grid.block.keys()
        # checking surface seems to be faster than checking if exist in dat
        if layer_i == 0:
            if geo.atmosphere_type == 0:
                # atm column does not really exist, create fake one
                from mulgrids import column
                atm_column = column(
                    name=geo.atmosphere_column_name,
                    node=geo.boundary_nodes,
                    surface=geo.layerlist[-1].bottom-1.0)
                    # make this now showing up in any layer (apart from layerlist[0])
                #geo.add_column(atm_column)
                this_layer_cols = [atm_column]
            elif geo.atmosphere_type == 1:
                this_layer_cols = [col for col in geo.columnlist]
            else:
                this_layer_cols = []
        else:
            this_layer_cols = [col for col in geo.columnlist if col.surface > geo.layerlist[layer_i].bottom]
        this_layer_name = geo.layerlist[layer_i].name
        # Populates scene

        start_time = time.time()
        for col in this_layer_cols: #geo.columnlist:
            # names (for later PyTOUGH look up)
            blkname = geo.block_name(this_layer_name, col.name)
            blkidx  = geo.block_name_index[blkname]

            verts = [(n.pos[0],n.pos[1]) for n in col.node]

            # TIME 95%+ time spent in addblock
            self._addBlock(blkname,blkidx,verts,(col.centre[0],col.centre[1]))
        msg = '    _addBlock() for all cols after %.2f sec' % (time.time() - start_time)
        logging.debug(msg)

        start_time = time.time()
        if layer_i != 0:
            from misc_funcs import normal
            from geometry import norm
            layercolset = set(this_layer_cols) # horizontal connections:
            layercons = [con for con in geo.connectionlist if set(con.column).issubset(layercolset)]
            for con in layercons:
                from misc_funcs import line_line_intersection
                cola, colb = con.column[0], con.column[1]
                # con._arrow_pts keeps cached points (from/to for arrow) value
                # is set to None if columns centres are too close to each other
                # so will skip creating arrow
                if hasattr(con, '_arrow_pts'):
                    if con._arrow_pts is None:
                        continue
                    else:
                        pf, pt = con._arrow_pts
                else:
                    p1,p2 = con.node[0].pos,con.node[1].pos
                    use_n = None
                    if list(p1) <> list(p2):
                        p12c = (p1 + p2) / 2.0
                        n = normal(p1,p2)
                        ninv = -1.0 * n
                        pa,pb = cola.centre,colb.centre
                        # find positive value so normal() same side as cola
                        n_max = max(np.dot(pa-p1,n),np.dot(p1-pb,n))
                        ninv_max = max(np.dot(pa-p1,ninv),np.dot(p1-pb,ninv))
                        # use tolerance kind of scale to the edge length
                        # temporarily set to 0.0
                        tol = 0.0e-11 * norm(p1-p2)
                        if n_max > ninv_max:
                            if n_max > tol: use_n = n
                        else:
                            if ninv_max > tol: use_n = ninv
                        # if still smaller than tol, means both column centres are too
                        # close to be on the line p1,p2, don't create conne
                        # should I search the next neighbour? maybe not
                    if use_n is not None:

                        p3 = p12c + use_n
                        pf = (p12c[0], p12c[1])
                        pt = (p3[0], p3[1])
                        con._arrow_pts = (pf, pt)
                    else:
                        con._arrow_pts = None
                if con._arrow_pts is not None:
                    b_name_a = geo.block_name(this_layer_name, cola.name)
                    b_name_b = geo.block_name(this_layer_name, colb.name)
                    conname = (b_name_a,b_name_b)
                    conidx  = geo.block_connection_name_index[conname]
                    # TIME 33% of conne creation time in addconne
                    self._addConnection(conname,conidx,pf,pt)
        msg = '    _addConnection() for all connes after %.2f sec' % (time.time() - start_time)
        logging.debug(msg)

        start_time = time.time()
        # this part might need to be redone, to be more flexible like general
        # layer on/off in various apps
        for well_i, wel in enumerate(geo.welllist):
            track = wel.pos_coordinate([0, 1])
            pwellhead = (track[0][0],track[0][1])
            wpos = [(p[0],p[1]) for p in track]
            txt = '%s: bottom@%.2f' % (wel.name, wel.pos[-1][2])

            self._addWell(wel.name,well_i,pwellhead,wpos,txt)
        msg = '    _addWell() for all wells after %.2f sec' % (time.time() - start_time)
        logging.debug(msg)

def testDrawColumnTrack(view,track):
    line = []
    lbls = []
    for ti in track:
        line.append(ti[1])
        line.append(ti[2])
        c = (ti[1][0]+ti[2][0]) / 2.0, (ti[1][1]+ti[2][1]) / 2.0
        lbls.append(TimTextItem(ti[0].name))
        lbls[-1].setPos(c[0],c[1])
    scene = view.scene()
    scene.loadPolyline(line)
    return lbls

