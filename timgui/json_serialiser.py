""" This module defines a set of fancy functions for json saving/loading tim-
related objects.  jsonDumpDefault() is used in json.dump as 'default'. And
jsonLoadHook() is used in json.load() as 'object_hook'.  In addition to the
python builtin types, some of the customised object types and some Qt objects
are supported.

To understand how it works, see: http://pymotw.com/2/json/

TODO: What is done here can actually be done using the library jsonpickle.  I
should probably just use that.  At this stage I am not sure whats the best way
to add one dependency after another.
"""

"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

import json

def loadFromFile(filename):
    """ This is a convenience function that loads a json and return a structure,
    generally a list or dict object.  This uses a specialised object hook that
    is able to re-create (some) customised objects as long as it was saved by
    using .saveToFile(). """
    f = open(filename, 'r')
    obj = json.load(f, object_hook=jsonLoadHook)
    f.close()
    return obj

def saveToFile(obj, filename):
    """ This is a convenience function that saves a list/dict-like object into a
    serialisation file format JSON.  This uses a specialised serialisation
    function that attempts to convert non-builtin objects into ones that can be
    stored in json.  Use loadFromFile() to load the file back and re-create the
    whole structure which may contain some customised objects.  Note not all
    customised objects can be successfully saved and reload, especially some of
    the more complex objects as well as most Qt objects. """
    f = open(filename, 'w')
    json.dump(obj, f, indent=4, default=jsonDumpDefault)
    f.close()

def jsonLoadHook(d):
    """ convert from json to the original object, to be used in json.load() as
    'object_hook' """
    if '__class__' in d and '__module__' in d:
        class_name = d.pop('__class__')
        module_name = d.pop('__module__')
        # __import__() needs fromlist so submodules can work
        # http://stackoverflow.com/questions/2724260/why-does-pythons-import-require-fromlist
        module = __import__(module_name, fromlist=['a'])
        class_ = getattr(module, class_name)

        if '__args__' in d:
            # use a fixed sequence of arguments to init, mostly for Qt objects
            args = [v.encode('ascii') for v in d.pop('__args__')]
            inst = class_(*args)
        else:
            # attempt to try using keyword arguments to init.  if failed, init
            # without args, then update properties
            kwargs = dict( (key.encode('ascii'), value) for key, value in d.items())
            try:
                inst = class_(**kwargs)
            except:
                try:
                    inst = class_()
                    for k,v in kwargs.iteritems():
                        setattr(inst, k, v)
                except:
                    logging.error('While loading from %s, failed to __init__() object of %s.%s' %
                        (self.filename,module_name,class_name))
    else:
        inst = d
    return inst

def jsonDumpDefault(obj):
    """ Convert objects to a dictionary of their representation, to be used in
    json.dump() as 'default' """
    d = {
        '__class__': obj.__class__.__name__,
        '__module__': obj.__module__,
    }

    if d['__class__'] in object_init_args:
        d['__args__'] = object_init_args[d['__class__']](obj)
    else:
        d.update(obj.__dict__)

    return d

# register classes that can be serialised nicely and provide the argument
# sequence to be used in __init__()
object_init_args = {
    'QColor': lambda q: [str(q.name())],

}

def registerObjectInit(class_name, args_func):
    """ This adds or overwrites (be careful!!!) the support of serialise and
    deserialise of custom objects.  class_name should be obtained by using
    .__class__.__name__  The args_func should be a function that translate an
    object into a list of arguments to be used when objects are re-created upon
    json.load().  Note this must match the class's __init__(). """
    object_init_args[class_name] = args_func

if __name__ == '__main__':

    def test(obj):
        j = json.dumps(obj, default=jsonDumpDefault, indent=4)
        ins = json.loads(j, object_hook=jsonLoadHook)
        to_p = [obj, j, ins]
        print '\n -> '.join([str(p) for p in to_p])
        return ins

    # from PyQt4.QtCore import *
    from PyQt4.QtGui import QColor
    print test( QColor('blue') ).name()
    print

    print test( ['abcdefg', {'dkey':'dval'}] )
    print

    class tt(object):
        def __init__(self, name='x', value=99):
            self.name = name
            self.value = value
    print test( tt('bluuuue', 100) ).name
    print

    class tt2(object):
        def __init__(self, nn='x', vv=99):
            self.name = nn
            self.value = vv
    print test( tt2('reeeeeed', 101) ).name
    print

    class tt3(object):
        def __init__(self, nn='x', vv=99):
            self.name = nn
            self.value = vv
    print test( tt3('compound', QColor('green')) ).value.name()
    print

    class tt4(object):
        def __init__(self, nn='x', vv=99):
            self.name = nn
            self.value = vv
        def xx(self):
            pass
    print test( tt4('w/ method', QColor('blue')) ).value.name()
    print

