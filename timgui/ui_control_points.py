"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from graphics_scenes import *
import settings

class Ui_PointsBoard(QWidget):
    """ a staging area for points/line, useful for slice creation etc.
    even if connects to ALL scenes, it will only hold one set of line at
    a time, the previous line will be wiped out everytime receiving a line """
    def __init__(self, parent=None):
        super(Ui_PointsBoard, self).__init__(parent)

        # as PyTOUGH: this stores a list of np.array, each np.array is
        # a point of x, y. eg:
        #    [ np.array([2775133.9, 6282885.4]),
        #      np.array([2780517.2, 6280577.0]),
        #      np.array([2780617.2, 6280077.0]) ]
        self._polyline = []

        self._s_manager = None

        self.list_widget = QListWidget(self)

        self._b_slice = QPushButton('Create Slice')
        self._b_copy = QPushButton('Copy')
        self._b_clear = QPushButton('Clear')
        self._b_show = QPushButton('Draw')
        self._b_load = QPushButton('Import')
        self._b_save_poly = QPushButton('Export as Polygon')
        self._b_save_line = QPushButton('Export as Line')
        blayout_1 = QHBoxLayout()
        blayout_2 = QHBoxLayout()
        blayout_3 = QHBoxLayout()
        blayout_1.addWidget(self._b_slice)
        blayout_1.addWidget(self._b_copy)
        blayout_2.addWidget(self._b_clear)
        blayout_2.addWidget(self._b_show)
        blayout_3.addWidget(self._b_load)
        blayout_3.addWidget(self._b_save_line)
        blayout_3.addWidget(self._b_save_poly)

        layout = QVBoxLayout()
        layout.addWidget(self.list_widget)
        layout.addLayout(blayout_1)
        layout.addLayout(blayout_2)
        layout.addLayout(blayout_3)
        self.setLayout(layout)

        # conect to its own methods
        self.connect(self._b_slice,SIGNAL("clicked ()"),self.createSlice)
        self.connect(self._b_copy,SIGNAL("clicked ()"),self.copyPolyline)
        self.connect(self._b_clear,SIGNAL("clicked ()"),self.clear)
        self.connect(self._b_show,SIGNAL("clicked ()"),self.emitPolyline)

        self.connect(self._b_load, SIGNAL("clicked ()"), self.loadFromFile)
        self.connect(self._b_save_line, SIGNAL("clicked ()"), self.saveLine)
        self.connect(self._b_save_poly, SIGNAL("clicked ()"), self.savePolygon)

    def _checkPolyline(self,polyline):
        """ check polyline (and process if necessary) and return a valid
        one """
        if len(polyline) >= 1:
            if type(polyline[0]) is type(np.array([0.,0.])):
                return polyline
        return []

    def polyline(self):
        return self._polyline

    # some of these a bit repeative, need re-work
    def clear(self):
        self._polyline = []
        self.list_widget.clear()
        self.emit(SIGNAL("clear_polyline"))
    def addPoint(self,pt):
        if len(pt) == 2:
            self._polyline.append(np.array([pt[0],pt[1]]))
            pitem = QListWidgetItem('%.1f, %.1f' % (pt[0],pt[1]))
            self.list_widget.addItem(pitem)
    def loadPolyline(self,polyline):
        self._polyline = self._checkPolyline(polyline)
        # refresh table
        self.list_widget.clear()
        for p in self._polyline:
            pitem = QListWidgetItem('%.1f, %.1f' % (p[0],p[1]))
            self.list_widget.addItem(pitem)
        self.parent().show()
        self.parent().raise_()
    def createSlice(self):
        self.emit(SIGNAL("create_slice"))

    def emitPolyline(self):
        self.emit(SIGNAL("send_polyline"),self._polyline)

    def copyPolyline(self):
        # as PyTOUGH: this stores a list of np.array, each np.array is
        # a point of x, y. eg:
        #    [ np.array([2775133.9, 6282885.4]),
        #      np.array([2780517.2, 6280577.0]),
        #      np.array([2780617.2, 6280077.0]) ]
        txt = ",\n".join(['np.array([%f,%f])' % tuple(p) for p in self._polyline])
        txt = "[ " + txt + " ]"
        clipboard = QApplication.clipboard()
        clipboard.setText(txt)

    def loadFromFile(self, fname=None):
        """ Import external files to use as polyline, supports:
        - BLN: Golden Software's blanking file format
        - GeoJSON: only support single Feature with Polygon (exterior only) and
          LineString
        """
        import os.path
        if fname is None:
            fname = QFileDialog.getOpenFileName(self,
                                                "Open Line File",
                                                settings._lines_path,
                                                "(*.bln *.json .geojson)")
        else:
            fname = QString(fname)
        if not fname.isEmpty():
            settings._lines_path = QFileInfo(fname).path()
            fext = os.path.splitext(str(fname))[1]
            if fext.lower() == '.bln':
                from timgui.file_format import bln_file
                fbln = bln_file(str(fname))
                self.loadPolyline(fbln.lines[0].line)
            elif fext.lower() in ['.json', '.geojson']:
                from file_format import load_geojson, shape_to_qgraphics
                s = load_geojson(str(fname))
                if s.geom_type == 'Polygon':
                    ### Only load exterior line
                    self.loadPolyline([np.array(p) for p in s.exterior.coords])
                elif s.geom_type == 'LineString':
                    self.loadPolyline([np.array(p) for p in s.coords])
                else:
                    print 'Ui_PointsBoard.loadFromFile() does not support %s yet.' % s.geom_type

    def savePolygon(self, fname=None):
        import os.path
        if fname is None:
            fname = QFileDialog.getSaveFileName (self,
                "Save As GeoJSON",
                os.path.join(str(settings._lines_path), 'polygon.json'),
                "JSON (JavaScript Object Notation) (*.json)")
        else:
            fname = QString(fname)
        if not fname.isEmpty():
            settings._lines_path = QFileInfo(fname).path()
            from file_format import save_polygon_as_geojson
            save_polygon_as_geojson(self._polyline, str(fname))

    def saveLine(self, fname=None):
        import os.path
        if fname is None:
            fname = QFileDialog.getSaveFileName (self,
                    "Save As GeoJSON",
                    os.path.join(str(settings._lines_path), 'line.json'),
                    "JSON (JavaScript Object Notation) (*.json)")
        else:
            fname = QString(fname)
        if not fname.isEmpty():
            settings._lines_path = QFileInfo(fname).path()
            from file_format import save_line_as_geojson
            save_line_as_geojson(self._polyline, str(fname))




