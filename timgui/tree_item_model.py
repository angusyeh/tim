"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *

class TreeItem(object):
    def __init__(self, data, parent=None):
        self.parent = parent
        self.children = []
        self.secret_children = []
        self.data = data
        self.color = '#000000'
        # private Qt.CheckState only significant when there is no children
        self._check_state = Qt.Unchecked

    def add(self, item, as_secret=False):
        """ Append another TreeItem() as a child, automatically sets the .parent
        attribute of the child item.  If as_secret is True, the child item will
        be added secretly, check state will propagate but child item will not
        know about this parent item. """
        if as_secret:
            self.secret_children.append(item)
        else:
            self.children.append(item)
            item.parent = self

    def removeChildren(self):
        """remove all children from a node. Usually used to clear the rootitem
        when resetting the tree"""
        all_children = self.children + self.secret_children
        for chld in all_children:
            chld.parent = None
        self.children = []
        self.secret_children = []

    def __getitem__(self, key):
        """ allow accessing child like a list (only *public* children) """
        return self.children[key]

    def position(self):
        """ order within siblings (index of self in self.parent.children) """
        if self.parent:
            return self.parent.children.index(self)
        return 0

    def setCheckState(self, state):
        """ state must be one of Qt.Unchecked, Qt.PartiallyChecked, Qt.Checked
        """
        all_children = self.children + self.secret_children
        if state == Qt.Unchecked:
            for c in all_children:
                c.setCheckState(Qt.Unchecked)
            self._check_state = Qt.Unchecked
        elif state == Qt.Checked:
            for c in all_children:
                c.setCheckState(Qt.Checked)
            self._check_state = Qt.Checked
        elif state == Qt.PartiallyChecked:
            self._check_state = Qt.PartiallyChecked
        else:
            raise Exception("Unrecognised state")

    def checkState(self):
        """ returns Qt.CheckState base on all children's states """
        all_children = self.children + self.secret_children
        if all_children:
            states = [x.checkState() for x in all_children]
            if all([c == Qt.Checked for c in states]):
                return Qt.Checked
            elif all([c == Qt.Unchecked for c in states]):
                return Qt.Unchecked
            else:
                return Qt.PartiallyChecked
        else:
            return self._check_state

    def setColor(self, color=None):
        """color is a string"""
        all_children = self.children + self.secret_children
        if color is not None:
            self.color = color


            for c in all_children:
                c.setColor(color)
        else:
            #check through all children colours. If they are not all the same, set color to 'mixed'
            chld_col = all_children[0].color
            mixed = False
            for chld in all_children:
                if chld.color != chld_col:
                    mixed = True
                    break
            if mixed:
                self.color = 'mixed'
            else:
                #if all children have the same colour, parent colour should automatically be the same
                if self.color != chld_col:
                    self.color = chld_col

        if self.parent:
            self.parent.setColor()





class TreeItemModel(QAbstractItemModel):
    def __init__(self):
        super(TreeItemModel, self).__init__()
        self.rootItem = TreeItem("ROOT")

    def clear(self):
        self.rootItem.removeChildren()

    def rowCount(self, parent):
        """returns number of children for the parent"""
        if parent.column() > 0:
            return 0
        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()
        return len(parentItem.children)

    def columnCount(self, parent):
        """assumes column count is 1"""
        if parent.isValid():
            return 1
        else:
            return 1

    def data(self, index, role):
        raise NotImplementedError("Implement .data()")

    def setData(self, index, value, role=Qt.CheckStateRole):
        raise NotImplementedError("Implementat .setData()")

    def index(self, row, column, parent):
        if not self.hasIndex(row, column, parent):
            return QModelIndex()
        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()
        childItem = parentItem.children[row]
        if childItem:
            return self.createIndex(row, column, childItem)
        else:
            return QModelIndex()

    def parent(self, index):
        if not index.isValid():
            return QModelIndex()
        childItem = index.internalPointer()
        parentItem = childItem.parent
        if parentItem == self.rootItem:
            return QModelIndex()
        if parentItem is None:
            return QModelIndex()
        return self.createIndex(parentItem.position(), 0, parentItem)

    def flags(self, index):
        if not index.isValid():
            return Qt.NoItemFlags
        return Qt.ItemFlags(QAbstractTableModel.flags(self,index) |
                            Qt.ItemIsEnabled |
                            Qt.ItemIsUserCheckable |
                            Qt.ItemIsTristate |
                            Qt.ItemIsSelectable |
                            Qt.ItemIsEditable
                            )

if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)

    model = TreeItemModel()
    model.rootItem.children.append(TreeItem('Wells'))
    model.rootItem.children.append(TreeItem('GIS Objects'))

    model.rootItem.children[0].add(TreeItem('Texts'))
    model.rootItem.children[0].add(TreeItem('WK  1'))
    model.rootItem.children[0].add(TreeItem('WK  9'))

    # model.rootItem.children[0].children[1].add(TreeItem('text'))
    # model.rootItem.children[0].children[1].add(TreeItem('track'))

    x = TreeItem('Text')
    model.rootItem[0][0].add(x, as_secret=True)
    model.rootItem[0][1].add(x)
    model.rootItem[0][1].add(TreeItem('Track'))
    model.rootItem[0][1].add(TreeItem('Head'))

    x = TreeItem('Text')
    model.rootItem[0][0].add(x, as_secret=True)
    model.rootItem[0][2].add(x)
    model.rootItem[0][2].add(TreeItem('Track'))
    model.rootItem[0][2].add(TreeItem('Head'))

    view = QTreeView()
    view.setModel(model)
    view.setWindowTitle('Test Tree View')
    view.show()

    app.exec_()

    from pprint import pprint as pp
    pp(model.getWellSetting())