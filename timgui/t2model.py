"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from mulgrids import *
from t2data import *
from t2listing import *

from PyQt4.QtCore import *
from PyQt4.QtGui import QProgressDialog, QMessageBox

from mulgrids import mulgrid as original_mulgrid
from waiwera_listing import wlisting

from units import Unit, guessUnits, unit_compatible
from ui_file_loader import load_file_progress
import logging

import json
import numpy as np

import time

# allow non-QObject like mulgrid to emit signal
geo_progress = QObject()

class mulgrid(original_mulgrid):
    """Extending the original mulgrid class from PyTOUGH"""
    def _report_progress(self, value):
        """ value should be a value between 0.0 and 1.0 """
        # for QProgressBar(), so convert to int, assume 0-100
        geo_progress.emit(SIGNAL("progress"), min(100,int(100.0*value)))

    def well_blocks(self,well_name, divisions=1, elevation=False,
        deviations=False, qtree=None, extend=False):
        return self.well_values(well_name, self.block_name_list, divisions=divisions, elevation=elevation,
            deviations=deviations, qtree=qtree, extend=extend)[1]

    def column_containing_point(self, pos, columns=None, guess=None,
                                bounds=None, qtree=None):
        """ This is replaces the one in PyTOUGH, being the most bottom level
        where qtree can be used.  I forced the cache of qtree here.  Even on a
        loarge grid of ~10,000 columns, this should only take about 1.5 sec, but
        could save 10s of seconds later when plotting well temperature etc.
        """
        if qtree is None:
            if not hasattr(self, '_qtree'):
                self._qtree = self.column_quadtree()
            qtree = self._qtree
        return super(mulgrid, self).column_containing_point(pos, columns, guess,
            bounds, qtree)

    def column_blocks_indices(self, col_name):
        """ Find all the blocks of this column, return a list of indices, intended for data slicing. """
        indices = []
        if col_name not in self.column:
            return
        col = self.column[col_name]
        for ilay,lay in enumerate(self.layerlist[1:]):
            if lay.bottom < col.surface:
                block_name = self.block_name_[(lay.name,col_name)]
                indices.append(self.block_name_index[block_name])
        return indices

    def column_name(self, blockname):
        """ Returns column name of block name.

        Overwrites original PyTOUGH one for fixing issue such as 'aa100' where
        fix/unfix_blockname() breaks column name.
        """
        if self.convention == 0: return blockname[0: 3]
        elif self.convention == 1:
            # 3 characters for layer followed by 2 digits for column
            col = blockname[3: 5]
            if (blockname[3] == '0' and
                blockname[2].isdigit() and
                ' ' + blockname[4] in self.column):
                # issue eg. 'aa100' where fix_blockname() breaks name
                return ' ' + blockname[4]
            return col
        elif self.convention == 2: return blockname[2: 5]
        elif self.convention == 3: return blockname[0: 3]
        else: return None

    def layer_name(self, blockname):
        """ Returns layer name of block name.

        Overwrites original PyTOUGH one for fixing issue such as 'aa100' where
        fix/unfix_blockname() breaks layer name.
        """
        if self.convention == 0:
            # 3 characters for column followed by 2 digits for layer
            lay = blockname[3: 5]
            if (blockname[3] == '0' and
                blockname[2].isdigit() and
                ' ' + blockname[4] in self.layer):
                # issue eg. 'aa100' where fix_blockname() breaks name
                return ' ' + blockname[4]
            return lay
        elif self.convention == 1: return blockname[0: 3]
        elif self.convention == 2: return blockname[0: 2]
        elif self.convention == 3: return blockname[3: 5]
        else: return None

    def read(self, filename):
        """ overwrites PyTOUGH one, just for progress reporting, otherwise identical """
        """Reads MULgraph grid from file"""
        self.empty()
        geo = fixed_format_file(filename, 'rU',
                                mulgrid_format_specification, self.read_function)
        self.read_header(geo)
        if self.type == 'GENER':
            read_fn = {
                'VERTI':self.read_nodes,
                'GRID': self.read_columns,
                'CONNE': self.read_connections,
                'LAYER': self.read_layers,
                'SURFA': self.read_surface,
                'SURF': self.read_surface,
                'WELLS': self.read_wells}
            more = True
            i = 0
            while more:
                line = geo.readline().strip()
                if line:
                    keyword = line[0:5].rstrip()
                    read_fn[keyword](geo)
                    i += 1
                    self._report_progress(float(i)/6.0*0.15)
                else: more = False
            self.setup_block_name_index()
            self.setup_block_connection_name_index()
        else: print('Grid type', self.type, 'not supported.')
        geo.close()
        return self

    def setup_block_name_index(self):
        """ Sets up list and dictionary of block names and indices for the
        tough2 grid represented by the geometry.

        Replaces the original PyTOUGH method, adding a few features required by
        TIM.  Also speedup some operations that is used frequently later.

        Additional properties (vs original):
            .block_height
            .block_name_   (faster .block_name(), key (lay name, col name))
        """
        import time
        start_time = time.time()
        self.block_name_list = []
        # each block's vertical distance to top and bottom from centre
        self.block_z_distances = {}
        # frequent use of .block_name() optimised by storing them in dict
        self.block_name_ = {} # block_name by (lay name,col name) tuple
        self.incomplete_block = {} # just for quick check, value is not important
        if self.num_layers > 0:
            self._report_progress(0.15)
            if self.atmosphere_type  ==  0: # one atmosphere block
                bname = self.block_name(self.layerlist[0].name, self.atmosphere_column_name)
                self.block_name_list.append(bname)
                self.block_name_[(self.layerlist[0].name, self.atmosphere_column_name)] = bname
                self.block_z_distances[bname] = (None, self.atmosphere_connection)
            elif self.atmosphere_type == 1: # one atmosphere block per column
                for col in self.columnlist:
                    bname = self.block_name(self.layerlist[0].name, col.name)
                    self.block_name_list.append(bname)
                    self.block_name_[(self.layerlist[0].name, col.name)] = bname
                    self.block_z_distances[bname] = (None, self.atmosphere_connection)
            if self.block_order is None or self.block_order == 'layer_column':
                # self.block_name_list += self.block_name_list_layer_column()
                for i,lay in enumerate(self.layerlist[1:]):
                    self._report_progress(0.15 + float(i)/float(self.num_layers)*0.25)
                    for col in [col for col in self.columnlist if col.surface > lay.bottom]:
                        bname = self.block_name(lay.name, col.name)
                        self.block_name_list.append(bname)
                        self.block_name_[(lay.name, col.name)] = bname
                        if col.surface is None:
                            d = lay.thickness / 2.0
                            self.block_z_distances[bname] = (d, d)
                        else:
                            if col.surface < lay.top:
                                # surface layer with surface below layer top
                                # incomplete layer
                                d = (col.surface - lay.bottom) / 2.0
                                self.block_z_distances[bname] = (d, d)
                                self.incomplete_block[bname] = 0
                            elif col.surface > self.layerlist[0].top:
                                if lay.name == self.layerlist[1].name:
                                    # surface layer with surface above layer top, centre at lay.centre
                                    self.block_z_distances[bname] = (col.surface-lay.centre, lay.centre-lay.bottom)
                                    self.incomplete_block[bname] = 0
                                else:
                                    # normal underground block
                                    d = lay.thickness / 2.0
                                    self.block_z_distances[bname] = (d, d)
                            else:
                                # subsurface layer
                                d = lay.thickness / 2.0
                                self.block_z_distances[bname] = (d, d)
            elif self.block_order == 'dmplex':
                # self.block_name_list += self.block_name_list_dmplex()
                blocknames = {6: [], 8: []}
                for i,lay in enumerate(self.layerlist[1:]):
                    self._report_progress(0.15 + float(i)/float(self.num_layers)*0.25)
                    for col in [col for col in self.columnlist if col.surface > lay.bottom]:
                        bname = self.block_name(lay.name, col.name)
                        # check num of nodes, DMPlex only supports 6 and 8 node blocks
                        num_block_nodes = 2 * col.num_nodes
                        try:
                            blocknames[num_block_nodes].append(bname)
                        except KeyError:
                            raise Exception('Blocks with %d nodes not supported by DMPlex ordering' %
                                            num_block_nodes)
                        self.block_name_[(lay.name, col.name)] = bname
                        if col.surface is None:
                            d = lay.thickness / 2.0
                            self.block_z_distances[bname] = (d, d)
                        else:
                            if col.surface < lay.top:
                                # surface layer with surface below layer top
                                # incomplete layer
                                d = (col.surface - lay.bottom) / 2.0
                                self.block_z_distances[bname] = (d, d)
                                self.incomplete_block[bname] = 0
                            elif col.surface > self.layerlist[0].top:
                                if lay.name == self.layerlist[1].name:
                                    # surface layer with surface above layer top, centre at lay.centre
                                    self.block_z_distances[bname] = (col.surface-lay.centre, lay.centre-lay.bottom)
                                    self.incomplete_block[bname] = 0
                                else:
                                    # normal underground block
                                    d = lay.thickness / 2.0
                                    self.block_z_distances[bname] = (d, d)
                            else:
                                # subsurface layer
                                d = lay.thickness / 2.0
                                self.block_z_distances[bname] = (d, d)
                self.block_name_list += blocknames[8] + blocknames[6]
            else:
                raise Exception('Unrecognised mulgrid block order: %s' % self.block_order)
        self.block_name_index = dict([(blk, i) for i, blk in enumerate(self.block_name_list)])
        msg = '    setup_block_name_index() finished after %.2f sec' % (time.time() - start_time)
        logging.debug(msg)

    def setup_block_connection_name_index(self):
        """ Sets up list and dictionary of connection names and indices for
        blocks in the TOUGH2 grid represented by the geometry.

        Replacess the original PyTOUGH method, adding a few features required by
        TIM.

        Additional properties (vs original):
            .block_top_connection_name_index
            .connection_areas
            con.sidelength
        """
        # original version of this can take ~25+ sec for 800,000 block model
        # original version of this can take ~ 5+ sec for 160,000 block model
        import time
        start_time = time.time()
        self.block_top_connection_name_index = {}
        self.block_horizontal_connection = {}
        self.block_connection_name_list = []
        for ilay,lay in enumerate(self.layerlist[1:]):
            self._report_progress(0.40 + float(ilay)/float(self.num_layers)*0.60)
            layercols = [col for col in self.columnlist if col.surface > lay.bottom]
            # layercolsnames = [c.name for c in layercols]
            for col in layercols: # vertical connections
                thisblkname = self.block_name_[(lay.name, col.name)]
                if (ilay == 0) or (col.surface <= lay.top): # connection to atmosphere
                    abovelayer = self.layerlist[0]
                    if self.atmosphere_type == 0:
                        aboveblkname = self.block_name_list[0]
                    elif self.atmosphere_type == 1:
                        aboveblkname = self.block_name_[(abovelayer.name,col.name)]
                    else:
                        continue
                else:
                    abovelayer = self.layerlist[ilay]
                    aboveblkname = self.block_name_[(abovelayer.name,col.name)]
                self.block_connection_name_list.append((thisblkname, aboveblkname))
                self.block_top_connection_name_index[thisblkname] = len(self.block_connection_name_list) -1
            # connection is the bottle-neck, 85% of time in this routine is here
            layercolset = set(layercols) # horizontal connections:
            layercons = [con for con in self.connectionlist if set(con.column).issubset(layercolset)]
            for con in layercons:
                conblocknames = tuple([self.block_name_[(lay.name,concol.name)] for concol in con.column])
                self.block_connection_name_list.append(conblocknames)
                self.block_horizontal_connection[conblocknames] = con
        self.block_connection_name_index = {con:i for i,con in enumerate(self.block_connection_name_list)}
        msg = '    setup_block_connection_name_index() finished after %.2f sec' % (time.time() - start_time)
        logging.debug(msg)

    def setup_block_connectionlist(self):
        """ setup all grid block connections, similar to t2grid's
        .connectionlist.  Only difference is con.block is a tuple of block names
        (str), instead of real t2block.  All other t2connection properties are
        normal.

        this gives geo/mulgrid two new properties:
            .block_connectionlist (t2grid.connectionlist)
            .block_connection (t2grid.connection)

        NOTE At the moment this is not automatically called when a geometry file
             is loaded.  It may be too slow for some models.
        """
        import time
        start_time = time.time()
        tilt = self.tilt_vector
        if tilt is None: tilt = np.array([0., 0., -1.])
        from math import cos, sin, radians
        anglerad = radians(self.permeability_angle)
        c, s = cos(anglerad), sin(anglerad)
        rotation = np.array([[c, s], [-s, c]])
        # speedup by working out all con .sidelength .distance .direction
        start_time_2 = time.time()
        for con in self.connectionlist:
            con.sidelength = norm(con.node[0].pos - con.node[1].pos)
            nodeline = [node.pos for node in con.node]
            con.distance = [norm(line_projection(c.centre, nodeline) - c.centre) for c in con.column]
            d = con.column[1].centre - con.column[0].centre
            d2 = np.dot(rotation, d)
            con.direction = np.argmax(abs(d2)) + 1
            if self.tilt_vector is None:
                con.dircos = 0.0
            else:
                dd = np.append(d, [0.0])
                con.dircos = np.dot(dd, tilt) / np.linalg.norm(dd)
        msg = '    pre-calculate all col connection params, finished after %.2f sec' % (time.time() - start_time_2)
        logging.debug(msg)

        self.block_connectionlist = []
        for b0, b1 in self.block_connection_name_list:
            if (b0,b1) not in self.block_horizontal_connection:
                # vertical connection
                abovedist = self.block_z_distances[b1][1]
                belowdist = self.block_z_distances[b0][0]
                area = self.column[self.column_name(b0)].area
                conn = t2connection([b0, b1], 3, [belowdist, abovedist], area, tilt[2])
                self.block_connectionlist.append(conn)
            else:
                # horizontal connection
                con = self.block_horizontal_connection[(b0,b1)]
                height = min([sum(self.block_z_distances[b]) for b in [b0,b1]])
                area = con.sidelength * height
                if b0 in self.incomplete_block or b1 in self.incomplete_block:
                    if self.tilt_vector is None:
                        dircos = 0.0
                    else:
                        zdiff = self.block_z_distances[b1][1] - self.block_z_distances[b0][1]
                        d = np.append((con.column[1].centre - con.column[0].centre), [zdiff])
                        dircos = np.dot(d, tilt) / np.linalg.norm(d)
                else:
                    # if both block thickness as layer, use pre-calculated dircos
                    dircos = con.dircos
                conn = t2connection([b0, b1], con.direction, con.distance, area, dircos)
                self.block_connectionlist.append(conn)

        self.block_connection = {con:i for i,con in enumerate(self.block_connectionlist)}
        msg = '    setup_block_connectionlist() finished after %.2f sec' % (time.time() - start_time)
        logging.debug(msg)


def lst_info(lst):
    """ returns a brief summary of t2listing object, similar to mulgrid/t2data's
    .__repr__()
    """
    return "\n".join([
        lst.title,
        "Tables: " + str(lst._tablenames),
        "Simulator: " + str(lst.simulator),
        "%i sets of full results" % lst.num_fulltimes,
        "%i sets of results (incl. short output)" % lst.num_times,
        "%i rows in element table" % lst.element.num_rows,
        ])

def dat_info(dat):
    """ returns summary of dat object, for either t2data class or waiwera json object"""
    if isinstance(dat,dict):
        num_rock_types = len(dat['rock']['types'])
        num_sources = len(dat['source'])
        return "\n".join([
            dat.get('title',"Waiwera model input"),
            str(num_rock_types) + " rock types; " +
            str(num_sources) + " sources; "
        ])
    else:
        return "\n".join([
            str(dat),
            str(dat.grid)
        ])

class Data(QObject):
    # to be overwritten by instance attribute OR sub-class class-attribute
    unit = ''
    simulators = ['autough2']
    def __init__(self):
        """ an Data object will be depending on geo/dat/lst, if not None """
        super(Data,self).__init__(parent=None)
        self._data = None

    def calculate(self, geo, dat, lst):
        """ This must be implemeted to return an np.array (or list) of values,
        be it strings or numbers.

        The size must match the data type specified (data_kind).  If data_kind =
        t2model.BLOCK, the size should be number of blocks.  If data_kind =
        t2model.FLOW, the size should be number of connections.
        """
        raise NotImplementedError("Implementation .calculate() is essential.")

    def isValid(self, geo, dat, lst):
        """ Implement this so Data only gets loaded with specific conditions.
        """
        return True

    # @property
    # def depends_on(self):
    #     raise NotImplementedError("Must define a valid static class variable 'depends_on.'")

    # @property
    # def data_kind(self):
    #     raise NotImplementedError("Must define a valid static class variable 'data_kind'")

    @property
    def valName(self):
        """ it will use the (sub-) class's name by default, with underscores
        translated into space.  But if the object has an '.overwriting'
        property, that will be used as name instead. """
        if hasattr(self,'overwrite_name'):
            # TODO: reconsider, why not just let subclass overwrite the property
            # with new name?
            return getattr(self,'overwrite_name')
        else:
            clsname = self.__class__.__name__
            return clsname.replace('_',' ')

    def setModel(self, geo, dat, lst):
        """ must get this done before start using it! """
        self._geo, self._dat, self._lst = geo,dat,lst

    def values(self):
        """ Will always return a numpy.array.
        """
        if self._data is None:
            self._data = np.array(self.calculate(self._geo, self._dat, self._lst))
        return self._data

    def clear(self):
        """ the only way to trigger an 'update' is to clear(), and  """
        self._data = None

class Model(QObject):
    """ this class is responsible of loading/re-loading geo/dat/lst (actual
    PyTOUGH objects), and storing named model variables.  Then client can then
    call getData() to obtain the latest values of a particular variables.
    Signals regarding addition, removal, and updates of the variables are
    emitted for clients to know when to obtain new data. """
    # 'mdtype' (model data types)
    BLOCK, FLOW = 'block', 'flow'
    # 'change'
    ADDED, UPDATED, REMOVED = 'added', 'updated', 'removed'
    def __init__(self):
        super(Model,self).__init__()
        self._geo = None
        self._dat = None
        self._lst = None

        self.geo_simulator = None # 'mulgrid'
        self.dat_simulator = None # 'tough2', 'autough2', 'waiwera'
        self.lst_simulator = None # 't2listing', 't2h5', 'waiwera'

        self._dirty_dat = False

        # dict store actual object, list store only names
        # [0] BLOCK, [1] FLOW
        self.data = {self.BLOCK:{},self.FLOW:{}} #[{}, {}]
        self.vars = {self.BLOCK:[],self.FLOW:[]} #[[], []]

    # access the PyTOUGH object via .geo() will allow future expansion of
    # multiplegeo/dat/listing etc, which may need additional parameter to
    # request different PyTOUGH objects
    def geo(self):
        return self._geo
    def dat(self):
        return self._dat
    def lst(self):
        return self._lst

    def simulator(self):
        """ return simulator type for (geometry, input, output) """
        return (self.geo_simulator, self.dat_simulator, self.lst_simulator)

    def getData(self, mdtype, valName, blocks=[]):
        """ If blocks are provided (and not empty), the 'slicing' of the data
        array will be done here.  The blocks can be a list of block indices  or
        block name.  (The returned value is a np.array.)
        """
        if blocks:
            if not isinstance(blocks[0],int):
                blocks = [self.geo().block_name_index[b] for b in blocks]
            return self.data[mdtype][valName].values()[blocks]
        else:
            return self.data[mdtype][valName].values()

    def getDataUnit(self, mdtype, valName):
        """ return the unit (a string) of the specified data. """
        return self.data[mdtype][valName].unit

    def dataIsFloat(self, mdtype, valName):
        """ return True if the data is a float, False otherwise. """
        vs = self.data[mdtype][valName].values()
        return np.issubdtype(vs.dtype, np.float_)

    def clearData(self,mdtype,valName):
        """ used when data is changed, so clear the cache and inform others.
        Values will be calculated next time when getData() is called. """
        if valName in self.data[mdtype]:
            self.data[mdtype][valName].clear()
            self.emit(SIGNAL("model_raw_data_changed"), (mdtype,valName,self.UPDATED))

    def removeData(self,mdtype,valName):
        """ data is deleted, others will be informed of the removal """
        if valName in self.data[mdtype]:
            del self.data[mdtype][valName]
            self.vars[mdtype].remove(valName)
            self.emit(SIGNAL("model_raw_data_changed"), (mdtype,valName,self.REMOVED))

    def registerData(self, data):
        """ will emit signal so scene manager knows additional valName """
        valName = data.valName
        mdtype = data.data_kind
        depends = data.depends_on
        if valName in self.data[mdtype]:
            self.removeData(mdtype,valName)
            print 'Warning! Model raw data %s overwritten.' % valName
        self_mobjs = (self._geo,self._dat,self._lst)
        for i,d in enumerate(depends):
            if d:
                if self_mobjs[i] is None:
                    raise Exception("registerData() failed, dependent model object does not exist.")
        data.setModel(self._geo,self._dat,self._lst)
        self.data[mdtype][valName] = data
        self.vars[mdtype].append(valName)
        self.emit(SIGNAL("model_raw_data_changed"), (mdtype,valName,self.ADDED))

    def _clearDependentData(self,clear=(False,False,False),delete=False):
        """ clears dependent Data, optionally delete instead of clear() """
        from copy import deepcopy

        for mdtype in [self.BLOCK, self.FLOW]:
            vs = deepcopy(self.vars[mdtype])
            for v in vs:
                data = self.data[mdtype][v]
                for c,dp in zip(clear,data.depends_on):
                    if c and dp:
                        if delete:
                            self.removeData(mdtype,v)
                        else:
                            self.clearData(mdtype,v)
                        break

    def emitRawDataLists(self):
        """ notify updated lists of raw data variables """
        self.emit(SIGNAL("model_block_raw_data_list_updated"), self.vars[self.BLOCK])
        self.emit(SIGNAL("model_flow_raw_data_list_updated"), self.vars[self.FLOW])

    def variableList(self,var_type):
        """return a list of variable (raw data) name, of specified type
        Model.BLOCK or Model.FLOW.

        Model.BLOCK are data with length of model's number of blocks.
        Model.FLOW are data with length of model's number of connections. """
        return self.vars[var_type]

    def variableTypes(self):
        """return allowed 'var_type' (model data types) that can be used in
        variableList() mehod."""
        return [self.BLOCK, self.FLOW]

    def loadMulgrid(self, geo):
        self._clearDependentData((True,False,False),delete=True)
        self._geo = geo
        self.geo_simulator = 'mulgrid'

        # remove dat and lst as well, they might not be relevant to new geo
        self._dat = None
        self.dat_simulator = None
        self._dirty_dat = False
        self.emit(SIGNAL("model_dat_reloaded"),"")
        self._lst = None
        self.lst_simulator = None
        self.emit(SIGNAL("model_lst_reloaded"),"")
        self.loadModelDataTypes()
        self.emit(SIGNAL("model_geo_reloaded"),self._geo.filename)

    def loadFileMulgrid(self, filename):
        """ Returns True is all goes well, otherwise returns False.
        """
        def geo_ok(geo):
            if geo.num_blocks > 0:
                return True
            else:
                return False
        geo = load_file_progress(filename,
                                 mulgrid,
                                 obj_ok=geo_ok,
                                 progress_qobj=geo_progress,
                                 name='Mulgrid Geometry File',
                                 code_name='PyTOUGH')
        if geo is None:
            return False
        else:
            self.loadMulgrid(geo)

            return True

    def loadDat(self,dat):
        self._clearDependentData((False,True,False), delete=True)
        self._dat = dat
        self._dirty_dat = False
        self.loadModelDataTypes()
        self.datDefaultVariables()
        if self.dat_simulator == 'waiwera':
            filename = dat['_metadata']['filename']
        else:
            filename = dat.filename
        self.emit(SIGNAL("model_dat_reloaded"), filename)


    def loadFileDataInput(self,filename):
        """ Returns True if all goes well, otherwise returns False.

        NOTE: see notes in loadFileMulgrid()

        if filename is a pytough data file, dat is a pytough t2data object.
        If filename ends is a waiwera json file, dat is a dictionary
        """
        def t2data_ok(dat):
            if dat.grid.num_blocks > 0:
                return True
            else:
                return False
        def read_json(fname):
            with open(fname, 'r') as f:
                data = json.load(f)
            if 'mesh' not in data:
                raise Exception('File does not contain the correct structure for waiwera')
            return data
        def wwinput_ok(dat):
            # TODO: use schema to check
            return all([
                'mesh' in dat, # 15/02/2019 only mesh required? see waiwera schema
                ])
        if filename.lower().endswith('.json'):
            ### waiwera
            logging.info('Opening file %s as Waiwera JSON input file.' % filename)
            dat = load_file_progress(filename,
                                     read_json,
                                     # obj_ok=wwinput_ok,
                                     progress_qobj=None,
                                     name='Waiwera Input File',
                                     code_name='JSON')
            if dat is not None:
                self.dat_simulator = 'waiwera'
                # store additional information needed by tim under the key '_metadata'
                _metadata = {
                    'filename':filename,
                    'simulator':'waiwera'
                    }
                dat['_metadata'] = _metadata
        else:
            ### aut2/tough2 etc
            logging.info('Opening file %s as (AU)TOUGH2 input file.' % filename)
            dat = load_file_progress(filename,
                                     t2data,
                                     obj_ok=t2data_ok,
                                     progress_qobj=None,
                                     name='TOUGH2 Data File',
                                     code_name='PyTOUGH')
            if dat is not None:
                if 'autough2' in dat.simulator.lower():
                    self.dat_simulator = 'autough2'
                else:
                    self.dat_simulator = 'tough2'

        if dat is None:
            return False
        else:
            self.loadDat(dat)
            return True

    def loadFileT2listing(self,filename):
        """ Returns True is all goes well, otherwise returns False.
        """
        def lst_ok(lst):
            if lst.num_fulltimes > 0:
                return True
            else:
                logging.error('There is no full results.' + msg + "\n" + lst_info(lst))
                return False
        lst = None
        if filename.lower().endswith('.h5'):
            ## checking h5 file, open a h5 file is fast, only reading metadata
            # TODO: add more basic checks?
            h5_type = ''
            import h5py
            with h5py.File(filename, 'r') as h5:
                if 'cell_index' in h5:
                    h5_type = 'waiwera'
                elif 'fulltimes' in h5:
                    h5_type = 'autough2'
            ## actually open the h5 file
            if h5_type == 'waiwera':
                logging.info('Opening file %s as Waiwera HDF5 output file.' % filename)
                from functools import partial
                if self.dat_simulator == 'waiwera':
                    lst = load_file_progress(filename,
                                             partial(wlisting, geo=self._geo,
                                                     fjson=self._dat, size_check=False),
                                             obj_ok=lst_ok,
                                             progress_qobj=None,
                                             name='Waiwera Output H5 File',
                                             code_name='waiwera_listing')
                else:
                    lst = load_file_progress(filename,
                                             partial(wlisting, geo=self._geo,
                                                     fjson=None, size_check=False),
                                             obj_ok=lst_ok,
                                             progress_qobj=None,
                                             name='Waiwera Output H5 File',
                                             code_name='waiwera_listing')
                if lst is not None:
                    self.lst_simulator = 'waiwera'
            elif h5_type == 'autough2':
                import t2listingh5
                logging.info('Opening file %s as AUTOUGH2 HDF5 output file.' % filename)
                lst = load_file_progress(filename,
                                         t2listingh5.t2listingh5,
                                         obj_ok=lst_ok,
                                         progress_qobj=None,
                                         name='AUTOUGH2 H5 Listing File',
                                         code_name='t2listingh5')
                if lst is not None:
                    self.lst_simulator = 'autough2'
            else:
                msg = 'Unrecognised HDF5 file %s, unable to open.' % filename
                logging.error(msg)
                QMessageBox.critical(None, 'Error', msg)
        else:
            lst = load_file_progress(filename,
                                     t2listing,
                                     obj_ok=lst_ok,
                                     progress_qobj=None,
                                     name='TOUGH2 Listing File',
                                     code_name='PyTOUGH')
            if lst is not None:
                self.lst_simulator = lst.simulator.lower()
                # all lower case in TIM, PyTOUGH detects
                # 'autough2', 'autough2', 'autough2', 'tough2', 'tough+'

        if lst is None:
            return False
        else:
            # .close() to release file lock or handles (.listing or .h5 files)
            if self._lst is not None:
                self._lst.close()

            self._clearDependentData((False,False,True),delete=True)
            self._lst = lst

            self.loadModelDataTypes()
            self.lstDefaultVariables()
            self.emit(SIGNAL("model_lst_reloaded"), filename)
            return True

    def changeListingStep(self,idx):
        if self._lst:
            if self._lst.index <> idx:
                start_time = time.time()
                self._lst.index = idx
                msg = '    self._lst.index = idx finished after %.2f sec' % (time.time() - start_time)
                logging.debug(msg)
                start_time = time.time()
                self._clearDependentData((False,False,True))
                msg = '    self._clearDependentData finished after %.2f sec' % (time.time() - start_time)
                logging.debug(msg)
                start_time = time.time()
                self.emit(SIGNAL("model_lst_step_changed"),idx)
                msg = '    emit signal finished after %.2f sec' % (time.time() - start_time)
                logging.debug(msg)


    def setDatEdited(self, refresh_tables=False):
        self._dirty_dat = True
        self._clearDependentData((False,True,False))

        if refresh_tables:
            if self.dat_simulator == 'waiwera':
                filename = self._dat['_metadata']['filename']
            else:
                filename = self._dat.filename
            self.emit(SIGNAL("model_dat_reloaded"),filename)

    def saveEditedDat(self, newfilename=''):
        # TODO: move dirty checking into command level
        #TODO:review this possibly
        # temporarily lift restriction of only write when dirty
        # if self._dirty_dat:
        if self.dat_simulator in ['autough2','tough2']:
            self._dat.write(newfilename)
            logging.info('Input file %s is saved.' % self._dat.filename)
            self._dirty_dat = False
            if newfilename:
                self.emit(SIGNAL("model_dat_renamed"), newfilename)
        elif self.dat_simulator in ['waiwera']:
            if not newfilename: newfilename = 'waiwera_data.json'
            with open(newfilename,'w') as f:
                metadata = self._dat.pop('_metadata') #remove metadata
                json.dump(self._dat,f,indent=1,sort_keys=True)
                metadata.update({'filename':newfilename})
                self._dat['_metadata'] = metadata
            logging.info('Input file %s is saved.' % self._dat['_metadata']['filename'])
            self._dirty_dat = False
            self.emit(SIGNAL("model_dat_renamed"),self._dat['_metadata']['filename'])


    def loadModelDataTypes(self):
        import default_model_datatypes
        #turned off User model datatypes: to be turned back on in the future if needed

        # import user_model_datatypes_ as user_model_datatypes

        # try import 'user_model_datatypes' from settings folder, otherwise use
        # builtin one TODO: might want a feature of user "reload" the module
        # instead of restart TIM
        ## temporarily removed user_datatypes while adding waiwera - need to rethink how this works
        #try:
            #import sys
            #import settings
            #sys.path.append(settings.settingsDir())
            #import user_model_datatypes
        #except:
            #import user_model_datatypes_ as user_model_datatypes

        import inspect
        all_classes = dict(inspect.getmembers(default_model_datatypes,inspect.isclass))

        # user one update/overwrite default one. TODO: come back to this
        #all_classes.update( dict(inspect.getmembers(user_model_datatypes,inspect.isclass)) )
        self.available_datatypes = []
        for k,c in all_classes.iteritems():
            if issubclass(c, Data):
                try:
                    data = c()
                    valName = data.valName
                    mdtype = data.data_kind
                    has_deps = True
                    availables = (self._geo, self._dat, self._lst)
                    for i,dp in enumerate(data.depends_on):
                        if dp:
                            if availables[i] is None:
                                has_deps = False
                                break
                            if not self.checkSimulatorMatch(i,data.simulators):
                                has_deps = False
                                break
                except Exception as e:
                    logging.error('Failed to load model data type <%s>: %s' % (k, str(e)))
                    continue

                if has_deps and (valName not in self.data[mdtype]):
                    if data.isValid(self._geo, self._dat, self._lst):
                        self.registerData(data)

    def checkSimulatorMatch(self,idx,simulators):
        """ Checks whether the model's geo, dat or lst models match the required
        simulator types idx 0 corresponds to checking geo, idx 1 checks dat, idx
        2 checks lst
        """
        ##TODO: this is probably not robust enough - could have a better method
        ##of checking data type in particular
        if idx == 0: #check geo
            return True #can assume geo exists, and as all geos are mulgrids, geo will always match
        elif idx == 1: #check dat
            if self.dat_simulator in simulators:
                return True
        elif idx == 2: #check lst
            if self.lst_simulator in simulators:
                return True

        return False


    def datDefaultVariables(self):
        """ load special model Data from dat/json input file that cannot be
        automatically loaded from module default_model_datatypes. """
        from functools import partial
        # use types.MethodType() to bind functions defined here as a method
        import types
        def waiwera_bc(self, geo, dat, lst, var):
            """ extract Waiwera input JSON's BC, return array of 'var' types.
            If var is a string "region", then array of region (converted to
            float) is returned.  If var is an integer 0/1/2/etc, then the
            primary index is returned.

            NOTE for cells where no BC is specified, value of -1 will be
            assigned for region and -1.0 will be assigned for primary.
            """
            if isinstance(var, int):
                data = np.full(geo.num_blocks, -1, dtype=np.float64)
            elif var == "region":
                data = np.full(geo.num_blocks, -1.0, dtype=np.float64)
            else:
                raise Exception('Unrecognised var type %s' % str(var))
            if "boundaries" in dat:
                # The "boundaries" value of json input is an array of objects.
                # In each boundary condition, the primary variables are
                # specified via the "primary" array and "region" integer value.
                # "faces" is either a single object or an array of objects that
                # contains the "cells" array.
                for bc in dat["boundaries"]:
                    primary = bc["primary"]
                    region = bc["region"]
                    if isinstance(bc["faces"], dict):
                        faces = [bc["faces"]]
                    else:
                        faces = bc["faces"]
                    cells = []
                    for face in faces:
                        cells += face["cells"]
                    for ci in cells:
                        if isinstance(var, int):
                            data[ci + geo.num_atmosphere_blocks] = primary[var]
                        elif var == "region":
                            data[ci + geo.num_atmosphere_blocks] = region
            return data
        def waiwera_primary_names(eos):
            """ return primary variable names for Waiwera eos """
            return {
                'w': ["pressure"],
                'we': ["pressure", "temperature/vapour_saturation"],
                'wae': ["pressure", "temperature/vapour_saturation", "air_partial_pressure"],
                'wce': ["pressure", "temperature/vapour_saturation", "CO2_partial_pressure"],
            }[eos]
        #####
        dat = self.dat()
        if self.dat_simulator in ['waiwera']:
            # create Data for Waiwera's (Dirichlet) boundary conditions, these
            # are specified in the input JSON under key "boundaries".
            eos = dat["eos"]
            if isinstance(eos, dict):
                eos = eos["name"]
            pri_names = waiwera_primary_names(eos)
            if "boundaries" in dat and dat["boundaries"]:
                ### Region
                data = Data()
                data.overwrite_name = 'BC Region'
                data.depends_on = (True,True,False)
                data.data_kind = Model.BLOCK
                data.unit = ''
                data.calculate = types.MethodType(partial(waiwera_bc,var='region'), data)
                self.registerData(data)
                ### Primary Variables
                for i,pname in enumerate(pri_names):
                    data = Data()
                    data.overwrite_name = 'BC Primary %i (%s)' % (i,pname)
                    data.depends_on = (True,True,False)
                    data.data_kind = Model.BLOCK
                    data.unit = ''
                    data.calculate = types.MethodType(partial(waiwera_bc,var=i), data)
                    self.registerData(data)

    def lstDefaultVariables(self):
        """ These are the tricky ones: each Data object needs to be dynamically
        loaded at runtime.  The class values as how it is defined in module
        default_model_datatypes needs to be modified, because by default the
        value name is derived from class name. """
        from functools import partial
        # use types.MethodType() to bind functions defined here as a method
        import types

        def nicer_waiwera_output_name(vname):
            """ return prettified variable names from Waiwera output

            - only change names starts with 'fluid_'
            - replace '_' by spaces
            - capitalise the first letter of each word
            """
            if vname.startswith('fluid_'):
                return ' '.join([w[0].upper() + w[1:] for w in vname[6:].split('_')])
            return vname

        ##### variables in element table
        def block_var(self,geo,dat,lst,vname):
            return lst.element[vname]
        # can have 'element', 'element1', 'element2' tables
        for tablename in [tab for tab in self.lst().table_names if tab.startswith('element')]:
            for v in self.lst().__dict__[tablename].column_name:
                data = Data()
                data.overwrite_name = nicer_waiwera_output_name(v)
                data.depends_on = (True,False,True)
                data.data_kind = Model.BLOCK
                data.unit = guessUnits(v)
                # bind the function to be a method of the object, see types
                data.calculate = types.MethodType(partial(block_var,vname=v), data)
                self.registerData(data)

        # vairables in the connection table
        def flow_var(self,geo,dat,lst,vname):
            return lst.connection[vname]
        if 'connection' in self.lst().table_names:
            for v in self.lst().connection.column_name:
                data = Data()
                data.overwrite_name = v
                data.depends_on = (True,False,True)
                data.data_kind = Model.FLOW
                data.unit = guessUnits(v)
                # bind the function to be a method of the object, see types
                data.calculate = types.MethodType(partial(flow_var,vname=v), data)
                self.registerData(data)

        # flow/flux through the top interface of the block, size should match
        # the number of block.  Note while viewing the slices, these are still
        # flow through the top interface of the block.
        def flow_thru_top_of_block(self,geo,dat,lst,vname,from_flux=False):
            try:
                ftop = np.zeros(geo.num_blocks)
                for i,b in enumerate(geo.block_name_list):
                    if b in geo.block_top_connection_name_index:
                        cname = geo.block_top_connection_name_index[b]
                        val = -lst.connection[cname][vname]
                        if from_flux:
                            cola = geo.column[geo.column_name(b)].area
                            val = val * cola
                        ftop[i] = val
                return ftop
            except Exception as e:
                logging.warning('Failed to get flow thru top of block: %s, error %s' % (vname, str(e)))
                return np.zeros(geo.num_blocks)
        def flux_thru_top_of_block(self,geo,dat,lst,vname,from_flow=False):
            try:
                ftop = np.zeros(geo.num_blocks)
                for i,b in enumerate(geo.block_name_list):
                    if b in geo.block_top_connection_name_index:
                        cname = geo.block_top_connection_name_index[b]
                        val = -lst.connection[cname][vname]
                        if from_flow:
                            cola = geo.column[geo.column_name(b)].area
                            val = val / cola
                        ftop[i] = val
                return ftop
            except Exception as e:
                logging.warning('Failed to get flux thru top of block: %s, error %s' % (vname, str(e)))
                return np.zeros(geo.num_blocks)
        def is_flow(v):
            """ Check if variable (listing.connection.column_name) is allowed to be used
            in 'flux thru top of block'.  Probably only heat flow and mass flow make
            sense.  This variable is flow, which needs to be converted to flux. """
            u = guessUnits(v)
            okays = ['J/s', 'kg/s']
            return any([unit_compatible(u,c) for c in okays])
        def is_flux(v):
            """ Check if variable (listing.connection.column_name) is allowed to be used
            in 'flux thru top of block'.  Probably only heat flow and mass flow make
            sense. """
            u = guessUnits(v)
            okays = ['J/m**2/s', 'kg/m**2/s']
            return any([unit_compatible(u,c) for c in okays])

        # Waiwera h5 provides only flux_ variables, so we need to convert them to flows
        def flow_from_flux(self, geo, dat, lst, vname):
            try:
                # get lst/h5 flux
                flux = lst.connection[vname]
                # get mulgrid connect areas
                if not hasattr(geo, 'block_connection_areas'):
                    print "calculating all connection areas ... ",
                    # there is no connection areas in a list
                    # this is probably slow, so only do it once
                    geo.setup_block_connectionlist()
                    geo.block_connection_areas = [c.area for c in geo.block_connectionlist]
                print "calculating flow from Waiwera %s ... " % vname,
                flow = lst.connection[vname] * np.array(geo.block_connection_areas)
                print "done."
                return flow
            except Exception as e:
                print e
                return np.zeros(geo.num_connections)
        def ww_flux_to_flow(v):
            """ check if a connection variable is allowed to be converted to flow """
            if v.startswith('flux_'):
                return True
            return False
        if 'connection' in self.lst().table_names:
            for v in self.lst().connection.column_name:
                ## for flux thru top of block
                if is_flux(v):
                    # flux to flux, no conversion needed
                    data = Data()
                    data.overwrite_name = 'Flux thru top of block: ' + v
                    data.depends_on = (True,False,True)
                    data.data_kind = Model.BLOCK
                    data.unit = guessUnits(v)
                    # bind the function to be a method of the object, see types
                    data.calculate = types.MethodType(partial(flux_thru_top_of_block,vname=v), data)
                    self.registerData(data)
                    # flux to flow
                    data = Data()
                    data.overwrite_name = 'Flow thru top of block: ' + v
                    data.depends_on = (True,False,True)
                    data.data_kind = Model.BLOCK
                    data.unit = str((Unit(guessUnits(v)) * Unit('m**2')).units)
                    # bind the function to be a method of the object, see types
                    data.calculate = types.MethodType(partial(flow_thru_top_of_block,vname=v,from_flux=True), data)
                    self.registerData(data)
                if is_flow(v):
                    # flow to flux
                    data = Data()
                    data.overwrite_name = 'Flux thru top of block: ' + v
                    data.depends_on = (True,False,True)
                    data.data_kind = Model.BLOCK
                    data.unit = str((Unit(guessUnits(v)) / Unit('m**2')).units)
                    # bind the function to be a method of the object, see types
                    data.calculate = types.MethodType(partial(flux_thru_top_of_block,vname=v,from_flow=True), data)
                    self.registerData(data)
                    # flow to flow, no conversion needed
                    data = Data()
                    data.overwrite_name = 'Flow thru top of block: ' + v
                    data.depends_on = (True,False,True)
                    data.data_kind = Model.BLOCK
                    data.unit = guessUnits(v)
                    # bind the function to be a method of the object, see types
                    data.calculate = types.MethodType(partial(flow_thru_top_of_block,vname=v), data)
                    self.registerData(data)
                ## for flow arrows
                if ww_flux_to_flow(v):
                    # these are for Waiwera outputs, which are flux
                    data = Data()
                    data.overwrite_name = 'Flow: ' + v.replace('flux_', '')
                    data.depends_on = (True,False,True)
                    data.data_kind = Model.FLOW
                    data.unit = str((Unit(guessUnits(v)) * Unit('m**2')).units)
                    # bind the function to be a method of the object, see types
                    data.calculate = types.MethodType(partial(flow_from_flux,vname=v), data)
                    self.registerData(data)

if __name__ == '__main__':
    model = Model()
    model.loadFileMulgrid('..\\gwai9011_13.DAT')
    model.loadFileDataInput('..\\wai9011pr_118e.dat')

    setupModelDefaultVariables(model)


    i = 4176
    print model.getData(Model.BLOCK,'Block Name')[i]
    print model.getData(Model.BLOCK,'Surf')[i]
    print model.getData(Model.BLOCK,'K 1')[i]

    model.loadFileT2listing('..\\wai9011pr_118e.listing')

    def temp(geo,dat,lst):
        return lst.element['Temperature']
    model.addBlockData('Temperature', temp, (True,False,True))

    print model.getData(Model.BLOCK,'Temperature')[i]
    model.lst().last()
    print model.getData(Model.BLOCK,'Temperature')[i]
    model.block_data['Temperature'].clear()
    print model.getData(Model.BLOCK,'Temperature')[i]


