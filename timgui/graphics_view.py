"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *
#from PyQt4.QtOpenGL import *

import math
from abstract_floating_widget import FloatingParentWidgetBase

# constants, will be moved
mv_OPTIMIZE = False
#mv_OPENGL = False


class TimGraphicsView(FloatingParentWidgetBase,QGraphicsView):
    def __init__(self,parent=None):
        super(TimGraphicsView, self).__init__(parent)

        self.scale(1,-1)

        self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        self.setViewportUpdateMode(QGraphicsView.FullViewportUpdate)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        self._pan = False

        # need more work, these rubber band selection made other modes inoperative
        #self.setDragMode(QGraphicsView.RubberBandDrag)
        #self.setRubberBandSelectionMode(Qt.ContainsItemShape)

        # Sets basic Flags for nice rendering
        self.setRenderHints(
            QPainter.Antialiasing |
            QPainter.TextAntialiasing |
            QPainter.SmoothPixmapTransform)
        """
        if mv_OPENGL:
            glw = QGLWidget()
            # this will set opengl antialiasing
            glw.setFormat(QGLFormat(QGL.SampleBuffers))
            self.setViewport(glw)
        """
        if mv_OPTIMIZE:
            self.setOptimizationFlags(
                QGraphicsView.DontAdjustForAntialiasing |
                QGraphicsView.DontSavePainterState)

    def scaleView(self, scaleFactor):
        factor = self.matrix().scale(scaleFactor, scaleFactor).mapRect(QRectF(0, 0, 1, 1)).width()
        if factor < 0.001 or factor > 1000: return

        self.scale(scaleFactor, scaleFactor)

        # if not self.scene(): return
        # allItems = self.scene().items()
        # for it in allItems:
        #     # 5 is polygon, 8 is text, 9 is simple text
        #     if int(it.type()) == 8:
        #         #print it.pos().x(), it.pos().y(), it.scenePos().x(), it.scenePos().y()
        #         #print 'transform text items', scaleFactor, factor, 0.01/scaleFactor
        #         #it.setTransformOriginPoint(it.boundingRect().center())
        #         it.setScale(1.0/scaleFactor)
        #         #it.adjustSize()
        #         #pass

    def wheelEvent(self, event):
        """ scale the view, but keep the scene position under mouse """
        if self.scene():
            # see https://stackoverflow.com/questions/19113532/qgraphicsview-zooming-in-and-out-under-mouse-position-using-mouse-wheel
            pscene = self.mapToScene(event.pos())

            self.scaleView(math.pow(2.0, event.delta() / 240.0))
            super(TimGraphicsView, self).wheelEvent(event)

            pmouse = self.mapFromScene(pscene)
            moveby = pmouse - event.pos()
            self.horizontalScrollBar().setValue(moveby.x() + self.horizontalScrollBar().value())
            self.verticalScrollBar().setValue(moveby.y() + self.verticalScrollBar().value())
            # QGraphicsView's transform has nothing to do with its scroll
            # position, so the behavior of QGraphicsView::mapToScene(const
            # QPoint&) const depends on both the scroll position and the
            # transform.

    def mouseMoveEvent(self, e):
        # make the pan "live" here
        if self._pan:
            self._panUpdate(e.pos())
        super(TimGraphicsView, self).mouseMoveEvent(e)

    def panStart(self,pos):
        self._pan = True
        self._pan_start_x = pos.x()
        self._pan_start_y = pos.y()
        self.setCursor(Qt.ClosedHandCursor)

    def _panUpdate(self,pos):
        """ make panning 'live', call from view.  DO NOT call this from scene,
        may cause recursion since mouse relative to scene will change """
        self.horizontalScrollBar().setValue(self.horizontalScrollBar().value() - (pos.x() - self._pan_start_x))
        self.verticalScrollBar().setValue(self.verticalScrollBar().value() - (pos.y() - self._pan_start_y))
        self._pan_start_x = pos.x()
        self._pan_start_y = pos.y()

    def panStop(self):
        self._pan = False
        self.unsetCursor()

    def keyPressEvent(self, event):
        key = event.key()
        if key == Qt.Key_Plus:
            ## use QApplication.sendEvent(). event()
            self.scaleView(1.2)
        elif key == Qt.Key_Minus:
            self.scaleView(1 / 1.2)
        QGraphicsView.keyPressEvent(self,event)

    def drawForeground(self, painter, rect):
        painter.setRenderHint(QPainter.Antialiasing)
        painter.setRenderHint(QPainter.TextAntialiasing)

        # vrect: visible rect in view coordinates. srect: in scene coordinates
        # NOTE:
        #       1. all drawing done in scene coordinates
        #       2. srect is "upside down", srect.top() will map to vrect.bottom()
        vrect = painter.viewport()
        srect = self.mapToScene(vrect).boundingRect()

        orig_font = painter.font()

        # ----- draw grid lines, algorithm from AEC's rmaview -----
        def firstScaleMark(value,inc):
            """ finds the first scale mark after input value, with inc as increment"""
            from math import modf
            # modf() gives (fractional, integer) parts of a float
            return value - modf(value/inc)[0] * inc + inc
        def calcGridIncrement(min_space, vrect, srect):
            """ find the best increment for a given grid.
            min_space: min inc length in view coordinates,
            vrect: area rect in view coordinates (related to pixel)
            srect: area rect in scene coordinates (real world) """
            def calcAlignedValue(value, aligns):
                """ align increment to rules of aligns """
                value = abs(value)
                mult = 1
                while True:
                    for a in aligns:
                        if a * mult >= value:
                            return a * mult
                    mult = mult * 10
            inc = min_space * (srect.width() / vrect.width())
            return calcAlignedValue(inc,[0.001, 0.002, 0.0025, 0.005])
        def gridLines(xs,ys,rect):
            """ returns a list of QLineF to be used to drawing grids at xs and ys """
            left, right, top, bottom = rect.left(), rect.right(), rect.top(), rect.bottom()
            lines = []
            for x in xs:
                lines.append(QLineF(x,top,x,bottom))
            for y in ys:
                lines.append(QLineF(left,y,right,y))
            return lines
        grid_pen = QPen()
        grid_pen.setColor(QColor("grey"))
        grid_pen.setStyle(Qt.DashLine)
        painter.setPen(grid_pen)
        margin = 0
        vrect_axes = QRect(vrect.left()+margin, vrect.top()+margin, vrect.width()-2*margin, vrect.height()-2*margin)
        srect_axes = self.mapToScene(vrect_axes).boundingRect()
        min_space = 100
        inc = calcGridIncrement(min_space, vrect, srect_axes)
        xstart = firstScaleMark(srect_axes.left(), inc)
        ystart = firstScaleMark(srect_axes.top(), inc)
        xs = [xstart+inc*i for i in range(int(srect_axes.width()/inc)+1)]
        ys = [ystart+inc*i for i in range(int(srect_axes.height()/inc)+1)]
        painter.drawLines(gridLines(xs,ys,srect_axes))
        # draw grid scale labels
        painter.setWorldMatrixEnabled(False)
        scale_text_ymargin = painter.fontMetrics().height() * 0.2
        scale_text_xmargin = painter.fontMetrics().width('0') * 0.2
        for x in xs:
            p = self.mapFromScene(QPointF(x,srect.top()))
            p.setX(p.x() + scale_text_xmargin)
            p.setY(p.y() - scale_text_ymargin)
            painter.drawText(p,str(x))
        for y in ys:
            p = self.mapFromScene(QPointF(srect.left(),y))
            p.setX(p.x() + scale_text_xmargin)
            p.setY(p.y() - scale_text_ymargin)
            painter.drawText(p,str(y))
        painter.setWorldMatrixEnabled(True)

        # ----- draw title/header -----
        margin = 15
        top_por = 0.05
        vrect_top = QRect(margin, margin, vrect.width()-2*margin, int(vrect.height() * top_por))
        title_font = self.font()
        title_font.setPointSize(title_font.pointSize()*2)
        painter.setFont(title_font)
        painter.setPen(QColor("black"))
        painter.setWorldMatrixEnabled(False)
        #painter.drawRect(vrect_top)
        #print QFontMetricsF(font).width('TEST')
        #painter.setFont(font)
        title = self.scene().name
        painter.drawText(vrect_top,Qt.AlignCenter|Qt.TextDontClip,title)
        #painter.drawText(vrect.center(),'TEST')
        painter.setWorldMatrixEnabled(True)

    def resetScale(self):
        """ reset view to have the whole scene fit in view """
        self.fitInView(self.sceneRect(),mode=Qt.KeepAspectRatio)

    def zoomIn(self):
        self.scaleView(1.25)

    def zoomOut(self):
        self.scaleView(0.8)

    def zoomScrollSetting(self):
        """ returns zoom and scroll settings """
        return (self.transform(),
            self.horizontalScrollBar().value(), self.verticalScrollBar().value())

    def setZoomScrollSetting(self,values):
        """ restores zoom and scroll settings """
        self.setTransform(values[0])
        self.horizontalScrollBar().setValue(values[1])
        self.verticalScrollBar().setValue(values[2])
