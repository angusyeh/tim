"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from ui_control_mode import Ui_SceneModeControl
from graphics_scenes import MyScene

from t2tables import Generators
import settings
import logging

class BlockGenerEntry(QObject):
    """ given block and/or gener names (regular expression allowed) this class can
    provide a list of matching generators (by index)"""
    def __init__(self,model,blocksourcenames=('','')):
        """ a tim._model_data.Model() object is required, entry """
        super(BlockGenerEntry,self).__init__()
        self._model = model # should I connect or just keep a ref?
        self._match = []    # keeps a list of index of matched geners
        self.entry = blocksourcenames # a tuple: (b_name,s_name)
    def __repr__(self):
        return "('%s','%s') has matched %i geners" % (self._entry[0],self._entry[1],len(self._match))
    def getEntry(self):
        """ original entry, a tuple of two strings """
        return self._entry
    def setEntry(self,e):
        """ entry should be a tuple of two strings (block,name), if any of them is
        empty, will result matching all in that particular field, as expected
        from regular expression """
        try:
            block,name = str(e[0]), str(e[1])
        except:
            raise Exception("BlockGenerEntry expects a tuple of two strings: (block_name,source_name)")
        self._entry = (block,name)
        import re
        self._r = (re.compile(block),re.compile(name))
        self.refresh()
    entry = property(getEntry,setEntry)
    def refresh(self):
        """ this should be called when model or entry updated """
        self._match = []
        if self._model is None: return # no model, no geners
        if self._model.dat() is None: return # no model.dat(), no geners
        if isinstance(self._model.dat(),dict): 
            ##Need to add support for waiwera in tables
            logging.error('Support for waiwera data has not been implemented in ui_table_gener')
            return #model.dat is waiwera, can't be treated like t2data type
        for i,g in enumerate(self._model.dat().generatorlist):
                mb, mg = self._r[0].match(g.block), self._r[1].match(g.name)
                if (mb is not None) and (mg is not None):
                    self._match.append(i)
    @property
    def matches(self):
        """ a list of matched generator index, read-only """
        return self._match

class Ui_GenerTable(QDialog):
    def __init__(self, parent=None):
        super(Ui_GenerTable, self).__init__(parent)
        ##### non UI objects
        self._model = None
        self.generators = Generators()
        self.pick = BlockGenerEntry(self._model,('',''))

        ##### UI, finder
        self.setWindowIcon(QIcon(":/gener_table.png"))
        self.block_edit = QLineEdit()
        block_label = QLabel('Find by &block:')
        block_label.setBuddy(self.block_edit)
        self.gname_edit = QLineEdit()
        gname_label = QLabel('&Gener name:')
        gname_label.setBuddy(self.gname_edit)
        self.connect(self.block_edit,SIGNAL("textChanged (const QString&)"),self.refreshMatch)
        self.connect(self.gname_edit,SIGNAL("textChanged (const QString&)"),self.refreshMatch)
        self.filter_option = QCheckBox('Show &matched only')
        self.connect(self.filter_option,SIGNAL("stateChanged (int)"),self.updateTableFilter)
        # pick a block button
        self.mode_control = Ui_SceneModeControl()
        single_button = self.mode_control.button()[MyScene.SINGLE_SELECTION_MODE]

        ##### UI, table
        self.table = QTableView()
        self.table.setModel(self.generators)
        # not sure why selectRwo() sometimes select a range instead of single row
        #self.connect(self.finder,SIGNAL("goto_gener_index"),self.table.selectRow)

        ##### UI, table edit
        self.sel_button = QPushButton('&Where is this?')
        self.connect(self.sel_button,SIGNAL("clicked ()"),self.whereAmI)
        self.add_button = QPushButton('&Add GENER')
        self.connect(self.add_button,SIGNAL("clicked ()"),self.addRow)
        self.del_button = QPushButton('&Remove GENER')
        self.connect(self.del_button,SIGNAL("clicked ()"),self.delRow)

        layout = QBoxLayout(QBoxLayout.TopToBottom)

        layout_1 = QBoxLayout(QBoxLayout.LeftToRight)
        layout_1.addWidget(block_label)
        layout_1.addWidget(self.block_edit)
        layout_1.addWidget(single_button)
        layout_1.addWidget(gname_label)
        layout_1.addWidget(self.gname_edit)
        layout_1.addStretch(1)
        layout_1.addWidget(self.filter_option)
        layout.addLayout(layout_1)

        layout.addWidget(self.table)

        layout_2 = QBoxLayout(QBoxLayout.LeftToRight)
        layout_2.addWidget(self.sel_button)
        layout_2.addStretch(1)
        layout_2.addWidget(self.add_button)
        layout_2.addWidget(self.del_button)
        layout.addLayout(layout_2)

        self.setLayout(layout)
        self.setWindowTitle('Generator Table')

        #self.table.setSelectionMode(QAbstractItemView.MultiSelection)
        #self.table.setSelectionBehavior(QAbstractItemView.SelectRows)
        #self.table.clearSelection()
        self.refreshMatch()

        self.loadSettings()

    def saveSettings(self):
        s = settings.appSettings()
        s.beginGroup("gener_table")
        s.setValue("geometry", self.saveGeometry())
        s.endGroup()

    def loadSettings(self):
        s = settings.appSettings()
        s.beginGroup("gener_table")
        if s.contains("geometry"):
            self.restoreGeometry(s.value("geometry").toByteArray())
        s.endGroup()

    def refreshMatch(self):
        """ either model updated, or entries updated """
        block = str(self.block_edit.text())
        name = str(self.gname_edit.text())
        self.pick.entry = (block,name)
        # update UIs
        self.updateTableFilter()

    def updateTableFilter(self):
        """ show/hide geners if option on, else show all """
        if self.filter_option.isChecked():
            for i in range(self.generators.rowCount()):
                if i in self.pick.matches:
                    self.table.showRow(i)
                else:
                    self.table.hideRow(i)
        else:
            for i in range(self.generators.rowCount()):
                self.table.showRow(i)
        # always go to first matched
        if self.pick.matches:
            self.gotoLine(self.pick.matches[0])

    def gotoLine(self,i):
        """ move to focus to table, and set index """
        selection_model = self.table.selectionModel()
        row = QItemSelection(self.generators.index(i,0),
            self.generators.index(i,self.generators.columnCount()-1))
        selection_model.select(row,QItemSelectionModel.ClearAndSelect)
        self.table.setCurrentIndex(row.indexes()[0])
        self.table.selectRow(i)
        self.table.scrollTo(self.generators.index(i,0))

    def addRow(self):
        r = self.generators.rowCount()
        self.generators.insertRows(r)
        index = self.generators.index(r,0) # move to gener
        self.table.setFocus()
        self.table.setCurrentIndex(index)
        self.table.edit(index)

    def delRow(self):
        r = self.table.currentIndex().row()
        self.generators.removeRows(r)

    def whereAmI(self):
        try:
            i = self.table.currentIndex().row()
            g = self._model.dat().generatorlist[i]
            block,name = g.block,g.name
        except AttributeError:
            pass

    def singleSelect(self,name,objType):
        self.block_edit.setText(name)

    def _modelReloaded(self):
        self.generators.connectModel(self._model)
        self.block_edit.setText('')
        self.gname_edit.setText('')
        # create a new one, as BlockGenerEntry only recognise one
        self.pick = BlockGenerEntry(self._model,('',''))

    def connectModel(self,model):
        if self._model:
            self.disconnect(self._model,SIGNAL("model_dat_reloaded"),self._modelReloaded)
        self._model = model
        if self._model:
            self.connect(self._model,SIGNAL("model_dat_reloaded"),self._modelReloaded)
            self._modelReloaded()

def test_Ui_GenerTable():
    import sys
    app = QApplication(sys.argv)

    model = Model()
    model.loadFileMulgrid('GWAI1515_AW_05.DAT')
    model.loadFileDataInput('WAI1515NS_AW_407_mod1 - Copy2.DAT')

    test = Ui_GenerTable()
    test.connectModel(model)
    test.show()

    app.exec_()

    model.dat().write()
    print 'file saved'

if __name__ == '__main__':
    test_Ui_GenerTable()
