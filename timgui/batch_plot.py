import json
import os.path
from itertools import cycle

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.backends.backend_pdf import PdfPages

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from timgui.base_commands import WindowCommand
from timgui.t2model import Model
import timgui.graph_dataseries as graph_dataseries
from timgui.graph_dataseries import FrozenDataSeries
import settings

def short_path(path, no_path=False):
    """ Shorten the path string, often used to display path that is too long """
    if '\\' in path:
        actual_sep = '\\'
    elif '/' in path:
        actual_sep = '/'
    else:
        actual_sep = os.path.sep

    pname, fname = os.path.split(path)
    if no_path:
        return fname

    if pname:
        ps = pname.split(actual_sep)
        pname = actual_sep.join([p[:1] for p in ps])
    return actual_sep.join([pname, fname])

def load_plot_list(fname, model):
    """ load the list of plots from json.  The returned object has nearly
    identical structure as the json file, apart from each series being loadded
    as one of the graph_dataseries objects. """
    f = open(fname,'r')
    jlist = json.load(f)
    f.close()

    plots = []
    for p in jlist:
        po = {'series':[]} # empty plot with empty series
        for s in p['series']:
            t = getattr(graph_dataseries, s['type'])
            del s['type']
            so = t(**s)
            so.connectModel(model)
            po['series'].append(so)
        del p['series']
        for k,v in p.iteritems():
            po[k] = v
        plots.append(po)

    return plots

def batch_plot(geo_name='', lst_name_list=[], fname=''):
    """ Simple batch plotting with some limitations.  WARNING time and timeunit
    for Downhole Series are not used here, hence this is really only for natural
    state, final full result table is always used (.last()) is always used.
    """
    print 'Batch Plot: WARNING for non-hisotry data series only the final full results in .listing file is used!'
    pdffname = os.path.splitext(fname)[0] + '.pdf'
    pdf_pages = PdfPages(pdffname)

    model = Model()
    plots = load_plot_list(fname, model)

    print 'Batch Plot: Loading geometry...'
    model.loadFileMulgrid(geo_name)

    # freeze series if not FieldData for Frozen for each listing
    for lst in lst_name_list:
        print 'Batch Plot: Loading Listing %s...' % lst
        model.loadFileT2listing(lst)
        # !!! ONLY uses the last set of full result
        model.lst().last()
        for p in plots:
            saved_s = []
            for s in p['series']:
                # only plot Field and Frozen data series, ignore the rest
                if 'Field' not in s.__class__.__name__ and 'Frozen' not in s.__class__.__name__:
                    saved_s.append(FrozenDataSeries())
                    saved_s[-1].saveSeries(s)
                    # saved_s[-1].name = ' '.join([str(s), short_path(lst)])
                    saved_s[-1].name = os.path.basename(lst)
            # only add to list after looping through
            p['series'] += saved_s

    linespecs = ['b+:', 'r^:', 'co:', 'k*:', 'ms:']
    fig = Figure(figsize=(11.69, 8.27), dpi=100)
    canvas = FigureCanvas(fig)
    for p in plots:

        ls = cycle(linespecs)
        fig.clear()
        ax = fig.add_subplot(1,1,1)
        for s in p['series']:

            if 'Field' in s.__class__.__name__:
                lbl = short_path(s.filename)
            elif 'Frozen' in s.__class__.__name__:
                lbl = s.name
            else:
                continue

            lss = ls.next()
            xs, ys = s.getXYData()
            ax.plot(xs,ys,
                lss, mfc='none', mec=lss[0], ms=6.0, mew=1.0, lw=2.0, label=lbl)

        # legend = ax.legend(loc='lower center', bbox_to_anchor=(0.5, -0.2)) # loc='lower left'
        legend = ax.legend(loc='best') # loc='lower left'
        ax.set_xlabel(str(p['xlabel']))
        ax.set_ylabel(str(p['ylabel']))
        ax.set_title(str(p['title']))

        if 'ylimits' in p and p['ylimits'] is not None:
            ax.set_ylim(p['ylimits'][0], p['ylimits'][1])
        if 'xlimits' in p and p['xlimits'] is not None:
            ax.set_xlim(p['xlimits'][0], p['xlimits'][1])

        ax.grid(True)
        pdf_pages.savefig(fig) #, bbox_extra_artists=(legend,), bbox_inches='tight')

    pdf_pages.close()
    print 'Batch Plot: File %s generated.' % pdffname
    return pdffname

class batch_plot_command(WindowCommand):
    tip = "Batch Plot"
    caption = "Batch Plot"
    def run(self):
        ## geometry
        if self.main_window.model.geo() is None:
            fname = QFileDialog.getOpenFileName(self.main_window,
                "Open Geometry File",
                settings._model_path)
            if fname.isEmpty():
                QMessageBox.warning(self.main_window, 'Warning', 'Batch Plotting Cancelled')
                return
            settings._model_path = QFileInfo(fname).path()
            geo_name = str(fname)
        else:
            geo_name = self.main_window.model.geo().filename

        ## plotlist json
        fname = QFileDialog.getOpenFileName(self.main_window,
            "Open Plot List JSON File",
            settings._model_path,
            "JSON (*.json)")
        if fname.isEmpty():
            QMessageBox.warning(self.main_window, 'Warning', 'Batch Plotting Cancelled')
            return
        json_name = str(fname)

        ## one or more listing file names
        fnames = QFileDialog.getOpenFileNames(self.main_window,
                    "Open One or More Listing Files",
                    settings._model_path)
        if len(fnames) == 0:
            QMessageBox.warning(self.main_window, 'Warning', 'Batch Plotting Cancelled')
            return
        lst_name_list = [str(fn) for fn in fnames]
        settings._model_path = QFileInfo(fnames[0]).path()

        ## actual plotting
        outfilename = batch_plot(geo_name, lst_name_list, json_name)
        QMessageBox.information(self.main_window,'', ('Batch Plotting Done\nFile %s generated.' % outfilename))

