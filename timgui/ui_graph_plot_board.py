"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from graph_widgets import LinePlotWidget
from graph_dataseries import DownholeWellSeries, DownholeFieldDataSeries, DownholeBlocksSeries
from graph_dataseries import HistoryBlockSeries, HistoryGenerSeries, HistoryFieldDataSeries
from graph_dataseries import FrozenDataSeries
from graph_dataseries import AlongAxisSeries, RadialSeries
from graph_lineplot import LinePlot
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
import settings
from units import Unit, defaultUnit
import numpy as np
import os.path
import json

import logging

class ListManagerWidget(QGroupBox):
    """ This is an UI that manages a list of objects (item).

    The Ui can be used as a normal widget.  It will try to maintain the
    selection to be single selection at any time, and emiting signals to inform
    that the current selection changes.  It has two buttons for adding and
    removing objects.  It is also possible to swap out the currently selected
    object with another one.

    If the objects is able to emit signal of "updated" the signify change in
    object, the manager is able to emit signal of "item_updated" to
    inform external objects that are interested.  Otherwise signal
    "item_updated" will only be emitted when the object is being swapped
    out.

    This object will emit signals:
    SIGNAL("item_added"), data, index
    SIGNAL("removing_item"), data, index
    SIGNAL("current_selection_changed"), data, index
    SIGNAL("item_updated"), data, index

    """
    def __init__(self, parent=None):
        super(ListManagerWidget, self).__init__('',parent)
        # use a simple list of series as a plot's data storage
        self._default_item_type = None

        # see addItem(), when adding an object into QListWidgetItem by setData()
        # does not cause the reference kept to be known by Python? hence garbage
        # collected if the object is out of scope.  Needs to keep a separate
        # reference
        self._keep_ref = []

        self.listwidget = QListWidget()
        self.add_button = QPushButton('New')
        self.remove_button = QPushButton('Remove')

        layout_buttons = QBoxLayout(QBoxLayout.LeftToRight)
        layout_buttons.addWidget(self.add_button)
        layout_buttons.addWidget(self.remove_button)

        self._layout_additional = QBoxLayout(QBoxLayout.LeftToRight)

        layout_vertical = QBoxLayout(QBoxLayout.TopToBottom)
        layout_vertical.addLayout(layout_buttons)
        layout_vertical.addLayout(self._layout_additional)
        layout_vertical.addWidget(self.listwidget)
        self.setLayout(layout_vertical)

        QObject.connect(self.add_button, SIGNAL('clicked()'), self.newItem)
        QObject.connect(self.remove_button, SIGNAL('clicked()'), self.removeItem)
        QObject.connect(self.listwidget,
            SIGNAL('currentItemChanged (QListWidgetItem *,QListWidgetItem *)'),
            self._currentItemChanged)
        QObject.connect(self, SIGNAL('item_updated'), self.updateText)

    def _currentItemChanged(self,new_item,prev_item):
        """ connected to self.listwidget, should be triggered when
        'currentItemChanged' """
        # currentRow() returns -1 if no new selection (when list becomes empty)
        i = self.listwidget.currentRow()
        if i >= 0:
            d = self._keep_ref[i]
        else:
            d = None
        self.emit(SIGNAL("current_selection_changed"),d,i)

    def _connectItemdataUpdate(self,item,itemdata):
        """add a special partial function attached to an item object so it can
        cause the manager to emit proper signal "item_updated".  And this is
        connected to the "updated" signal that is expected from the itemdata
        object. """
        from functools import partial
        def emit_updated(it):
            listwidget = it.listWidget()
            i = listwidget.row(it)
            d = self._keep_ref[i]
            self.emit(SIGNAL("item_updated"),d,i)
        item.emit_updated = partial(emit_updated,item)
        QObject.connect(itemdata, SIGNAL('updated'), item.emit_updated)

    def _disconnectItemdataUpdate(self,item,itemdata):
        """disconect the connection created by _connectItemdataUpdate.  Also
        remove the partial created for that purpose."""
        QObject.disconnect(itemdata, SIGNAL('updated'), item.emit_updated)
        delattr(item,'emit_updated')

    def addItem(self,itemdata):
        """ add an externally-generated object into the list """
        self._keep_ref.append(itemdata)

        item = QListWidgetItem(str(itemdata))
        self.listwidget.addItem(item)
        self.listwidget.setCurrentItem(item)

        # important to connect itemdata's updating signal
        self._connectItemdataUpdate(item,itemdata)

        self.emit(SIGNAL("item_added"),itemdata,self.listwidget.currentRow())

    def newItem(self):
        """ create a new object using default type (configurable) and add it
        into the list """
        try:
            itemdata = self._default_item_type()
        except:
            raise Exception("%s failed to create new object with default type: %s" % (str(self),str(self._default_item_type)))
        self.addItem(itemdata)

    def removeItem(self):
        """ remove the currently selected object, if nothing is selected,
        nothing will be done """
        i = self.listwidget.currentRow()
        item = self.listwidget.currentItem()
        if i >= 0:
            itemdata = self._keep_ref[i]
            self.emit(SIGNAL("removing_item"),itemdata,self.listwidget.currentRow())

            # disconnect and take actual item out
            self._disconnectItemdataUpdate(item,itemdata)
            self.listwidget.takeItem(i)

            # this is okay if the itemdata is one of those more complex type ie. not
            # one of the simpler data like integer or strings.
            self._keep_ref.pop(i)
            del item

    def replaceCurrentItem(self,itemdata):
        """ if the current object index is valid, this will replace the
        currently selected  object in the list with the new one and delete the
        original object. """
        i = self.listwidget.currentRow()
        if i >= 0:
            item = self.listwidget.item(i)
            old_itemdata = self._keep_ref[i]
            # disconnect and replace old_itemdata
            self._disconnectItemdataUpdate(item,old_itemdata)
            self._keep_ref[i] = itemdata
            # connect new itemdata to item
            self._connectItemdataUpdate(item,itemdata)
            self.emit(SIGNAL("item_updated"), itemdata, self.listwidget.currentRow())

    def setDefaultItemType(self,t):
        """ set the default type of objects created by using newItem().  This
        type will be used as the default type used to create new series. """
        self._default_item_type = t

    def updateText(self, itemdata, i):
        """ update all items' text in the list widget """
        item = self.listwidget.item(i)
        if item:
            item.setText(str(itemdata))

    def setListObject(self,list_obj):
        """ This allows user to 'hand over' a list-like object to be totally
        managed by self.  Warning, any adding, removing, replacing of the
        element will be applied to the list object directly.  Binding another
        list-like object to self will simply let go of control of the original
        list, may be garbage controlled if not 'kept' by caller. """
        # safe handling of the current list
        for i in reversed(range(self.listwidget.count())):
            item = self.listwidget.item(i)
            itemdata = self._keep_ref[i]
            self._disconnectItemdataUpdate(item, itemdata)
            # take it out of listwidget
            item = self.listwidget.takeItem(i)
            del item
            # note do not remove itemdata from self._keep_ref, in case it is
            # another externally owned list object that the object was managing.

        # give the external list_obj directly to self._keep_ref
        self._keep_ref = list_obj

        # if the list object passed in is None, then, create a empty list
        if list_obj is None:
            self._keep_ref = []

        # create items for each itemdata and connect properly
        for itemdata in self._keep_ref:
            item = QListWidgetItem(str(itemdata))
            self.listwidget.addItem(item)

            # important to connect itemdata's updating signal
            self._connectItemdataUpdate(item,itemdata)

        # only set currentItem once, so that no signal of "item_added" will be
        # emitted, only "current_selection_changed" is emitted once.
        if len(self._keep_ref) > 0:
            self.listwidget.setCurrentItem(item)

    def listObject(self):
        """return the actual list object (self._keep_ref)"""
        return self._keep_ref

    def currentItem(self):
        """return the currently selected/active item and its index"""
        i = self.listwidget.currentRow()
        if i >= 0:
            return self._keep_ref[i], i
        else:
            return None, -1

    def next(self):
        i = self.listwidget.currentRow()
        if i == (self.listwidget.count() - 1):
            return
        self.listwidget.setCurrentRow(i+1)

    def prev(self):
        i = self.listwidget.currentRow()
        if i == 0:
            return
        self.listwidget.setCurrentRow(i-1)

    def addButton(self, button_widget, callback):
        """Add additional buttons to the second row of the ui, with its callback
        function.  This can be useful when adding additional feature to the list
        manager such as load/save list."""
        self._layout_additional.addWidget(button_widget)
        QObject.connect(button_widget, SIGNAL('clicked()'), callback)

class DataSeriesEditWidget(QGroupBox):
    """ An editing panel for controlling various data series. User of this class
    must call .addAcceptSeries(name, series_type) to set up the editor widgets
    for each type that is going to be supported. """
    def __init__(self, parent=None):
        super(DataSeriesEditWidget, self).__init__('Edit Data Series', parent)
        self._current_series = None

        # keeping track of widgets matching series type
        self._type_index = {}
        # protected, cannot be switched either from or into other types
        self._immutable_series_types = []

        layout = QGridLayout()
        layout.setColumnStretch(0,0)
        layout.setColumnStretch(1,10)
        self.setLayout(layout)

    def addAcceptSeries(self, name, series_init, editor_widget, immutable=False):
        """ Insert a series-editing widget and setup radio button at the same
        time.  To enable an editor widget, the corresponding radiobutton has to
        be .setChecked(True).  Immutable series is a fixed type that cannot be
        controlled directly by user, where type switching in the UI is disabled.
        """
        i = len(self._type_index)
        # important to use type of an instance so that a fake type (creator
        # function) can work here.  The dictionary contains index and the
        # original creator/type that came into this function
        self._type_index[type(series_init())] = (i,series_init)
        r = QRadioButton(name)
        self.layout().addWidget(r,i,0,Qt.AlignLeft)
        self.layout().addWidget(editor_widget,i,1,Qt.AlignLeft)

        # radio controls the editor widget
        QObject.connect(r,SIGNAL("toggled (bool)"),editor_widget.setEnabled)
        # if user clicked radiobutton, series type is switched
        QObject.connect(r, SIGNAL('clicked ()'), self._switchType)

        # self._current_series = series_init
        # self._current_series.connectEditor(editor_widget)
        # self._current_series.updateEditor(editor_widget)
        r.setChecked(True)
        # editor_widget.setEnabled(True)

        # immutable here means no series can switch into or out of the type
        if immutable:
            self._immutable_series_types.append(type(series_init()))
            r.setEnabled(False)

    def currentSeriesType(self):
        """ return the currently active series type, by finding out which
        type is self._current_series """
        if self._current_series:
            return type(self._current_series)
        return None

    def currentRadioType(self):
        """ return the currently active radiobutton's type, by finding out which
        radiobutton is activated. """
        for t,(i,c) in self._type_index.items():
            r = self.layout().itemAtPosition(i,0).widget()
            if r.isChecked():
                return t
        return None

    def currentRadioCreator(self):
        """ return the currently active radiobutton's instance creator, by
        finding out which radiobutton is activated. """
        for t,(i,c) in self._type_index.items():
            r = self.layout().itemAtPosition(i,0).widget()
            if r.isChecked():
                return c
        return None

    def _getMatchedEditor(self,series):
        """ find the correct editor wdiget for the series. """
        w = self.layout().itemAtPosition(self._type_index[type(series)][0],1).widget()
        return w

    def _getMatchedRadio(self,series):
        """ find the correct radio button for the series """
        r = self.layout().itemAtPosition(self._type_index[type(series)][0],0).widget()
        return r

    def setSeries(self,series):
        """ Pass the control of a plot series object to the ui.  The series will
        be connected to a matching editor widget.  The editor widget will also
        be updated with the series's values. """

        # if already connected, disconnect editor from series first
        if self._current_series:
            w = self._getMatchedEditor(self._current_series)
            self._current_series.disconnectEditor(w)

        if series is None:
            self._current_series = None
            return

        # connect series to proper editor
        w = self._getMatchedEditor(series)
        if w is None: return
        self._current_series = series
        self._current_series.updateEditor(w)
        self._current_series.connectEditor(w)
        r = self._getMatchedRadio(self._current_series)
        # !!! Important to set corresponding radio button checked
        r.setChecked(True)

        # Stops user touching immutable series by disablingfrom UI level
        if self.currentSeriesType() in self._immutable_series_types:
            for i in range(len(self._type_index)):
                self.layout().itemAtPosition(i,0).widget().setEnabled(False)
        else:
            for k,(i,s_init) in self._type_index.items():
                if type(s_init()) not in self._immutable_series_types:
                    self.layout().itemAtPosition(i,0).widget().setEnabled(True)

    def _switchType(self):
        """ internal method that handles switching from one type of series to
        another, this one needs to do extra work of informing external objects
        (the one that keeps a list of series) about the need to switch series.
        WIP: not ideal, need rethink. """
        if self.currentRadioType() == self.currentSeriesType():
            return
        if self.currentSeriesType() in self._immutable_series_types:
            logging.error('Current series is immutable, cannot be switch to other series.')
            return

        # if needs switching, the current series's type will not match radio buttons
        old_w = self._getMatchedEditor(self._current_series)
        old_series = self._current_series
        old_series.disconnectEditor(old_w)
        type(old_series)().updateEditor(old_w)

        # create a new series object
        new_series = self.currentRadioCreator()()
        new_w = self._getMatchedEditor(new_series)
        new_series.connectEditor(new_w)
        new_series.updateEditor(new_w)
        self._current_series = new_series
        # ??? when do I need this? for updating plot?
        self.emit(SIGNAL("updated_to_new_series"),new_series)
        self.emit(SIGNAL("series_replaced"),self._current_series)

    def refreshCurrentEditor(self):
        """Refresh the active editor widget.  To avoid recursive changes back to
        the series, the editor and series are disconnected first and then re-
        connected afte editor refreshed."""
        if self._current_series:
            w = self._getMatchedEditor(self._current_series)
            self._current_series.disconnectEditor(w)
            self._current_series.updateEditor(w)
            self._current_series.connectEditor(w)

def bestLabels(xlbl, ylbl, series):
    """ Depdends on the initial values of x/y labels, along with the contents of
    series, returns the best x/y labels
    """
    if xlbl is None:
        xlbl = ''
    if xlbl == '':
        if series.xlabel:
            xlbl = series.xlabel
    if ylbl is None:
        ylbl = ''
    if ylbl == '':
        if series.ylabel:
            ylbl = series.ylabel
    if series.xunit and (not xlbl.endswith(')')):
        xlbl += ' (%s)' % str(defaultUnit(series.xunit))
    if series.yunit and (not ylbl.endswith(')')):
        ylbl += ' (%s)' % str(defaultUnit(series.yunit))
    return xlbl, ylbl

class Ui_PlotBoard(QDialog):
    """ Important, use must call setUp() so that appropriate data series edits
    will be added."""
    def __init__(self, parent=None):
        """ This is not the final function called before the dialog is shown.
        .setUP() is called after __init__() of the object and before being
        actually .show(). """
        super(Ui_PlotBoard, self).__init__(parent)
        self._model = None
        self.filename = 'New'

        self.plot = LinePlotWidget()
        self.plot_tools = NavigationToolbar(self.plot, self)
        self.series_edit = DataSeriesEditWidget()

        self.series_list = ListManagerWidget()
        self.series_list.setTitle('Data Series')
        self.series_list.addButton(QPushButton('Export values'),self.saveLineXY_)
        self.series_list.addButton(QPushButton('Keep values'),self.freezeCurrent)

        self.plot_list = ListManagerWidget()
        self.plot_list.setTitle('Plot List')
        self.plot_list.addButton(QPushButton('Load List'),self.loadPlotList_)
        self.plot_list.addButton(QPushButton('Save List'),self.savePlotList_)

        
        layout_centre = QBoxLayout(QBoxLayout.TopToBottom)
        layout_centre.addWidget(self.series_edit,1)
        layout_centre.addWidget(self.plot_tools,1)
        layout_centre.addWidget(self.plot,10)


        self.layout_left = QBoxLayout(QBoxLayout.TopToBottom)
        self.layout_left.addWidget(self.series_list,1)
        self.layout_left.addWidget(self.plot_list,2)
       
        layout = QBoxLayout(QBoxLayout.LeftToRight)
        # layout.addWidget(self.show_plot_list_button)
        layout.addLayout(self.layout_left,2)
        layout.addLayout(layout_centre,10)
        self.setLayout(layout)

        # self.connect(self.show_plot_list_button,SIGNAL("clicked()"),self.toggleLeftPanel)

        ##### connection between all the controls #####
        # series list and series edit, changing between series and series types
        self.connect(self.series_list,
            SIGNAL("current_selection_changed"),self.setSeriesToEdit)
        self.connect(self.series_edit,
            SIGNAL("series_replaced"),self.series_list.replaceCurrentItem)


        # series list and plot, add/remove/update lines in plot
        QObject.connect(self.series_list,
            SIGNAL('item_added'), self.plotAddSeries)
        QObject.connect(self.series_list,
            SIGNAL('removing_item'), self.plotRemoveSeries)
        QObject.connect(self.series_list,
            SIGNAL('item_updated'), self.plotUpdateSeries)

        # plot_list and series list, changing between plots
        QObject.connect(self.plot_list,
            SIGNAL("current_selection_changed"), self.series_list.setListObject)
        QObject.connect(self.plot_list,
            SIGNAL("current_selection_changed"), self.plotAPlot)

        # additional if a series changes, plot needs to know as well
        QObject.connect(self.series_list, SIGNAL('item_updated'), self.updatePlotText)

        # here the new series (within new plot) will not be properly connected
        # to the model, as self hasn't been connected to model yet.
        # self.plot_list.newItem()

        # use evenfilter to trigger edit refresh when window get focus.  This is
        # to avoid the editor not showing new options as model refreshed.
        # Connection to the model's signals isn't ideal, as it cannot anitcipate
        # what signals those series/editor pair needs.  See:
        # http://www.riverbankcomputing.com/pipermail/pyqt/2008-May/019590.html
        class OnFocusEventFilter(QObject):
            def __init__(self, parent, callback):
                QObject.__init__(self, parent)
                self.callback = callback
            def eventFilter(self, obj, event):
                if event.type() == QEvent.ActivationChange:
                    if self.parent().isActiveWindow():
                        self.callback()
                return QObject.eventFilter(self, obj, event)
        self.installEventFilter(OnFocusEventFilter(self,
            self.series_edit.refreshCurrentEditor))
  
    def nextPlot(self):
        self.plot_list.next()

    def prevPlot(self):
        self.plot_list.prev()

    def plotCreator(self,series_type):
        """ create a new plot with a new series, series_type is the default for
        the series. """
        s = series_type()
        s.connectModel(self._model)
        p = LinePlot()
        p.append(s)
        return p

    def seriesCreator(self,series_type):
        """this wraps the series types and make sure all new series is connected
        to the model """
        s = series_type()
        s.connectModel(self._model)
        return s

    def setUp(self, series_types, win_title='', win_icon='', new_item=True):
        """Setting up the list of accepted data series is the most important. It
        should be a list of tuples: (name string, series type).  This is
        separate from the __init__() because the dialog is generic, and need a
        more complex setup before being used. """

        # example of series_types
        # series_types = [
        #     ('By Well',DownholeWellSeries, DownholeWellSeries.createEditor()),
        #     ('Field Data',DownholeFieldDataSeries, DownholeFieldDataSeries.createEditor()),
        # ]

        from functools import partial
        # create and set series edit's types
        self._series_creators = []
        for name, typ, w in series_types:
            self._series_creators.append(partial(self.seriesCreator,typ))
            if typ is FrozenDataSeries:
                immutable = True
            else:
                immutable = False
            self.series_edit.addAcceptSeries(name,self._series_creators[-1],w, immutable)

        # new in series list use first type
        self.series_list.setDefaultItemType(self._series_creators[0])
        # new in plot list creates a plot with default series
        self.plot_list.setDefaultItemType(partial(self.plotCreator,self._series_creators[0]))

        self.win_title = win_title
        self.setWindowTitle(win_title)
        self.setWindowIcon(QIcon(win_icon))
        
        # create a new item upon initialisation, instead of empty everything
        if new_item:
            self.plot_list.newItem()

        self.loadSettings()

    def refreshUnits(self):
        #look into graph updating when listing file is reloaded - possibly could take advantage of existing methods/signals
        for i,d in enumerate(self.series_list.listObject()):
            self.plotUpdateSeries(d,i)

    def plotAddSeries(self,series,i):
        """adding a series into plot"""
        x, y = series.getXYData()
        xlbl, ylbl = bestLabels('', '', series)
        if series.xunit:
            x = (np.array(x) * Unit(series.xunit)).to(defaultUnit(series.xunit)).magnitude
        if series.yunit:
            y = (np.array(y) * Unit(series.yunit)).to(defaultUnit(series.yunit)).magnitude
        legend = str(series)
        self.plot.addLine(x,y,legend)
        self.plot.setLabels(xlbl, ylbl)

    def plotRemoveSeries(self,series,i):
        """removing a series from plot"""
        self.plot.removeLine(i)

    def plotUpdateSeries(self,series,i):
        """updating a series in plot"""
        x, y = series.getXYData()
        xlbl, ylbl = bestLabels('', '', series)
        if series.xunit:
            x = (np.array(x) * Unit(series.xunit)).to(defaultUnit(series.xunit)).magnitude
        if series.yunit:
            y = (np.array(y) * Unit(series.yunit)).to(defaultUnit(series.yunit)).magnitude
        legend = str(series)
        self.plot.updateLine(i,x,y,legend)
        self.plot.setLabels(xlbl, ylbl)

    def setSeriesToEdit(self,series):
        self.series_edit.setSeries(series)
        if series is None:
            self.series_edit.hide()
        else:
            self.series_edit.show()

    def plotAPlot(self,plot_obj):
        """empty the plot and fill with plot's series"""
        self.plot.empty()
        if plot_obj is None:
            self.series_list.hide()
            self.series_edit.hide()
        else:
            self.series_list.show()
            self.series_edit.show()
        if plot_obj is not None:
            xlbl, ylbl = plot_obj.xlabel, plot_obj.ylabel
            for i,s in enumerate(plot_obj):
                xlbl, ylbl = bestLabels(xlbl, ylbl, s)
                self.plotAddSeries(s,i)
            self.plot.setLabels(xlbl, ylbl, plot_obj.title)

    def updatePlotText(self):
        """ note this node not accept any itemdata and i argument, as they are
        emitted by self.series_list, which is series, and here we want the
        current plots. """
        itemdata,i = self.plot_list.currentItem()
        self.plot_list.updateText(itemdata,i)

    def freezeCurrent(self):
        """ create a frozen x-y by save current x-y and then load back in as
        field data """
        it, i = self.series_list.currentItem()
        if it:
            # TODO: do the name/key properly to include time/listing name, etc.
            # might need to add addtional info property in each DataSeries, or
            # even model's Data.
            desc, ok = QInputDialog.getText(self,
                "Save Current Series",
                "Description:",
                QLineEdit.Normal,
                str(it))
            if ok:
                newit = FrozenDataSeries(original_series=str(it), name=str(desc))
                # this is necessary, unique to FrozenDataSeries
                newit.saveSeries(it)
                self.series_list.addItem(newit)

    def saveLineXY_(self):
        """show file dialog, let user select file to overwrite, and call
        self.saveLineXY()"""
        fname = QFileDialog.getSaveFileName (self,
                "Save X Y Data", self.filename,
                "Plain x y file (*.dat)")
        if not fname.isEmpty():
            self.saveLineXY(str(fname))

    def saveLineXY(self, fname): #this could cause some issues if we play with default units
        """save the current line's xy values into a text file"""
        it, i = self.series_list.currentItem()
        if it:
            xs, ys = it.getXYData()
            if len(xs) > 0:
                if it.xunit:
                    xs = (np.array(xs) * Unit(it.xunit)).to(defaultUnit(it.xunit)).magnitude
                if it.yunit:
                    ys = (np.array(ys) * Unit(it.yunit)).to(defaultUnit(it.yunit)).magnitude

                f = open(fname,'w')
                for x,y in zip(xs,ys):
                    f.write("%15.10e %15.10e\n" % (x,y))
                f.close()

    def savePlotList_(self):
        """show file dialog, let user select file to overwrite, and call
        self.savePlotList()"""

        fname = QFileDialog.getSaveFileName (self,
                "Save To Plot List", self.filename,
                "JSON (JavaScript Object Notation) (*.json)")
        if not fname.isEmpty():
            self.filename = str(fname)
            self.savePlotList(self.filename)
            self.setWindowTitle(self.win_title + ': ' + self.filename)

    def savePlotList(self,fname):
        """serialise the list of plots into file, allows later reload of the list"""

        def serialisePlot(plot):
            po = {}
            po['title'] = plot.title
            po['xlabel'] = plot.xlabel
            po['ylabel'] = plot.ylabel
            po['properties'] = plot.properties
            po['series'] = []
            for s in plot.series:
                so = {}
                so['type'] = s.__class__.__name__
                for k in s.KEYS:
                    so[k] = getattr(s,k)
                po['series'].append(so)
            return po

        f = open(fname,'w')
        # NOTE, the self.plot_list.listObject() will be a normal sequence, so
        # the default encoding will treat it as normal list, and only call the
        # default at plot object level.  Hence serialisePlot only handles plot
        # instead of plot list.
        json.dump(self.plot_list.listObject(), f, default=serialisePlot,
            indent=4, sort_keys=True)
        f.close()

    def loadPlotList_(self):
        """show file dialog, let user select file, and call
        self.loadPlotList()"""

        fname = QFileDialog.getOpenFileName(self,
                "Load From Plot List", '.',
                "JSON (JavaScript Object Notation) (*.json)")
        if not fname.isEmpty():
            self.filename = str(fname)
            self.loadPlotList(self.filename)
            self.setWindowTitle(self.win_title + ': ' + self.filename)

    def loadPlotList(self,fname):
        """load previously saved plot list file (.json) back."""
        # can replace built-in json module with simplejson in the future, which
        # (more) effectively report errors users may have made in .json files:
        # https://github.com/simplejson/simplejson
        # http://simplejson.readthedocs.org/en/latest/

        # load json file into simple structure
        jlist = settings.safe_loading_json_file(fname, [],
            try_dirs=[(os.path.normpath(settings.settingsDir()), 'User settings'),])

        # decoding/go through structure and create objects
        import sys
        import graph_dataseries
        plots = []
        for p in jlist:
            try:
                po = LinePlot()
                for s in p['series']:
                    t = getattr(graph_dataseries, s['type'])
                    del s['type']
                    so = t(**s)
                    so.connectModel(self._model)
                    po.series.append(so)
                del p['series']
                for k in p.keys():
                    setattr(po,k,p[k])
                plots.append(po)
            except Exception as ex:
                logging.warning('Failed loading plot entry, skipping: %s' % json.dumps(p))
                logging.warning('  Exception: %s' % str(ex))

        # show the object in ui
        self.plot_list.setListObject(plots)

    def TEMP_add_plot(self,plot_obj):
        """ temporary testing add whole plot (a list-like object) to series_list
        directly """
        self.series_list.setListObject(plot_obj)

        # still have to manually add to plot, as plot is not aware of the
        # concept of a plot, only what it's told to draw and remove,
        # self.series_list updates connected to plot chagnes.
        self.plot.empty()
        for i,s in enumerate(plot_obj):
            self.plotAddSeries(s,i)

    def toggleLeftPanel(self):
        self.plot_list.setVisible(not self.plot_list.isVisible())

    def connectModel(self,model):
        """ This should take care of the event when connecting to different
        model (including from None to something real).  It should go through all
        objects that depends on model, eg. series.  Only connects to one model
        at any given time."""

        # for all series in the plot list
        for p in self.plot_list.listObject():
            for s in p.series:
                try:
                    s.connectModel(model)
                except:
                    logging.warning('Warning data series %s failed to connect to model.' % str(s))

       # for used in series creator
        self._model = model

    def defaultPlotListPath(self):
        """ creates widget's own setting file name, based on window title. """
        import misc_funcs
        fname_base = misc_funcs.string_to_filename(self.win_title, nice=True)
        return settings.getSettingsPath(fname_base+'.json', fallback=False)

    def defaultSavedSeriesPath(self):
        """ creates widget's own setting file name, based on window title. """
        import misc_funcs
        fname_base = misc_funcs.string_to_filename(self.win_title, nice=True)
        return settings.getSettingsPath(fname_base+'_saved.json', fallback=False)

    def saveSettings(self):
        s = settings.appSettings()
        s.beginGroup(self.win_title)
        s.setValue("geometry", self.saveGeometry())
        s.endGroup()

        self.savePlotList(self.defaultPlotListPath())

    def loadSettings(self):
        s = settings.appSettings()
        s.beginGroup(self.win_title)
        if s.contains("geometry"):
            self.restoreGeometry(s.value("geometry").toByteArray())
        s.endGroup()

        p = settings.userSettings()
        if p.value("load_plots_on_start", True).toBool():
            try:
                self.loadPlotList(self.defaultPlotListPath())
            except IOError:
                logging.warning("Previous list of plots (%s) does not exist, list will be empty." % self.defaultPlotListPath())
                pass

    def closeEvent(self, event):
        # note if I put saveSettings() here, it will be called every time when
        # the dialog is closed/hidden.
        event.accept()

class Ui_HistoryPlotBoard(Ui_PlotBoard):
    def __init__(self, parent=None):
        """ This is not the final function called before the dialog is shown.
        .setUP() is called after __init__() of the object and before being
        actually .show(). """
        super(Ui_HistoryPlotBoard, self).__init__(parent)

        self.default_unit_button = QPushButton('Set Initial Time')
        self.default_unit_button.setEnabled(False)

        self.layout_left.addWidget(self.default_unit_button,0)
        self.connect(self.default_unit_button,SIGNAL("clicked ()"), self._promptUnitControl)

    def _promptUnitControl(self):
        self.emit(SIGNAL("show_units_control"))

    def _updateTimeButton(self):
        if self._model.lst():
            self.default_unit_button.setEnabled(True)
        else:
            self.default_unit_button.setEnabled(False)

    def connectModel(self,model):
        """ This should take care of the event when connecting to different
        model (including from None to something real).  It should go through all
        objects that depends on model, eg. series.  Only connects to one model
        at any given time."""

        # for all series in the plot list
        for p in self.plot_list.listObject():
            for s in p.series:
                try:
                    s.connectModel(model)
                except:
                    logging.warning('Warning data series %s failed to connect to model.' % str(s))
        
        if self._model:
            self.disconnect(self._model,SIGNAL("model_lst_reloaded"),self._updateTimeButton)
        # for used in series creator
        self._model = model
        self.connect(self._model,SIGNAL("model_lst_reloaded"),self._updateTimeButton)
      

def makeDownholePlotBoard(model, scene_manager, parent=None):
    board = Ui_PlotBoard(parent)
    board.connectModel(model)
    series_types = [
        ('By Well',DownholeWellSeries,DownholeWellSeries().createEditor()),
        ('By Column:',DownholeBlocksSeries,DownholeBlocksSeries().createEditor(scene_manager, model)),
        ('File Data',DownholeFieldDataSeries,DownholeFieldDataSeries().createEditor()),
        ('Saved',FrozenDataSeries,FrozenDataSeries().createEditor()),
    ]
    board.setUp(series_types,
        win_title='Plot with Elevation',
        win_icon=':/plot_depth.png',
        new_item=True)
    return board

def makeHistoryPlotBoard(model, scene_manager, parent=None):
    board = Ui_HistoryPlotBoard(parent)
    board.connectModel(model)
    series_types = [
        ('Block Property',HistoryBlockSeries,HistoryBlockSeries().createEditor(scene_manager)),
        ('Gener Property',HistoryGenerSeries,HistoryGenerSeries().createEditor(scene_manager)),
        ('File Data',HistoryFieldDataSeries,HistoryFieldDataSeries().createEditor()),
        ('Saved',FrozenDataSeries,FrozenDataSeries().createEditor()),
    ]
    board.setUp(series_types,
        win_title='Time History Plot',
        win_icon=':/plot_history.png',
        new_item=True)
    return board

def makeAlongPlotBoard(model, scene_manager, parent=None):
    board = Ui_PlotBoard(parent)
    board.connectModel(model)
    series_types = [
        ('Radial Plot',RadialSeries,RadialSeries().createEditor()),
        # ('Along Axis',AlongAxisSeries,AlongAxisSeries().createEditor()),
        ('File Data',HistoryFieldDataSeries,HistoryFieldDataSeries().createEditor()),
        ('Saved',FrozenDataSeries,FrozenDataSeries().createEditor()),
    ]
    board.setUp(series_types,
        win_title='Radial Plot',
        win_icon=':/plot_history.png',
        new_item=True)
    return board


if __name__ == '__main__':
    import unittest
    class Test_DepthPlotBoard(unittest.TestCase):
        """Ui_PlotBoard - downhole plot (elevation vs. depth) """
        def setUp(self):
            import sys
            self.app = QApplication(sys.argv)
            self.win = Ui_PlotBoard()

            from t2model import Model
            self.model = Model()
            self.win.connectModel(self.model)

            self.model.loadFileMulgrid('GWAI1515_AW_05.DAT')
            self.model.loadFileT2listing('WAI1515PR_AW_407_ayfixnew6.LISTING')

        def tearDown(self):
            """Needs to del these objects otherwise the window will crash upon finishing tests."""
            del self.win
            del self.app
            # del self.model

        def newSeries(self):
            # s = DownholeWellSeries(well='E   4', time=1.0, variable='Surf')
            # s.connectModel(self.model)
            # self.win.series_edit.setSeries(s)
            self.win.series_list.newItem()

        def loadPlot(self):
            self.p0 = [
                DownholeWellSeries(well='xxx', time=1.0, variable='Surf'),
                DownholeWellSeries(well='xxx', time=1.0, variable='Temperature'),
            ]
            self.win.TEMP_add_plot(self.p0)

            self.p = [
                DownholeWellSeries(well='E   4', time=1.0, variable='Surf'),
                DownholeWellSeries(well='WK  1', time=1.0, variable='Temperature'),
            ]
            for s in self.p:
                s.connectModel(self.model)
            self.win.TEMP_add_plot(self.p)

        def test_win(self):

            series_types = [
                ('By Well',DownholeWellSeries),
                ('Field Data',DownholeFieldDataSeries),
            ]
            self.win.setUp(series_types,
                win_title='Plot with Elevation',
                win_icon=':/plot_depth.png',
                new_item=True)

            # actually show window, not necessary
            self.win.show()
            self.win.raise_()
            self.win.activateWindow()
            self.app.exec_()

    unittest.main(verbosity=2)


