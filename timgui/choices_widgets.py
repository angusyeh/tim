"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *

class ChoicesDialog(QDialog):
    """ A modal dialog box that forces user to choose an option.
    """
    def __init__(self, parent=None):
        super(ChoicesDialog, self).__init__(parent,
            Qt.WindowTitleHint | Qt.WindowSystemMenuHint) # hide What's This
        self.setWindowTitle('Unit selection')
        self.group_box = QGroupBox()
        self.group_box.setLayout(QGridLayout())
        self.buttons = QButtonGroup()
        self.button_box = QDialogButtonBox(
            QDialogButtonBox.Ok | QDialogButtonBox.Cancel)

        self.setLayout(QVBoxLayout())
        self.layout().addWidget(self.group_box)
        self.layout().addWidget(self.button_box)

        QObject.connect(self.button_box, SIGNAL('accepted()'), self.accept)
        QObject.connect(self.button_box, SIGNAL('rejected()'), self.reject)

    def setUp(self, desc, choices, current_idx):
        """ set up the choices of the dialog box, choices is a list of text,
        also needs index of the inital selection.
        """
        self.group_box.setTitle(desc)
        for i,u in enumerate(choices):
            b = QRadioButton(u)
            if current_idx == i:
                b.setChecked(True)
            self.buttons.addButton(b, i)
            self.group_box.layout().addWidget(b,i,0)

    def getSelected(self):
        """ return the selected button in text"""
        return str(self.buttons.checkedButton().text())

def getChoiceFromDialog(description, choices, current_index):
    """ Ask user to select an option, choices is a list of text, also needs
    index of the inital selection.  If user cancel and exit the dialogbox, None
    is returned.  This is a static convenience function, no need to worry about
    book keeping of the actual dialog etc.
    """
    diag = ChoicesDialog()
    diag.setUp(description, choices, current_index)
    if diag.exec_():
        return diag.getSelected()
    else:
        return None

if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)

    print getChoiceFromDialog('Temperature:', ['degC', 'degF'], 0)
